<?php 
	// echo $this->Form->create(null, array('url'	=>	"#"));
	echo $this->Form->control('phone');
	// echo $this->Form->end();
?>

<button id="sign-in-button">submit</button>

<div id="recaptcha-container"></div>

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-firestore.js"></script>

<script>
		  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDoRMRRyCcAvspYKDD5fM4iynggUEH4rTU",
    authDomain: "yuasa-test.firebaseapp.com",
    databaseURL: "https://yuasa-test.firebaseio.com",
    projectId: "yuasa-test",
    storageBucket: "yuasa-test.appspot.com",
    messagingSenderId: "388381034910",
    appId: "1:388381034910:web:58324ffc4291b2c2e92e8f"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.auth().useDeviceLanguage();

  window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
	  'size': 'invisible',
	  'callback': function(response) {
	    // reCAPTCHA solved, allow signInWithPhoneNumber.
	    alert("recaptha verifier");
	    onSignInSubmit();
	  }
	});

  window.recaptchaVerifier.render();

  function onSignInSubmit()
  {
  	alert("onSignInSubmit");
  	var phoneNumber = "+62816831402";
	var appVerifier = window.recaptchaVerifier;
	firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
	    .then(function (confirmationResult) {
	      // SMS sent. Prompt user to type the code from the message, then sign the
	      // user in with confirmationResult.confirm(code).
	      alert("sms sent");
	      window.confirmationResult = confirmationResult;
	    }).catch(function (error) {
	      // Error; SMS not sent
	      // ...
	      console.log(error);
	      alert("error");
	    });
  }

</script>