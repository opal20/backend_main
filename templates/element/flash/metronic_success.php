<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-brand alert-dismissible fade show   m-alert m-alert--square m-alert--air" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
	<strong>
		Success!
	</strong>
	<?= $message ?>
</div>
