<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $actionTitle . " List",
        ['controller' => 'users', 'action' => 'index'],
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ],
            'templateVars'  =>  [
                'beforeTitle'   =>  '<span class="m-nav__link-text">',
                'afterTitle'    =>  '</span>'
            ]
        ]
    );


    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>
<style>
#map {
width: 100%;
height: 400px;
z-index:100;
}
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <?= $this->Form->create($customer, ['class' =>  'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'type'   =>  'file']) ?>
        <?php
            $this->Form->setTemplates([
                'inputContainer'        => '<div class="form-group m-form__group row">{{content}}</div>',
                'inputContainerError'   => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                'label'             => '<label class="col-lg-2 col-form-label">{{text}}</label>',
                'input'             => '<div class="col-lg-6"><input type="{{type}}" name="{{name}}"{{attrs}}/></div>',
                'select'            =>  '<div class="col-lg-6"><select name="{{name}}"{{attrs}}>{{content}}</select></div>',
                'error' => '<div class="col-lg-6"></div><div class="col-lg-6"><span class="m-form__help error">{{content}}</span></div>',
            ]);
        ?>
            <div class="m-portlet__body">
                <?php
                    echo $this->Form->control('name', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter name',
                    ]);
                    echo $this->Form->control('address', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter Address',
                    ]);
                    echo $this->Form->control('no_telp', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter No Telpon',
                    ]);
                    echo $this->Form->control('lat', [
                        'type' =>  'hidden',
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter Lat',
                    ]);
                    echo $this->Form->control('lng', [
                        'type' =>  'hidden',
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter Lng',
                    ]);
                ?>
                <div id="map"></div>



            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <?= $this->Form->button(__('Submit'), [
                                'class' =>  'btn btn-primary'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?= $this->Form->end() ?>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */

?>

<?php
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
    <script type="text/javascript">
        // Initialize the map and assign it to a variable for later use
        var map = L.map('map', {
          // Set latitude and longitude of the map center (required)
          center: [-6.2088,106.8456],
          // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
          zoom: 10
        });
        L.control.scale().addTo(map);

        // Create a Tile Layer and add it to the map
        //var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var searchControl = new L.esri.Controls.Geosearch().addTo(map);
        var marker;
        var results = new L.LayerGroup().addTo(map);
        var geocoder = L.Control.Geocoder.nominatim();
        var lat = '<?=$customer->lat?>';
        var lng = '<?=$customer->lng?>';
        var address;

        marker = L.marker([lat,lng], {
          draggable: true
        }).bindTooltip('',
                                                        {
                                                            permanent : true,
                                                            direction: 'top',
                                                            offset :  L.point(-10, 0)
                                                        }).on('dragend', onDragEnd);
        var settings_getv = {

                      "crossDomain": true,
                      "dataType": 'json',
                      "url": "https://nominatim.openstreetmap.org/reverse?lat="+lat+"&lon="+lng+"&zoom=15&addressdetails=1&format=json",
                      "method": "GET",
                    }


                    $.ajax(settings_getv).done(function (response_getv) {
                      console.log("response_getv");
                      address = response_getv.display_name;
                          var popup = marker.getTooltip();
                          popup.setContent(address);
                          var lat_lng = marker.getLatLng();
                          map.fitBounds([
                                lat_lng
                              ]);
                    });





    // geocoder.reverse(marker.getLatLng(), map.options.crs.scale(map.getZoom()), function(results) {
    //   var r = results[0];
    //   if (r) {
    //     var popup = marker.getTooltip();
    //     console.log(popup);
    //     popup.setContent(r.name);
    //     loc = r.name;
    //     map.fitBounds([
    //           lat_lng
    //         ]);
    //   }
    // });
    results.addLayer(marker);
    // console.log("marker.getLatLng");
    // console.log(marker.getLatLng);
    // initMarker(lat,lng);
    // function initMarker(lat,lng){
    //
    //   marker = L.marker([<?=$customer->lat?>,<?=$customer->lng?>], {
    //   draggable: true
    //   }).bindTooltip(''+ data.results[i].address,
    //                                                 {
    //                                                     permanent : true,
    //                                                     direction: 'top',
    //                                                     offset :  L.point(-10, 0)
    //                                                 }).on('dragend', onDragEnd);
    // }


    console.log("geocoder");
    console.log(geocoder);
    searchControl.on('results', function(data){
      results.clearLayers();
      for (var i = data.results.length - 1; i >= 0; i--) {
        console.log(data.results[i].address);
        var ltlg = data.results[i].latlng;
        var loc = data.results[i].address;
        displayLatLng(ltlg);
        marker = L.marker(data.results[i].latlng, {
        draggable: true
        }).bindTooltip(''+ data.results[i].address,
                                                      {
                                                          permanent : true,
                                                          direction: 'top',
                                                          offset :  L.point(-10, 0)
                                                      }).on('dragend', onDragEnd);
                                                      console.log(results);
        results.addLayer(marker);

      }


    });

    // map.on('click', function(e) {
    //     results.clearLayers();
    //   geocoder.reverse(e.latlng, map.options.crs.scale(map.getZoom()), function(results) {
    //     var r = results[0];
    //     if (r) {
    //       if (marker) {
    //         marker
    //           .setLatLng(r.center)
    //           .setPopupContent(r.html || r.name)
    //           .openPopup();
    //       } else {
    //         marker =new L.marker(r.center, {
    //             draggable: true
    //         })
    //           .bindPopup(r.name)
    //           .addTo(map)
    //           .openPopup().on('dragend', onDragEnd);
    //       }
    //     }
    //   });
    // });
    var   latInput = document.getElementById('lat');
     var   lngInput = document.getElementById('lng');

    function onDragEnd(event) {
        var latlng = event.target.getLatLng();
        console.log("latlng");
        console.log(latlng);
        var loc;
          geocoder.reverse(latlng, map.options.crs.scale(map.getZoom()), function(results) {
            var r = results[0];
            if (r) {
              var popup = event.target.getTooltip();
              console.log(popup);
              popup.setContent(r.name);
              loc = r.name;
            }
          });
    		console.log(latlng);
        displayLatLng(latlng);
    }

    function displayLatLng(latlng) {
        latInput.value = latlng.lat;
        lngInput.value = latlng.lng;
    }


    </script>

    <?php
    $this->end();
?>
