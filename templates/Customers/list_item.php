
<div class="row" >
                      <div class="col-12">
                          <div class="card">
                              <div class="card-header">

                              

                              </div><!--end card-header-->

                              <div class="card-body">
                                <!-- <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="sa-warning">Click me</button> -->
                                  <table id="datatable-buttons" class="table table-striped m-table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                      <thead>
                                      <tr>

                                          <th>ID</th>
                                          <th>Name</th>
                                          <th>Address</th>
                                          <th>No tlp</th>
                                          <th>Created</th>
                                          <th>
                                            <?= __('Actions') ?>
                                          </th>
                                      </tr>
                                      </thead>


                                      <tbody>

                                      <?php foreach ($customers as $customer): ?>

          <tr scope="row">

              <td><?= $this->Number->format($customer->id) ?></td>

              <td><?= h($customer->name) ?></td>
              <td><?= h($customer->address) ?></td>
              <td><?= h($customer->no_telp) ?></td>

              <td><?= h($customer->created) ?></td>

              <td class="actions">
                  <?= $this->Html->link(__('View'), ['action' => 'view', $customer->id]) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id]) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
              </td>
                                      </tr>
                                        <?php endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div> <!-- end col -->
                  </div> <!-- end row -->
