<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
		Backend
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

		<!--end::Web font -->
        <!--begin::Base Styles -->
				<script>
							WebFont.load({
								google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
								active: function() {
										sessionStorage.fonts = true;
								}
							});
				</script>
		<?php
			echo $this->Html->css('/assets/css/css/bootstrap.min.css');
			echo $this->Html->css('/assets/css/css/icons.min.css');
			echo $this->Html->css('/assets/css/css/app.min.css');

		?>
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="/assets/demo/default/media/img/logo/favicon.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="account-body accountbg"  >
		<?= $this->fetch('content') ?>
    	<!--begin::Base Scripts -->
    	<?php
    		echo $this->Html->script('/assets/css/js/jquery.min.js');
    		echo $this->Html->script('/assets/css/js/bootstrap.bundle.min.js');
    		echo $this->Html->script('/assets/css/js/waves.js');
    		echo $this->Html->script('/assets/css/js/feather.min.js');
    		echo $this->Html->script('/assets/css/js/simplebar.min.js');

    		// echo $this->Html->script('/assets/snippets/custom/pages/user/login.js');
    		echo $this->fetch('script');
    	?>
	</body>
	<!-- end::Body -->
</html>
