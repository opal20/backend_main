<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<?= $this->Html->charset() ?>
		<title>
			<?= $this->fetch('title') ?>
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>

		<!-- Plugins css -->
		<link href="/assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css" />
		<link href="/assets/plugins/timepicker/bootstrap-material-datetimepicker.css" rel="stylesheet">
		<link href="/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

		<!-- App css -->
		<link href="/assets/css/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/css/icons.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
		<link href="/assets/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="/assets/css/css/app.min.css" rel="stylesheet" type="text/css" />

		<!-- css maps picker -->
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css" />

		<link href="/assets/picker/src/leaflet-locationpicker.css" rel="stylesheet" type="text/css" />

		<?php
			echo $this->Html->script('/assets/vendors/base/vendors.bundle.js');
    		echo $this->Html->script('/assets/demo/default/base/scripts.bundle.js');
		?>
		<?php
			echo $this->Html->css('/assets/vendors/base/vendors.bundle');
			echo $this->Html->css('/assets/demo/default/base/style.bundle');
		?>
		<!--end::Web font -->
        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->
		<!--end::Page Vendors -->

		<?php

			// echo $this->Html->css('/assets/css/css/bootstrap.min.css');
			// echo $this->Html->css('/assets/css/css/icons.min.css');
			// echo $this->Html->css('/assets/css/css/metisMenu.min.css');
			// echo $this->Html->css('/assets/plugins/daterangepicker/daterangepicker.css');
		?>
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="/assets/demo/default/media/img/logo/favicon.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body>
		<!-- begin:: Page -->



			<!-- begin::LEFT SIDENAF -->
			<div class="left-sidenav">
				<?php
					echo $this->element('metronic_left_menu');
				?>
			</div>
				<!-- END::LEFT SIDENAF -->
			<div class="page-wrapper">
				<!-- topbar BEGIN-->
					<div class="topbar">
						<?php
							echo $this->element('metronic_header', [
								'loggedInUser'	=>	$loggedInUser
							]);
						?>
					</div>
						<!-- topbar END-->
					<div class="page-content">
							<?= $this->fetch('content') ?>
					</div>
						<!-- content END-->
				</div>
				<!-- jQuery  -->
				<script src="/assets/css/js/jquery.min.js"></script>
				<script src="/assets/css/js/bootstrap.bundle.min.js"></script>
				<script src="/assets/css/js/metismenu.min.js"></script>
				<script src="/assets/css/js/waves.js"></script>
				<script src="/assets/css/js/feather.min.js"></script>
				<script src="/assets/css/js/simplebar.min.js"></script>
				<script src="/assets/css/js/moment.js"></script>
				<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>

				<!-- Plugins js -->
				<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
				<script src="/assets/plugins/select2/select2.min.js"></script>
				<script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
				<script src="/assets/plugins/timepicker/bootstrap-material-datetimepicker.js"></script>
				<script src="/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
				<script src="/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

				<script src="/assets/pages/jquery.forms-advanced.js"></script>


				<!-- App js -->
				<script src="/assets/js/app.js"></script>

    	<?php

    		echo $this->fetch('script');
    		echo $this->fetch('scriptBottom');
    	?>

		<!--end::Base Scripts -->
        <!--begin::Page Vendors -->
		<!--end::Page Vendors -->
        <!--begin::Page Snippets -->
		<!-- <script src="/assets/app/js/dashboard.js" type="text/javascript"></script> -->
		<!--end::Page Snippets -->
	</body>
	<!-- end::Body -->
</html>
