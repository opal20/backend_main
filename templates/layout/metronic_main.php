<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<?= $this->Html->charset() ?>
		<title>
			<?= $this->fetch('title') ?>
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>


		<!--end::Web font -->
        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->
		<!--end::Page Vendors -->
		<link rel="stylesheet" href="/assets/css/css/bootstrap.min.css" type="text/css"/>
		<link rel="stylesheet" href="/assets/css/css/icons.min.css" type="text/css"/>
		<link rel="stylesheet" href="/assets/css/css/metisMenu.min.css" type="text/css" />
		<link rel="stylesheet" href="/assets/plugins/daterangepicker/daterangepicker.css" type="text/css" />
		<link rel="stylesheet" href="/assets/css/css/app.min.css" type="text/css" />
		<link rel="stylesheet" href="/assets/plugins/sweet-alert2/sweetalert2.min.css"  type="text/css">
		<link rel="stylesheet" href="/assets/plugins/timepicker/bootstrap-material-datetimepicker.css"  type="text/css">
    <link rel="stylesheet" href="/assets/plugins/animate/animate.css"  type="text/css">
		<!-- leaflet -->
		<link rel="stylesheet"	href="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.0/MarkerCluster.Default.min.css" />
		<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />

		<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js"></script>
	 <!-- CSS and JS files for Search Box -->
		<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet/0.0.1-beta.5/esri-leaflet.js"></script>

		<script src="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.js"></script>
		<script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>

		<link rel="stylesheet" type="text/css" href="https://cdn-geoweb.s3.amazonaws.com/esri-leaflet-geocoder/0.0.1-beta.5/esri-leaflet-geocoder.css">

		<?php

			// echo $this->Html->css('/assets/css/css/bootstrap.min.css');
			// echo $this->Html->css('/assets/css/css/icons.min.css');
			// echo $this->Html->css('/assets/css/css/metisMenu.min.css');
			// echo $this->Html->css('/assets/plugins/daterangepicker/daterangepicker.css');
		?>
		<?php
			echo $this->Html->script('/assets/vendors/base/vendors.bundle.js');
    		echo $this->Html->script('/assets/demo/default/base/scripts.bundle.js');
		?>
		<?php
			echo $this->Html->css('/assets/vendors/base/vendors.bundle');
			echo $this->Html->css('/assets/demo/default/base/style.bundle');
		?>
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="/assets/demo/default/media/img/logo/favicon.png" />
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body>
		<!-- begin:: Page -->



			<!-- begin::LEFT SIDENAF -->
			<div class="left-sidenav">
				<?php
					echo $this->element('metronic_left_menu');
				?>
			</div>
				<!-- END::LEFT SIDENAF -->
			<div class="page-wrapper">
				<!-- topbar BEGIN-->
					<div class="topbar">
						<?php
							echo $this->element('metronic_header', [
								'loggedInUser'	=>	$loggedInUser
							]);
						?>
					</div>
						<!-- topbar END-->
					<div class="page-content">
							<?= $this->fetch('content') ?>
					</div>
						<!-- content END-->
				</div>
				<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
				<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>
			<?php
				echo $this->Html->script('/assets/plugins/sweet-alert2/sweetalert2.min.js');
				echo $this->Html->script('/assets/pages/jquery.sweet-alert.init.js');
				echo $this->Html->script('/assets/pages/jquery.forms-advanced.js');
				echo $this->Html->script('/assets/css/js/jquery.min.js');
	    	echo $this->Html->script('/assets/css/js/bootstrap.bundle.min.js');
				echo $this->Html->script('/assets/css/js/metismenu.min.js');
				echo $this->Html->script('/assets/css/js/waves.js');
				echo $this->Html->script('/assets/css/js/feather.min.js');
				echo $this->Html->script('/assets/css/js/simplebar.min.js');
				echo $this->Html->script('/assets/css/js/moment.js');
				echo $this->Html->script('/assets/plugins/daterangepicker/daterangepicker.js');
				echo $this->Html->script('/assets/plugins/datatables/jquery.dataTables.min.js');
				echo $this->Html->script('/assets/plugins/datatables/dataTables.bootstrap4.min.js');
				echo $this->Html->script('/assets/plugins/datatables/dataTables.buttons.min.js');
				echo $this->Html->script('/assets/plugins/datatables/buttons.bootstrap4.min.js');
				echo $this->Html->script('/assets/plugins/datatables/jszip.min.js');
				echo $this->Html->script('/assets/plugins/datatables/pdfmake.min.js');
				echo $this->Html->script('/assets/plugins/datatables/vfs_fonts.js');
				echo $this->Html->script('/assets/plugins/datatables/buttons.html5.min.js');
				echo $this->Html->script('/assets/plugins/datatables/buttons.print.min.js');
				echo $this->Html->script('/assets/plugins/datatables/buttons.colVis.min.js');
				echo $this->Html->script('/assets/plugins/timepicker/bootstrap-material-datetimepicker.js');

				// echo $this->Html->script('/assets/plugins/datatables/responsive.bootstrap4.min.js');
				echo $this->Html->script('/assets/pages/jquery.datatable.init.js');
				//
				//
				//
				//
				// echo $this->Html->script('/assets/plugins/apex-charts/apexcharts.min.js');
				// echo $this->Html->script('/assets/pages/jquery.analytics_dashboard.init.js');
				echo $this->Html->script('/assets/css/js/app.js');
			?>
    	<?php

    		echo $this->fetch('script');
    		echo $this->fetch('scriptBottom');
    	?>

		<!--end::Base Scripts -->
        <!--begin::Page Vendors -->
		<!--end::Page Vendors -->
        <!--begin::Page Snippets -->
		<!-- <script src="/assets/app/js/dashboard.js" type="text/javascript"></script> -->
		<!--end::Page Snippets -->
	</body>
	<!-- end::Body -->
</html>
