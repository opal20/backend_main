<?php

	function printMenu($menus, $firstLevel = true, $activeParentMenuId = false) : string {

		$arrayMenu = array();

		$printString 	=	"";

		foreach($menus as $menu) {

			// cek apakah first level atau second level.

			if($firstLevel && count($menu->children) < 1 ) {

				$tempIconClass	=	"";
				if(!empty($menu->icon_class)) {
					$tempIconClass	=	$menu->icon_class;
				}

				if(!empty($menu->aco)) {

					$UrlHelper 	=	new Cake\View\Helper\UrlHelper(new \Cake\View\View());
					$tempUrl	=	$UrlHelper->build(
						[
							'controller'	=>	$menu->aco['controller'],
							'action'		=>	$menu->action
						]
					);
				} else {
					$tempUrl = "#";
				}

				$menuActive 	=	"";

				if($menu->id 	==	$activeParentMenuId) {
					$menuActive 	=	"m-menu__item--active";
				}

				// Tambahkan class m-menu__item--active untuk active menu.
				$printString .= 	'
				<li>
								<a  href="'.$tempUrl.'" class="align-self-center menu-icon">
									<i  class="' . $tempIconClass . '"></i>
									<span>' . $menu->name . '</span>
								</a>
				</li>';

			} else if($firstLevel && count($menu->children)	>	0) {



				$tempIconClass	=	"";
				if(!empty($menu->icon_class)) {
					$tempIconClass	=	$menu->icon_class;
				}

				$menuActive 	=	"";

				if($menu->id 	==	$activeParentMenuId) {
					$menuActive 	=	"m-menu__item--active m-menu__item--open m-menu__item--expanded";
				}

				$printString .=	'
				<!-- LI  BEGIN FOR SECOND MENU-->
				<li>
								<a  href="javascript: void(0);" class="align-self-center menu-icon">
									<i  class="' . $tempIconClass . '"></i>
									<span>' . $menu->name . '</span>
									<span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
									<!-- UL SECOND BEGIN-->
	                        <ul class="nav-second-level" aria-expanded="false">
							';

				$printString .= printMenu($menu->children, false);

				$printString .= '
								</ul>	<!-- UL SECOND END-->
				</li>
				<!-- LI  EMD-->';

			} else {



				$tempIconClass	=	"";
				if(!empty($menu->icon_class)) {
					$tempIconClass	=	$menu->icon_class;
				}

				if(!empty($menu->aco)) {

					$UrlHelper 	=	new Cake\View\Helper\UrlHelper(new \Cake\View\View());
					$tempUrl	=	$UrlHelper->build(
						[
							'controller'	=>	$menu->aco['controller'],
							'action'		=>	$menu->action
						]
					);

				} else {
					$tempUrl = "#";
				}

				$printString .= '
						<!-- LI  ITEM SECOND MENU-->
										<li class="nav-item" aria-haspopup="true" >
											<a  href="'. $tempUrl .'" class="nav-link">
												<i class="ti-control-record">
													<span></span>
												</i>

													' . $menu->name . '

											</a>
										</li>
						<!-- LI  ITEM SECOND MENU END-->';

			}
		}

		return $printString;

	}

?>
<!-- UL  MATIS MENU BEGIN-->
<ul class="metismenu left-sidenav-menu">
	<?php
		echo printMenu($cmsMenus, true, $activeParentMenuId);
	?>
</ul>
<!-- UL  MATIS MENU END-->
