<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aco $aco
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Aco'), ['action' => 'edit', $aco->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Aco'), ['action' => 'delete', $aco->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aco->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Acos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Aco'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="acos view content">
            <h3><?= h($aco->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Parent Aco') ?></th>
                    <td><?= $aco->has('parent_aco') ? $this->Html->link($aco->parent_aco->id, ['controller' => 'Acos', 'action' => 'view', $aco->parent_aco->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Acos Type') ?></th>
                    <td><?= $aco->has('acos_type') ? $this->Html->link($aco->acos_type->name, ['controller' => 'AcosTypes', 'action' => 'view', $aco->acos_type->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Model') ?></th>
                    <td><?= h($aco->model) ?></td>
                </tr>
                <tr>
                    <th><?= __('Controller') ?></th>
                    <td><?= h($aco->controller) ?></td>
                </tr>
                <tr>
                    <th><?= __('Alias') ?></th>
                    <td><?= h($aco->alias) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($aco->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lft') ?></th>
                    <td><?= $this->Number->format($aco->lft) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rght') ?></th>
                    <td><?= $this->Number->format($aco->rght) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($aco->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($aco->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($aco->modified) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($aco->description)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Aros') ?></h4>
                <?php if (!empty($aco->aros)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Parent Id') ?></th>
                            <th><?= __('Model') ?></th>
                            <th><?= __('Foreign Key') ?></th>
                            <th><?= __('Alias') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Lft') ?></th>
                            <th><?= __('Rght') ?></th>
                            <th><?= __('Total Admin') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($aco->aros as $aros) : ?>
                        <tr>
                            <td><?= h($aros->id) ?></td>
                            <td><?= h($aros->parent_id) ?></td>
                            <td><?= h($aros->model) ?></td>
                            <td><?= h($aros->foreign_key) ?></td>
                            <td><?= h($aros->alias) ?></td>
                            <td><?= h($aros->description) ?></td>
                            <td><?= h($aros->lft) ?></td>
                            <td><?= h($aros->rght) ?></td>
                            <td><?= h($aros->total_admin) ?></td>
                            <td><?= h($aros->status) ?></td>
                            <td><?= h($aros->created) ?></td>
                            <td><?= h($aros->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Aros', 'action' => 'view', $aros->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Aros', 'action' => 'edit', $aros->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Aros', 'action' => 'delete', $aros->id], ['confirm' => __('Are you sure you want to delete # {0}?', $aros->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <h4><?= __('Related Acos') ?></h4>
                <?php if (!empty($aco->child_acos)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Parent Id') ?></th>
                            <th><?= __('Acos Type Id') ?></th>
                            <th><?= __('Model') ?></th>
                            <th><?= __('Controller') ?></th>
                            <th><?= __('Alias') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Lft') ?></th>
                            <th><?= __('Rght') ?></th>
                            <th><?= __('Status') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($aco->child_acos as $childAcos) : ?>
                        <tr>
                            <td><?= h($childAcos->id) ?></td>
                            <td><?= h($childAcos->parent_id) ?></td>
                            <td><?= h($childAcos->acos_type_id) ?></td>
                            <td><?= h($childAcos->model) ?></td>
                            <td><?= h($childAcos->controller) ?></td>
                            <td><?= h($childAcos->alias) ?></td>
                            <td><?= h($childAcos->description) ?></td>
                            <td><?= h($childAcos->lft) ?></td>
                            <td><?= h($childAcos->rght) ?></td>
                            <td><?= h($childAcos->status) ?></td>
                            <td><?= h($childAcos->created) ?></td>
                            <td><?= h($childAcos->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Acos', 'action' => 'view', $childAcos->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Acos', 'action' => 'edit', $childAcos->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Acos', 'action' => 'delete', $childAcos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childAcos->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
