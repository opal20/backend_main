<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aco $aco
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Acos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="acos form content">
            <?= $this->Form->create($aco) ?>
            <fieldset>
                <legend><?= __('Add Aco') ?></legend>
                <?php
                    echo $this->Form->control('parent_id', ['options' => $parentAcos, 'empty' => true]);
                    echo $this->Form->control('acos_type_id', ['options' => $acosTypes]);
                    echo $this->Form->control('controller', [
                        'options'   =>  $controllerList,
                        'empty'     =>  "Please choose"
                    ]);
                    echo $this->Form->control('description');
                    echo $this->Form->control('status');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
