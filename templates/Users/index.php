<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
               <?= $this->Html->link(__('<button class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-plus"></i></button> Add User'),
                    [
                        'action' => 'add'
                    ],
                    [
                        'class'    =>  '',
                        'escape'    =>  false,
                        'title'     =>  'Add User'
                    ]
                ) ?>
            </div>
        </div>
        <!-- END Portlet head -->
        <!-- <div class="m-portlet__body">
            <!--begin: Search Form -->
            <!-- <?= $this->Form->create(null, array('type' =>  'get')) ?> -->
                <!-- <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-4">
                                    <?=
                                        $this->Form->control('aro_id', [
                                            'label' => 'User Role',
                                            'options' => $arosList,
                                            'empty'   =>  'All Role',
                                            'class'     =>  'custom-select form-control',
                                            'value'         =>  $this->request->getQuery('aro_id'),
                                            'templateVars'  =>  [
                                                'divClass'  =>  'form-group m-form__group'
                                            ]
                                        ])
                                    ?>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <div class="col-md-4">
                                    <?= $this->Form->control('email',
                                        [
                                            'label'         =>  "Email",
                                            'required'      =>  false,
                                            // 'after'     =>  '</div>',
                                            'class'         =>  'form-control m-input',
                                            'type'          =>  'text',
                                            'placeholder'   =>  'Email',
                                            'value'         =>  $this->request->getQuery('email'),
                                            'templateVars'  =>  [
                                                'divClass'  =>  "form-group m-form__group"
                                            ]
                                        ])
                                    ?>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-2 order-xl-2 m--align-right">
                            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Search
                            </button>
                            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Reset
                            </button>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div> -->
            <!-- <?php echo $this->Form->end(); ?> -->
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div id="listItemContent">
                <?php echo $this->element('../Users/list_item'); ?>
            </div>

            <!--end: Datatable -->
        </div>
        <!-- END PORTLET Body -->
    </div>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
        <script type="text/javascript">
        function reloadListItemContent() {
              var thisHref ="/users";;
              $('#listItemContent').fadeTo(300, 0);
              $('#listItemContent').load(thisHref, function() {
                  $(this).fadeTo(200, 1);
              });
          }

        </script>
    <?php
    $this->end();
?>
