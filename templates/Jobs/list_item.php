
<div class="row" >
                      <div class="col-12">
                          <div class="card">
                              <div class="card-header">



                              </div><!--end card-header-->

                              <div class="card-body">
                                <!-- <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="sa-warning">Click me</button> -->
                                  <table id="datatable-buttons" class="table table-striped m-table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                      <thead>
                                      <tr>

                                          <th>ID</th>

                                          <th>Job Type</th>
                                          <th>Job Status</th>
                                          <th>Product Name</th>
                                          <th>Customer Name</th>
                                          <th>Due Date</th>
                                          <th>
                                            <?= __('Actions') ?>
                                          </th>
                                      </tr>
                                      </thead>


                                      <tbody>

                                      <?php foreach ($jobs as $job): ?>

          <tr scope="row">

              <td><?= $this->Number->format($job->id) ?></td>
              <td><?= h($job->job_type->name) ?></td>
              <td><?= h($job->job_status->name) ?></td>
              <td><?= h($job->product->name) ?></td>
              <td><?= h($job->customer->name) ?></td>
              <td><?php
              $time = $job->date;


              echo $time->i18nFormat('dd-MMM-yyyy'); ?></td>


              <td class="actions">
                <?php
                if($job->job_status->id == 1){


                 ?>
                  <?= $this->Html->link(__('Jobs To Employes'), ['action' => 'managedJobEmployee', $job->id]) ?>
                  <?php
                }else{


                 ?>
                 <?= $this->Html->link(__('Tracking'), ['action' => 'managedJobEmployee', $job->id]) ?>
               <?php } ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $job->id]) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $job->id], ['confirm' => __('Are you sure you want to delete # {0}?', $job->id)]) ?>
              </td>
                                      </tr>
                                        <?php endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div> <!-- end col -->
                  </div> <!-- end row -->
                  <script type="text/javascript">
                      $(document).ready(function() {

                          // kalau check all, semua checkbox di checked
                          $("#check_all").click(function(){
                              $('input:checkbox').not(this).prop('checked', this.checked);
                          });

                          $("#deleteButton").click(function(){

                              var $form       =   $('#submitDeleteForm'),
                                  url         =   $form.attr('action'); // url nya ambil dari url form diatas
                              var data        =   $form.serialize();

                              swal.fire(
                                  {title: "Are you sure?",
                                  text: "You won't be able to revert this!",
                                  type: "warning",
                                  showCancelButton: !0,
                                  confirmButtonText: "Yaa, Hapus saja!",
                                  cancelButtonText: "Tidak, Batalkan!",
                                  reverseButtons: !0}).then(function(e){
                                          if(typeof e.value !== 'undefined' && e.value == true) {

                                              $.ajax({
                                                type: "POST",
                                                url: url,
                                                data: data,
                                                success: function(data){
                                                  if (typeof data.status !== 'undefined' && data.status == 1) {
                                                      swal.fire("Deleted!","User berhasil di hapus.","success").then(function(e){
                                                          reloadListItemContent();
                                                      });
                                                  } else {
                                                      alert("Please try again");
                                                  }
                                                }
                                              });
                                          }else{
                                            e.dismiss === Swal.DismissReason.cancel && swal.fire("Batal", "Data User dibatalkan untuk di hapus)", "error")
                                          }
                                      }
                                  );

                          });

                          // untuk apabila mao hapus semua.
                          $("#submitDeleteForm").submit(function(event){
                              event.preventDefault();
                          });

                      });
                      function reloadListItemContent() {
                            var thisHref ="/users";;
                            $('#listItemContent').fadeTo(300, 0);
                            $('#listItemContent').load(thisHref, function() {
                                $(this).fadeTo(200, 1);
                            });
                        }
                  </script>
