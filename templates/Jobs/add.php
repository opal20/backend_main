<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $actionTitle . " List",
        ['controller' => 'users', 'action' => 'index'],
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ],
            'templateVars'  =>  [
                'beforeTitle'   =>  '<span class="m-nav__link-text">',
                'afterTitle'    =>  '</span>'
            ]
        ]
    );


    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <?= $this->Form->create($job, ['class' =>  'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'type'   =>  'file']) ?>
        <?php
            $this->Form->setTemplates([
                'inputContainer'        => '<div class="form-group m-form__group row">{{content}}</div>',
                'inputContainerError'   => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                'label'             => '<label class="mb-3">{{text}}</label>',
                'input'             => '<input type="{{type}}" name="{{name}}"{{attrs}}/>',
                'select'            =>  '<select name="{{name}}"{{attrs}}>{{content}}</select>',
                'error' => '<div class="col-lg-6"></div><div class="col-lg-6"><span class="m-form__help error">{{content}}</span></div>',
            ]);
        ?>
            <div class="m-portlet__body">
              <div class="row">
                  <div class="col-lg-6">
                      <div class="card">
                          <div class="card-header">
                              <h4 class="card-title">Instalation Form</h4>
                              <p class="text-muted mb-0">Put all field to complited Submit Form</p>
                          </div><!--end card-header-->
                          <div class="card-body">
                              <div class="form-material">
                                <?php
                                echo $this->Form->control('date', [
                                    'type' => 'text',
                                    'class' =>  'form-control',
                                    'placeholder'   =>  'Enter Due date',
                                    'id' => 'mdate'
                                ]);
                                echo $this->Form->control('job_type_id', [
                                      'label' => 'Job List',
                                      'options' => $jobTypeList,
                                      'empty'   =>  'Please choose',
                                      'class' =>  'custom-select form-control',
                                      'onchange'  =>  "ChangeService(this.value, 'service-id')"
                                  ]);

                                echo $this->Form->control('service_id', [
                                        'label' => 'Service',
                                        'options' => array(),
                                        'empty'   =>  'Please choose',
                                        'class' =>  'custom-select form-control',
                                        'onchange'  =>  "ChangeProduct(this.value, 'product-id')"


                                    ]);
                                echo $this->Form->control('product_id', [
                                            'label' => 'Product',
                                            'options' => array(),
                                            'empty'   =>  'Please choose',
                                            'class' =>  'custom-select form-control',


                                        ]);
                                        ?>
                                        <div id="customer-list" style="display: none">
                                        <?php
                                echo $this->Form->control('customer_id', [
                                              'label' => 'Customer List',
                                              'options' => $customerList,
                                              'empty'   =>  'Please choose',
                                              'class' =>  'custom-select form-control',

                                          ]);
                                 ?>
                               </div>


                              </div>
                          </div><!--end card-body-->
                      </div><!--end card-->
                  </div> <!-- end col -->

                    <!-- <div class="col-lg-6" id="customer" style="display: none"> -->
                      <div class="col-lg-6" id="customer" style="display: none">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Date Range Picker</h4>
                                <p class="text-muted mb-0">A JavaScript component for choosing date ranges, dates and times.</p>
                            </div><!--end card-header-->
                            <div class="card-body">
                              <?php



                                  echo $this->Form->control('customer.name', [
                                      'class' =>  'form-control m-input',
                                      'placeholder'   =>  'Enter Name',
                                  ]);

                                  echo $this->Form->control('customer.address', [
                                      'class' =>  'form-control m-input',
                                      'placeholder'   =>  'Enter spec',
                                  ]);
                                  echo $this->Form->control('customer.no_telp', [
                                      'class' =>  'form-control m-input',
                                      'placeholder'   =>  'Enter no telephone',
                                  ]);
                                  echo $this->Form->input('events', [
                                      // 'type'  =>  'hidden',
                                      'value' =>  $valueLongLat,
                                      'id'    => 'geomaps'
                                  ]);
                                  ?>
                                  <i>Click Here to show Maps</i>
                                  <?php
                                  echo $this->Form->control('customer.lat', [
                                      'label' => 'Lattitude',
                                      'type' => 'text',
                                      'class' =>  'form-control m-input',
                                      'placeholder'   =>  '',
                                  ]);
                                  echo $this->Form->control('customer.lng', [
                                      'label' => 'Longitude',
                                      'type' => 'text',
                                      'class' =>  'form-control m-input',
                                      'placeholder'   =>  '',
                                  ]);







                              ?>
                              	<div id="fixedMapCont1" style="border: 1px solid black; height: 340px;min-width: 200;"></div>
                            </div><!--end card-body-->
                        </div><!--end card-->
                    </div><!--end col-->

              </div><!--end row-->

            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <?= $this->Form->button(__('Submit'), [
                                'class' =>  'btn btn-primary'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?= $this->Form->end() ?>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
    <script src="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="http://cdn.leafletjs.com/leaflet-0.7/leaflet.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="/assets/picker/src/leaflet-locationpicker.js"></script>
    <script>
    $('#geomaps').leafletLocationPicker({
        locationSep: ' - ',
        alwaysOpen: false,
        mapContainer: "#fixedMapCont1",
        height:340,
        map:{
          zoom : 13,
        }
      })
      .on('show', function(e) {
        $(this).siblings('em').text('click on map for insert the location');
      })
      .on('hide', function(e) {
        $(this).siblings('em').text('');
      })
      .on('changeLocation', function(e) {
        $('#customer-lat').val(e.latlng.lat);
        $('#customer-lng').val(e.latlng.lng);

      });
    </script>
    <script type="text/javascript">
      var _job_type_id ;
      var _service_id ;
      $(document).ready(function(){
        //events

        // $("#customer").hide();



        <?php
            if(isset($job->job_type_id)) {
                ?>
                    _job_type_id     =   <?= $job->job_type_id ?>;
                <?php
            }

            if(isset($job->service_id)) {
                ?>
                    _service_id     =   <?= $job->service_id ?>;
                <?php
            }



        ?>

        ChangeService(_job_type_id, 'job_type_id');
        ChangeProduct(_service_id, 'product_id');






      });

      function ChangeService(job_type_id, target) {

        if(job_type_id == 1){
            $("#customer").show();
            $("#customer-list").hide();

        }else if(job_type_id == 2){
          $("#customer").hide();
          $("#customer-list").show();
        }




        var optionData  =   '<option value="">Loading</option>';
        $("#" + target).html(optionData);

        $.getJSON('/Jobs/getServiceByJobTypeId/' + job_type_id + '.json',{},function(result){
            optionData  =   '<option value="">Select Service</option>';

            if( Object.keys(result.data).length > 0)
            {
                $.each(result.data,function(key,value){

                        optionData  +=  '<option value="'+key+'">'+value+'</option>';

                });



            }

            $("#" + target).html(optionData);


        });


      }
      function ChangeProduct(service_id, target) {



        var optionData  =   '<option value="">Loading</option>';
        $("#" + target).html(optionData);

        $.getJSON('/Jobs/getProductByServiceId/' + service_id + '.json',{},function(result){
            optionData  =   '<option value="">Select Product</option>';

            if( Object.keys(result.data).length > 0)
            {
                $.each(result.data,function(key,value){

                        optionData  +=  '<option value="'+key+'">'+value+'</option>';

                });



            }

            $("#" + target).html(optionData);


        });


      }
      function reloadMaps() {
            var thisHref ="/jobs/add";;
            $('#fixedMapCont1').fadeTo(300, 0);
            $('#fixedMapCont1').load(thisHref, function() {
                $(this).fadeTo(200, 1);
            });
        }

</script>
    <?php
    $this->end();
?>
