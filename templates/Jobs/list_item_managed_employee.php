<?= $this->Form->create(null, ['id' =>  'submitDeleteForm', 'url'   =>  '/products/deleteItems/' . $data->id . ".json"]) ?>
<?php
    echo $this->Form->input('Products.id', [
        'type'  =>  'hidden',
        'value' =>  $data->id
    ]);

?>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Employee List
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <div class="m-form__actions m-form__actions--solid">
                <div class="row">
                    <div class="col-lg-2">
                        &nbsp;
                    </div>
                    <div class="col-lg-2">
                        &nbsp;
                    </div>
                    <?php
                    if($data->employes == null){


                     ?>
                    <div class="col-lg-8">
                        <?= $this->Html->link(__('<button class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-plus"></i></button> Add Employee'),
                            [
                                'action' => 'managedEmployeeAdd', $data->id
                            ],
                            [
                                'class'    =>  '',
                                'escape'    =>  false,
                                'title'     =>  'Add Employee'
                            ]
                        )
                        ?>
                    </div>
                  <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="table-responsive">
          <?php if($data->employes == null){ ?>
            <?= $this->Form->button(__('Delete selection'), [
                        'class' =>  'btn btn-primary',
                        'id'    =>  'deleteButton'
                    ]) ?>
            <?php } ?>
            <table class="table table-striped m-table">
                <thead>
                    <tr>
                      <?php if($data->employes == null){ ?>
                        <th>
                            <?php

                                echo $this->Form->checkbox('check_all', [
                                    'hiddenField'   =>  false,
                                    'id'    =>  'check_all'
                                ]);

                            ?>
                        </th>
                      <?php } ?>
                        <th>
                          Employee ID
                        </th>
                        <th>
                          Employee Name
                        </th>
                        <th>
                          Employee Address
                        </th>
                        <?php if($data->employes == null){ ?>
                        <th>
                            Action
                        </th>
                      <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data->employes as $key => $value): ?>
                    <tr scope="row">
                      <?php
                        if($data->employes == null){
                       ?>
                        <td>
                            <!-- checkbox -->
                            <?php
                                echo $this->Form->checkbox('employes.'.$value->id.'.is_checked', [
                                    'hiddenField'   =>  false
                                ]);
                            ?>
                        </td>
                      <?php }  ?>
                        <td><?= $this->Number->format($value->id) ?></td>
                        <td><?= h($value->full_name) ?></td>

                        <td><?= h($value->address) ?></td>

                        <?php if($data->employes == null){ ?>
                        <td>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'deleteItem', $data->id, $value->id], ['confirm' => __('Are you sure you want to delete product # {0}?', $value->id)]) ?>
                        </td>
                      <?php } ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div> <!-- end table responsive -->
    </div><!-- end portlet body -->
</div>
<?= $this->Form->end() ?>
<script type="text/javascript">
    $(document).ready(function() {

        // kalau check all, semua checkbox di checked
        $("#check_all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $("#deleteButton").click(function(){

            var $form       =   $('#submitDeleteForm'),
                url         =   $form.attr('action'); // url nya ambil dari url form diatas
            var data        =   $form.serialize();

            swal.fire(
                {title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yaa, Hapus saja!",
                cancelButtonText: "Tidak, Batalkan!",
                reverseButtons: !0}).then(function(e){
                        if(typeof e.value !== 'undefined' && e.value == true) {

                            $.ajax({
                              type: "POST",
                              url: url,
                              data: data,
                              success: function(data){
                                if (typeof data.status !== 'undefined' && data.status == 1) {
                                    swal.fire("Deleted!","Services berhasil di hapus.","success").then(function(e){
                                        reloadListItemContent();
                                    });
                                } else {
                                    data.dismiss === Swal.DismissReason.cancel && swal.fire("Batal", "Data User dibatalkan untuk di hapus)", "error");
                                }
                              }
                            });
                        }else{
                          e.dismiss === Swal.DismissReason.cancel && swal.fire("Batal", "Data User dibatalkan untuk di hapus)", "error");
                        }
                    }
                );

        });

        // untuk apabila mao hapus semua.
        $("#submitDeleteForm").submit(function(event){
            event.preventDefault();
        });

    });
</script>
