<div class="table-responsive">
    <?= $this->Form->create(null, ['id' =>  'submitAddForm', 'url'   =>  '/jobs/managedEmployeeSubmitAdd/' . $data->id]) ?>
    <?php
        echo $this->Form->input('Jobs.id', [
            'type'  =>  'hidden',
            'value' =>  $data->id
        ]);

    ?>
    <table class="table table-striped m-table">
        <thead>
            <tr>
                <th>
                    <?php
                        echo $this->Form->checkbox('check_all', [
                            'hiddenField'   =>  false,
                            'id'    =>  'check_all'
                        ]);
                    ?>
                </th>
                <th>
                    Employee ID
                </th>
                <th>
                  Foto Pictures
                </th>
                <th>
                    Employee Name
                </th>
                <th>
                    Employee Address
                </th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($employes as $key => $value): ?>
            <tr scope="row">
                <td>
                    <!-- checkbox -->
                    <?php
                        echo $this->Form->checkbox('employes.'.$value->id.'.is_checked', [
                            'hiddenField'   =>  false
                        ]);
                    ?>
                </td>
                <td><?= $this->Number->format($value->id) ?></td>
                <?php
                    $urlImage   =   "/assets/no_user_image.png";
                    if(!empty($value->user->thumb)) {
                        $urlImage   =   $value->user->thumb->host . $value->user->thumb->url;

                    }
                ?>
                <td><img class="m--marginless m--img-centered" src="<?=$urlImage ?>" style="width: 50px;"></td>

                  <td><?= $value->full_name?></td>
                <td><?= $value->address?></td>

            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr scope="row">
                <td colspan="5">&nbsp;</td>
                <td>
                    <?= $this->Form->button(__('Submit'), [
                        'class' =>  'btn btn-primary',
                        'id'    =>  'submit'
                    ]) ?>
                </td>
            </tr>
        </tfoot>
        <?= $this->Form->end() ?>
    </table>
</div> <!-- end table responsive -->

<script type="text/javascript">
    $(document).ready(function(){
        $("#check_all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
