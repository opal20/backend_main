<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $actionTitle . " List",
        ['controller' => 'users', 'action' => 'index'],
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ],
            'templateVars'  =>  [
                'beforeTitle'   =>  '<span class="m-nav__link-text">',
                'afterTitle'    =>  '</span>'
            ]
        ]
    );


    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <?= $this->Form->create($product, ['class' =>  'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'type'   =>  'file']) ?>
        <?php
            $this->Form->setTemplates([
                'inputContainer'        => '<div class="form-group m-form__group row">{{content}}</div>',
                'inputContainerError'   => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                'label'             => '<label class="col-lg-2 col-form-label">{{text}}</label>',
                'input'             => '<div class="col-lg-6"><input type="{{type}}" name="{{name}}"{{attrs}}/></div>',
                'select'            =>  '<div class="col-lg-6"><select name="{{name}}"{{attrs}}>{{content}}</select></div>',
                'error' => '<div class="col-lg-6"></div><div class="col-lg-6"><span class="m-form__help error">{{content}}</span></div>',
            ]);
        ?>
            <div class="m-portlet__body">
                <?php



                    echo $this->Form->control('name', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter Name',
                    ]);

                    echo $this->Form->control('spec', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Enter spec',
                    ]);


                    echo $this->Form->control('imagefile', [
                        'type' => 'file'
                    ]);
                ?>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <?= $this->Form->button(__('Submit'), [
                                'class' =>  'btn btn-primary'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?= $this->Form->end() ?>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
        <script type="text/javascript">
        </script>
    <?php
    $this->end();
?>
