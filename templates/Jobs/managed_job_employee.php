<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        ['controller' => 'jobs', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                // 'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $data->job_type_with_product_name,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>
<style>
      .card {
      z-index: 0;
      background-color: #ECEFF1;
      padding-bottom: 20px;
      border-radius: 10px
      }

      .top {
        padding-top: 10px;
        padding-left: 7% !important;
        padding-right: 18% !important;
      }
      .top p {
        float: left;
        margin-left: -70px;
        margin-top: 70px;
      }
      #progressbar {
      margin-bottom: 30px;
      overflow: hidden;
      color: #455A64;
      padding-left: 0px;
      margin-top: 30px
      }

      #progressbar li {
      list-style-type: none;
      font-size: 13px;
      width: 15%;
      float: left;
      position: relative;
      font-weight: 400
      }

      #progressbar .step0:before {
      font-family: FontAwesome;
      content: "\f10c";
      color: #fff
      }

      #progressbar li:before {
      width: 40px;
      height: 40px;
      line-height: 45px;
      display: block;
      font-size: 20px;
      background: #C5CAE9;
      border-radius: 50%;
      margin: auto;
      padding: 0px
      }

      #progressbar li:after {
      content: '';
      width: 100%;
      height: 12px;
      background: #C5CAE9;
      position: absolute;
      left: 0;
      top: 16px;
      z-index: -1
      }

      #progressbar li:last-child:after {
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px;
      position: absolute;
      left: -50%
      }

      #progressbar li:nth-child(2):after,
      #progressbar li:nth-child(3):after,
      #progressbar li:nth-child(4):after,
      #progressbar li:nth-child(5):after {
      left: -50%
      }

      #progressbar li:first-child:after {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
      position: absolute;
      left: 50%
      }

      #progressbar li:last-child:after {
      border-top-right-radius: 10px;
      border-bottom-right-radius: 10px
      }

      #progressbar li:first-child:after {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px
      }

      #progressbar li.active:before,
      #progressbar li.active:after {
      background-color: #651FFF
      /* transition: 0.5s ease; */
      }

      #progressbar li.active:before {
      font-family: FontAwesome;
      content: "\f00c"
      }

      .icon {
      width: 60px;
      height: 60px;
      margin-right: 15px
      }

      .icon-content {
      padding-bottom: 20px

      }

      @media screen and (max-width: 992px) {
      .icon-content {
          width: 50%
      }
      }
      #center{
        margin-left: 50px;
      }
</style>
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
      <div class="col-xl-12">
          <!--begin:: Widgets/Company Summary-->
          <div class="m-portlet m-portlet--full-height ">
              <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                          <h3 class="m-portlet__head-text">
                              Job Tracking
                          </h3>
                      </div>
                  </div>
              </div>
              <div class="m-portlet__body">
                  <div class="m-widget13">
                    <div class="card">

          <div class="row d-flex justify-content-between px-3 top">
              <div class="d-flex">
                  <h5>Track Job For Customer <span class="text-primary font-weight-bold"><?= h($data->customer->name) ?></span></h5>
              </div>
            
          </div> <!-- Add class 'active' to progress -->
          <div id="center">
            <div class="row d-flex justify-content-center">
                <div class="col-12" id="status">
                    <ul id="progressbar" class="text-center">
                        <li id="step0" class="active step0"></li>
                        <li id="step1" class=" step0"></li>
                        <li id="step2" class=" step0"></li>
                        <li id="step3" class=" step0"></li>
                        <li id="step4" class=" step0"></li>
                        <li id="step5" class=" step0"></li>

                    </ul>
                </div>
            </div>
    <div class="row justify-content-between top">
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/created.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>Created</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/confirmed.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>Confirmed</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/on-progress.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>On Progrees</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/on-hold.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>On Hold</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/cancelled.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>Cancelled</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="/assets/completed.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Job<br>Comleted</p>
                    </div>
                </div>
            </div>
          </div>


      </div>
       <div class="col-xl-12">
            <!--begin:: Widgets/Company Summary-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Job Summary
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget13">

                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Job Type
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->job_type->name) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Job Status
                            </span>
                            <!-- <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->job_status->name) ?>
                            </span> -->
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->job_status->name) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Poduct Name
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->product->name) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Service
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->service->name) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Customer Name
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->customer->name) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Customer Address
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->customer->address) ?>
                            </span>
                        </div>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-left">
                                Due Dates
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->date) ?>
                            </span>
                        </div>


                    </div>
                </div>
            </div>
            <!--end:: Widgets/Company Summary-->
        </div>




                    </div>
                </div>
            </div>
            <!--end:: Widgets/Company Summary-->
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <?= $this->Flash->render(); ?>
            <div id="listItemContent">
                <?php echo $this->element('../Jobs/list_item_managed_employee'); ?>
            </div>
        </div>
    </div>
</div>


        <script type="text/javascript">
            function reloadListItemContent() {
                var thisHref = "/products/managed-product-service/<?php echo $data->id ?>";
                $('#listItemContent').fadeTo(300, 0);
                $('#listItemContent').load(thisHref, function() {
                    $(this).fadeTo(200, 1);
                });
            }

        </script>
        <script type="text/javascript">
        var _job_id;
        $(document).ready(function() {
          <?php
              if(isset($data->id)) {
                  ?>
                      _job_id     =   <?= $data->id ?>;
                  <?php
              }
                  ?>
          selesai();
          //console.log(_job_id)
        });

        function selesai() {
        setTimeout(function() {
          update();
          selesai();
        }, 200);
        }

        function update() {
          $.getJSON('/Jobs/getJobStatusByJobId/' + _job_id + '.json',{},function(result){
            $('#progressbar li.step0').removeClass('active');

              if( Object.keys(result.data).length > 0)
              {
                  $.each(result.data,function(key,value){

                       $('#step'+key).addClass('active');
                var test = $('#step'+key).addClass('active');
                       console.log(test);
                  });

                  //let lastElement = Object.keys(result.data)[Object.keys(result.data).length - 1];
                  console.log('naufal');
                  var tes = result.data.pop();
                  console.log(tes.name);

              }


          });
        }
        </script>
