<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
               <?= $this->Html->link(__('<button class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-plus"></i></button> Add Service'),
                    [
                        'action' => 'add'
                    ],
                    [
                        'class'    =>  '',
                        'escape'    =>  false,
                        'title'     =>  'Add Service'
                    ]
                ) ?>
            </div>
        </div>

            <div id="listItemContent">
                <?php echo $this->element('../Services/list_item'); ?>
            </div>
            <div id="map" style="height: 600px;">
                <!-- <img src="/assets/maps_1.jpg" alt="" style="width: 100%;height: auto;" /> -->
            </div>
            <div id="mapInformationBig">

            </div>
            <!--end: Datatable -->
        </div>
        <!-- END PORTLET Body -->
    </div>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?= $setting['map_browser_api_key'] ?>&callback=initMap"
  type="text/javascript"></script>
        <script type="text/javascript">
        let styleMap = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8ec3b9"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1a3646"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#4b6878"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#64779e"
      }
    ]
  },
  {
    "featureType": "administrative.province",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#4b6878"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#334e87"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#283d6a"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6f9ba5"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3C7680"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#304a7d"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#98a5be"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2c6675"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#255763"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#b0d5ce"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#023e58"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#98a5be"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1d2c4d"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#283d6a"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3a4762"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#0e1626"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#4e6d70"
      }
    ]
  }
];

            let map;
            let iconStr     =   "/assets/slider_circle_active_map.png";
        function initMap() {
                map = new google.maps.Map(document.getElementById("map"), {
                    center: { lat: -6.229728, lng: 106.68943 }, // tengah indonesia
                    zoom: 5,
                    styles: styleMap,

                    disableDefaultUI: true,
                    zoomControl: true,
                    scrollwheel:true
                });


                <?php
                    foreach($customers as $key => $value) {

                        ?>
                            marker<?= $key ?> = new google.maps.Marker({
                                position: { lat: <?= $value->lat ?>, lng: <?= $value->lng ?> },
                                map,
                                title: "<?= $value->name . $value->id ?>",
                                icon:iconStr,
                            });

                            marker<?= $key ?>.addListener("click", () => {
                                $('#mapInformationBig').html(createInformationBig('<?= htmlspecialchars($value->name) ?>', 'Alamat :<?= $value->address ?>', '<?= htmlspecialchars($value->no_telp) ?>'));



                            });

                        <?php
                    }
                ?>
                if($(window).width() < 768) {
                    $("#map").height(200);
                    map.setZoom(3);
                } else if($(window).width() < 968){
                    $("#map").height(350);
                    map.setZoom(4);
                } else {
                    $("#map").height(500);
                    map.setZoom(5);
                }

            }
            $(document).ready(function(){

              $( window ).resize(function() {
                  // $( "body" ).prepend( "<div>" + $( window ).width() + "</div>" );
                  if($(window).width() < 768) {
                      $("#map").height(200);
                      map.setZoom(3);
                  } else if($(window).width() < 968){
                      $("#map").height(350);
                      map.setZoom(4);
                  } else {
                      $("#map").height(500);
                      map.setZoom(5);
                  }

              });

          });
          function closeInformationWindowBig() {
                $("#mapInformationBig").html("");
            }
            function createInformationBig(name, address, noTelp) {
              return '<div id="mapInformationWindow" style="margin-top: -450px;margin-left:20px">'+
                      '<div class="containerfull">'+
                          '<div class="row">'+
                              '<div class="col-xs-12 col-md-6" style="background:white;">'+
                                  '<div class="padding-40px-all">'+
                                      '<span style="float:right;">' +
                                          '<button class="mfp-close text-red-yuasa" id="closeInformationWindow" onclick="closeInformationWindowBig();" href="javascript:void(0);" style="font-size: 25px;">'+
                                              'x'+
                                          '</button>'+
                                      '</span>'+
                                      '<span class="d-xs-none text-extra-dark-gray text-uppercase alt-font text-extra-large font-weight-600 margin-15px-bottom d-block">'+name+'</span>'+
                                      '<p class="margin-four">'+address + '<br>' + noTelp
                                      '</p>'+
                                      '<a class="btn btn-medium btn-rounded btn-danger" onclick="closeInformationWindowBig();" href="https://'+companyWebsite+'">' +
                                          'Website' +
                                      '</a>' +
                                  '</div>'+
                              '</div>'+
                              '<div class="d-none d-sm-block col-md-8 no-padding-lr" id="backgroundPopUpInformationWindow" onclick="closeInformationWindowBig()">'+
                                  '&nbsp;'+
                              '</div>'+
                          '</div>'+
                      '</div>'+
                  '</div>';

          }

        function reloadListItemContent() {
              var thisHref ="/users";;
              $('#listItemContent').fadeTo(300, 0);
              $('#listItemContent').load(thisHref, function() {
                  $(this).fadeTo(200, 1);
              });
          }

        </script>
    <?php
    $this->end();
?>
