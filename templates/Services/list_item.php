
<div class="row" >
                      <div class="col-12">
                          <div class="card">
                              <div class="card-header">



                              </div><!--end card-header-->

                              <div class="card-body">
                                <!-- <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="sa-warning">Click me</button> -->
                                  <table id="datatable-buttons" class="table table-striped m-table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                      <thead>
                                      <tr>

                                          <th>ID</th>
                                          <th>Name</th>
                                          <th>Job Type</th>
                                          <th>Description</th>
                                          <th>Created</th>
                                          <th>
                                            <?= __('Actions') ?>
                                          </th>
                                      </tr>
                                      </thead>


                                      <tbody>

                                      <?php foreach ($services as $service): ?>

          <tr scope="row">

              <td><?= $this->Number->format($service->id) ?></td>

              <td><?= h($service->name) ?></td>

              <td><?= h($service->job_type->name) ?></td>
                <td><?= h($service->description) ?></td>
              <td><?= h($service->created) ?></td>

              <td class="actions">
                  <!-- <?= $this->Html->link(__('View'), ['action' => 'view', $service->id]) ?> -->
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $service->id]) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $service->id], ['confirm' => __('Are you sure you want to delete # {0}?', $service->id)]) ?>
              </td>
                                      </tr>
                                        <?php endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div> <!-- end col -->
                  </div> <!-- end row -->
