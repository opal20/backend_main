<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aro[]|\Cake\Collection\CollectionInterface $aros
 */
?>
<?php 
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php 
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!-- END Portlet head -->
        <div class="m-portlet__body">
            <?= $this->Flash->render() ?>
            <!--begin: Datatable -->
            <div id="listItemContent">
                <?php echo $this->element('../ModuleObjects/list_item'); ?>
            </div>
            <!--end: Datatable -->
        </div>
        <!-- END PORTLET Body -->
    </div>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php 
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
        <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('click', 'a.ajaxPaginator', function () {
                var thisHref = $(this).attr('href');
                if (!thisHref) {
                    return false;
                }
                $('#listItemContent').fadeTo(300, 0);

                $('#listItemContent').load(thisHref, function() {
                    $(this).fadeTo(200, 1);
                });
                return false;
            });
        });
        </script>
    <?php 
    $this->end();
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aro[]|\Cake\Collection\CollectionInterface $aros
 */
?>
