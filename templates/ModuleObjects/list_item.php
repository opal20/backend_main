<?php
    // Change a template
    // $this->Paginator->setTemplates();
    // Pagination template using $this->loadHelper('Paginator', ['templates' => 'Amaya.metronic-paginator-template']); in AppView
?>

<div class="table-responsive">
    <table class="table table-striped m-table">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('Aros.id') ?></th>
                <th><?= $this->Paginator->sort('Aros.alias') ?></th>
                <th><?= $this->Paginator->sort('Aros.total_admin') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($arosListData as $aro): ?>
            <tr scope="row">
                <td><?= $this->Number->format($aro->id) ?></td>
                <td><?= h($aro->alias) ?></td>
                <td><?= $this->Number->format($aro->total_admin) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Edit Access'), ['action' => 'edit-access', $aro->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-xl-6">
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
        <?php 
            if(strpos($this->Paginator->generateUrl(), '?') !== false) {
               echo $this->Html->link(__("Export"), $this->Paginator->generateUrl() . "&excel=1"); 
            } else {
                echo $this->Html->link(__("Export"), $this->Paginator->generateUrl() . "?excel=1"); 
            }
        ?>
    </div>
    <div class="col-xl-6 m--align-right">
        <div class="paginator" style="float:right;">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>

