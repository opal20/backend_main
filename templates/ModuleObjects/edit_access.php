<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aro[]|\Cake\Collection\CollectionInterface $aros
 */
?>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Aro[]|\Cake\Collection\CollectionInterface $aros
 */
?>
<?php 
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        ['controller' => 'ModuleObjects', 'action' => 'index'],
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        "Edit module object",
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php 
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle . " - " . $aros->alias) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!-- END Portlet head -->
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <div class="table-responsive">
                <table class="table table-striped m-table">
                    <thead>
                        <tr>
                            <th>Module</th>
                            <th>Create</th>
                            <th>Read</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            echo $this->Form->create($aros);
                        ?>
                        <?php foreach ($acosList as $key => $aco): ?>

                            <?php 
                                $createValue    = "0";
                                $readValue      = "0";
                                $updateValue    = "0";
                                $deleteValue    = "0";
                                // regenerate data here. 
                                if(isset($matrixArosAcos[$aco->id])) {
                                    echo $this->Form->control('aros_acos.' . $aco->id . '.id', ['value' =>  $matrixArosAcos[$aco->id]['id']]);
                                    $createValue    = $matrixArosAcos[$aco->id]['_create'];
                                    $readValue      = $matrixArosAcos[$aco->id]['_read'];
                                    $updateValue    = $matrixArosAcos[$aco->id]['_update'];
                                    $deleteValue    = $matrixArosAcos[$aco->id]['_delete'];
                                }


                            ?>

                            <?= $this->Form->control('aros_acos.' . $aco->id . '.aco_id', array(
                                    'type'  =>  'hidden',
                                    'value' =>  $aco->id
                            )) ?>
                            <?= $this->Form->control('aros_acos.' . $aco->id . '.aro_id', array(
                                    'type'  =>  'hidden',
                                    'value' =>  $aros->id
                            )) ?>
                            <tr>
                                <td><?= h($aco->alias) ?></td>
                                <td>
                                    <?php 
                                        if($createValue == "1")
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._create', ['checked' => 'checked']);
                                        else
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._create', ['checked' => false]);
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if($readValue == "1")
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._read', ['checked' => 'checked']);
                                        else
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._read', ['checked' => false]);
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if($updateValue == "1")
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._update', ['checked' => 'checked']);
                                        else
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._update', ['checked' => false]);
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if($deleteValue == "1")
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._delete', ['checked' => 'checked']);
                                        else
                                            echo $this->Form->checkbox('aros_acos.' . $aco->id . '._delete', ['checked' => false]);
                                    ?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                        <?php 
                            echo $this->Form->button(__('Save Access'));
                            echo $this->Form->end();
                        ?>
                    </tbody>
                </table>
            </div>
            <!--end: Datatable -->
        </div>
        <!-- END PORTLET Body -->
    </div>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<?php 
    $this->start('scriptBottom');
    // echo $this->Html->script("/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js");
    ?>
        <script type="text/javascript">
        </script>
    <?php 
    $this->end();
?>