<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $actionTitle . " List",
        ['controller' => 'cms-menus', 'action' => 'index'],
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ],
            'templateVars'  =>  [
                'beforeTitle'   =>  '<span class="m-nav__link-text">',
                'afterTitle'    =>  '</span>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <!-- FLash Message Here -->
    <?= $this->Flash->render(); ?>

    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        <?= h($listItemTitle) ?>
                    </h3>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <?= $this->Form->create($cmsMenu, ['class' =>  'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed']) ?>
        <?php
            $this->Form->setTemplates([
                'inputContainer'        => '<div class="form-group m-form__group row">{{content}}</div>',
                'inputContainerError'   => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                'label'             => '<label class="col-lg-2 col-form-label">{{text}}</label>',
                'input'             => '<div class="col-lg-6"><input type="{{type}}" name="{{name}}"{{attrs}}/>{{after}}</div>',
                'select'            =>  '<div class="col-lg-6"><select name="{{name}}"{{attrs}}>{{content}}</select></div>',
                'error' => '<div class="col-lg-6"></div><div class="col-lg-6"><span class="m-form__help error">{{content}}</span></div>',
            ]);
        ?>
            <div class="m-portlet__body">
                <?php

                    echo $this->Form->control('name', [
                        'class' =>  'form-control m-input',
                        // 'placeholder'   =>  'Enter email',
                    ]);

                    echo $this->Form->control('parent_id', [
                        'label' => 'Parent menu',
                        'options' => $parentList,
                        'empty'   =>  'Please choose',
                        'class' =>  'custom-select form-control',
                    ]);

                    echo $this->Form->control('aco_id', [
                        'label' => 'Select Module',
                        'options' => $acosList,
                        'empty'   =>  'Please choose',
                        'class' =>  'custom-select form-control',
                    ]);

                    echo $this->Form->control('action', [
                        'class' =>  'form-control m-input',
                        'label' =>  'Action'
                        // 'placeholder'   =>  'Enter email',
                    ]);

                    echo $this->Form->control('icon_class', [
                        'class' =>  'form-control m-input',
                        'templateVars'  =>  [
                            'after' =>  '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#m_modal_1_2">
                        Choose icon
                            </button>',
                        ],

                        // 'placeholder'   =>  'Enter email',
                    ]);

                    echo $this->Form->control('is_group_separator', [
                        'class' =>  'form-control m-input',
                        'placeholder'   =>  'Is Group Separator ?',
                    ]);

                    echo $this->Form->control('status', [
                        'label' => 'Status',
                        'options' => array(
                            "0" =>  "Not Active",
                            "1"    =>  "Active"
                        ),
                        'default'   =>  1,
                        'class' =>  'custom-select form-control',
                    ]);
                ?>
            </div>
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                <div class="m-form__actions m-form__actions--solid">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-6">
                            <?= $this->Form->button(__('Submit'), [
                                'class' =>  'btn btn-brand'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?= $this->Form->end() ?>
        <!--end::Form-->
        <!--begin::Modal-->
        <div class="modal fade modal-auto-clear" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" aria-labelledby="m_modal_1_2">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Please click the icon.
                        </h5>
                        <button type="button" id="click"class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                &times;
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">

                            <?php echo $this->element("list_icon")?>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Modal-->
    </div>
    <!--end::Portlet-->
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>


        <script type="text/javascript">
            $(document).ready(function(){
                var intVal ;
                $(".m-demo-icons").click(function(){
                    var temp = $(this).find('i').attr('class');
                    $("#icon-class").val(temp);
                intVal =   setInterval(function(){
                      $("#click").trigger('click');
                },100);

                setTimeout(function(){ clearInterval(intVal); }, 1000);


                });
                
            });
        </script>
