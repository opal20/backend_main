<?php 
	
	
	// PHP Spreadsheet
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

	$headerTxt	=	[
		'Id',
		'Type User',
		'User Email',
		'User created',
		'User status'
	];

	$headerStyle	=	[
		'font'	=>	[
			'bold'	=>	true
		]
	];

	$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

	// set style. 
	$startColumn	=	'A';
	$startRow		=	1;
	for($ctr = 0; $ctr < count($headerTxt); $ctr++) {
		$spreadsheet->getActiveSheet()->getColumnDimension($startColumn)->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getStyle($startColumn . $startRow)->applyFromArray($headerStyle);	
		$startColumn++;
	}

	$startColumn	=	'A';
	foreach ($headerTxt as $key => $value) {
		$sheet->setCellValue($startColumn . $startRow, __($value));	
		$startColumn++;
	}
	$startRow++;
	foreach($users as $key => $value) {
		$sheet->setCellValue("A" . $startRow, $this->Number->format($value->id));	
		$sheet->setCellValue("B" . $startRow, $value->aro['alias']);	
		$sheet->setCellValue("C" . $startRow, $value->email);	
		$sheet->setCellValue("D" . $startRow, $value->created);	
		$sheet->setCellValue("E" . $startRow, $value->status);	
		$startRow++;
	}

    ob_clean();

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="'. $filename .'"');
    header('Cache-Control: max-age=0');

    $writer = new Xlsx($spreadsheet);
    $writer->save('php://output');

    $spreadsheet->disconnectWorksheets();
	unset($spreadsheet);

    exit();
    // $writer->save(WWW_ROOT . 'test_excel.xlsx');

?>