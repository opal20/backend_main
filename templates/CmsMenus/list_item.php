<?php
    // Change a template
    // $this->Paginator->setTemplates();
    // Pagination template using $this->loadHelper('Paginator', ['templates' => 'Amaya.metronic-paginator-template']); in AppView
?>
<div class="table-responsive">
    <table class="table table-striped m-table">
        <thead>
            <tr>
                <th>
                    <?= $this->Paginator->sort('CmsMenus.id', 'Id') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('ParentCmsMenus.name', 'Parent') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('CmsMenus.name', 'Name') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('Acos.alias', 'Module') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('CmsMenus.action', 'Action') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('CmsMenus.created') ?>
                </th>
                <th>
                    <?= $this->Paginator->sort('CmsMenus.status') ?>
                </th>
                <th>
                    <?= __('Actions') ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cmsMenus as $key    =>  $value): ?>
            <tr scope="row">
                <td><?= $this->Number->format($value->id) ?></td>
                <td>
                    <?php 
                        if(isset($value->parent_cms_menu['name'])) {
                            echo h($value->parent_cms_menu['name']);
                        }
                    ?>
                </td>
                <td><?= h($value->name) ?></td>
                <td>
                    <?php 
                        if(isset($value->aco['alias'])) {
                            echo h($value->aco['alias']);
                        }
                    ?>        
                </td>
                <td><?= h($value->action) ?></td>
                <td><?= h($value->created) ?></td>
                <td><?= h($value->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $value->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $value->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $value->id], ['confirm' => __('Are you sure you want to delete # {0}?', $value->id)]) ?>
                    <a href="#" onclick="MoveUp(<?php echo $value->id ?>, '<?php echo $this->Paginator->generateUrl(); ?>')">Move Up</a>
                    <a href="#" onclick="MoveDown(<?php echo $value->id ?>, '<?php echo $this->Paginator->generateUrl(); ?>')">Move Down</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-xl-6">
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
        <?php 
            if(strpos($this->Paginator->generateUrl(), '?') !== false) {
               echo $this->Html->link(__("Export"), $this->Paginator->generateUrl() . "&excel=1"); 
            } else {
                echo $this->Html->link(__("Export"), $this->Paginator->generateUrl() . "?excel=1"); 
            }
        ?>
    </div>
    <div class="col-xl-6 m--align-right">
        <div class="paginator" style="float:right;">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>

