<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(/assets/app/media/img//bg/bg-3.jpg);">
        <div class="m-login__wrapper-1 m-portlet-full-height">
            <div class="m-login__wrapper-1-1">
                <div class="m-login__contanier">
                    <div class="m-login__content">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="/assets/app/media/img//logos/logo-2.png">
                            </a>
                        </div>
                        <div class="m-login__title">
                            <h3>
                                Welcome to AMAYA Framework
                            </h3>
                        </div>
                        <div class="m-login__desc">
                            Amazing Stuff is Lorem Here.Grownng Team
                        </div>
                    </div>
                </div>
                <div class="m-login__border">
                    <div></div>
                </div>
            </div>
        </div>
        <div class="m-login__wrapper-2 m-portlet-full-height">
            <div class="m-login__contanier">
                <div class="m-login__signin">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            Login To Your Account
                        </h3>
                    </div>
                    <?= $this->Flash->render() ?>
                    <?= $this->Form->create(null, array('class' =>  'm-login__form m-form')) ?>
                    <!-- <form class="m-login__form m-form" action=""> -->
                        <?php 
                            $this->Form->setTemplates([
                                'inputContainer'    =>  '<div class="{{divClass}}">{{content}}</div>',
                            ]);
                        ?>
                        <?= $this->Form->control('email', 
                            [
                                'label'         =>  false,
                                'required'      => true, 
                                // 'after'     =>  '</div>',
                                'class'         =>  'form-control m-input',
                                'type'          =>  'text',
                                'placeholder'   =>  'Email',
                                'templateVars'  =>  [
                                    'divClass'  =>  "form-group m-form__group"
                                ]
                            ]) 
                        ?>
                        <?= $this->Form->control('password', 
                            [
                                'label'         =>  false,
                                'required'      => true, 
                                // 'after'     =>  '</div>',
                                'class'         =>  'form-control m-input m-login__form-input--last',
                                'type'          =>  'password',
                                'placeholder'   =>  'Password',
                                'templateVars'  =>  [
                                    'divClass'  =>  "form-group m-form__group"
                                ]
                            ])  ?>

                        <div class="row m-login__form-sub">
                            <div class="col m--align-left">
                                <label class="m-checkbox m-checkbox--focus">
                                <?php 
                                    echo $this->Form->checkbox(
                                        'remember_me', 
                                        ['hiddenField' => false]
                                    )
                                ?>
                                Remember me
                                <span></span>
                                </label>
                            </div>
                            <div class="col m--align-right">
                                <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                    Forget Password ?
                                </a>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Sign In
                            </button>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="m-login__forget-password">
                    <div class="m-login__head">
                        <h3 class="m-login__title">
                            Forgotten Password ?
                        </h3>
                        <div class="m-login__desc">
                            Enter your email to reset your password:
                        </div>
                    </div>
                    <form class="m-login__form m-form" action="">
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Request
                            </button>
                            <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom ">
                                Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
    $this->start('script');
    ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#m_login_forget_password").click(function(){
                    alert("clicked");
                });
            });
        </script>
    <?php 
    $this->end();
?>
<!-- end:: Page -->
<!-- in /templates/Users/login.php -->
<!--
<div class="users form">
    <h3>Login</h3>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please enter your username and password') ?></legend>
        <?= $this->Form->control('email', ['required' => true]) ?>
        <?= $this->Form->control('password', ['required' => true]) ?>
        <label for="color-orange">
            <?= $this->Form->checkbox(
                'remember_me', 
                ['hiddenField' => false]
            ) ?>
            Remember Me
        </label>
    </fieldset>
    <?= $this->Form->submit(__('Login')); ?>
    <?= $this->Form->end() ?>

    <?= $this->Html->link("Add User", ['action' => 'add']) ?>
</div>
