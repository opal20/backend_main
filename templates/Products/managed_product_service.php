<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        ['controller' => 'products', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                // 'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $data->name,
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-xl-6">
            <!--begin:: Widgets/Company Summary-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Product Summary
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-widget13">
                      <?php
                      if(!empty($data->image)) {
                          ?>
                          <div class="form-group m-form__group row">

                              <div class="col-lg-10">
                                  <img src="<?= $data->image->host . $data->image->url ?>" />
                              </div>
                          </div>
                          <?php
                      } ?>
                        <div class="m-widget13__item">
                            <span class="m-widget13__desc m--align-right">
                                Name
                            </span>
                            <span class="m-widget13__text m-widget13__text-bolder">
                                <?= h($data->name) ?>
                            </span>
                        </div>


                    </div>
                </div>
            </div>
            <!--end:: Widgets/Company Summary-->
        </div>

    </div>
    <div class="row">
        <div class="col-xl-12">
            <?= $this->Flash->render(); ?>
            <div id="listItemContent">
                <?php echo $this->element('../Products/list_item_managed_service'); ?>
            </div>
        </div>
    </div>
</div>


        <script type="text/javascript">
            function reloadListItemContent() {
                var thisHref = "/products/managed-product-service/<?php echo $data->id ?>";
                $('#listItemContent').fadeTo(300, 0);
                $('#listItemContent').load(thisHref, function() {
                    $(this).fadeTo(200, 1);
                });
            }
        </script>
