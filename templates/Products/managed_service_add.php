<?php
    // configure breadcrumb
    // $this->Breadcrumbs->setTemplates();

    $this->Breadcrumbs->add(
        '&nbsp;',
        ['controller' => 'dashboards', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $listItemTitle,
        ['controller' => 'productItemDiscounts', 'action' => 'index'],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                // 'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        $data->name,
        ['controller' => 'product', 'action' => 'managedProductVehicle', $data->id],
        [
            'class' =>  'm-nav__item m-nav__item--home',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link m-nav__link--icon'
            ],
            'templateVars'  =>  [
                // 'beforeTitle'  =>  '<i class="m-nav__link-icon la la-home"></i>'
            ]
        ]
    );

    $this->Breadcrumbs->add(
        "Add Product",
        null,
        [
            'class' =>  'm-nav__item',
            'innerAttrs'    =>  [
                'class' =>  'm-nav__link'
            ]
        ]
    );
?>

<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                <?= h($actionTitle) ?>
            </h3>
            <?php
                echo $this->Breadcrumbs->render(
                    [],
                    [
                        'separator' =>  '-'
                    ]
                );
            ?>
        </div>
    </div>
</div>
<!-- END: Subheader -->
<div class="m-content">
    <div class="row">
        <div class="col-xl-12">
            <?= $this->Flash->render(); ?>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Product Service List
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <?= $this->Form->create(null, ['id' =>  'submitSearchForm', 'url'   =>  '/products/managedServiceAdd/' . $data->id]) ?>
                        <div class="m-form__actions m-form__actions--solid">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-6">
                                    <?= $this->Form->control('job_type_id', [
                                        'label' => false,
                                        'options' => $jobTypeList,
                                        'empty'   =>  'All Job Type',
                                        'class'     =>  'custom-select form-control',
                                        'value'         =>  $this->request->getQuery('job_type_id'),
                                        'templateVars'  =>  [
                                            'divClass'  =>  'form-group m-form__group'
                                        ]
                                    ])
                                    ?>
                                </div>
                                <div class="col-lg-2">
                                    <?= $this->Form->button(__('Search'), [
                                        'class' =>  'btn btn-primary',
                                        'id'    =>  'searchButton'
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <div class="m-portlet__body" id="listItemContent">
                <?php echo $this->element('../Products/list_item_managed_service_add'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $this->start('scriptBottom');
    ?>
        <script type="text/javascript">

            $(document).ready(function(){

                $("#searchButton").click(function(){
                    reloadListItemContent();
                });

                $("#submitSearchForm").submit(function(event){
                    event.preventDefault();
                });

                $("#searchButton").click(function(){

                    var $form       =   $('#submitSearchForm'),
                        url         =   $form.attr('action'); // url nya ambil dari url form diatas
                    var data        =   $form.serialize();

                    $('#listItemContent').fadeTo(300, 0);

                    $.ajax({
                      type: "POST",
                      url: url,
                      data: data,
                      success: function(data){
                        $("#listItemContent").html(data);
                        $("#listItemContent").fadeTo(200, 1);
                      }
                    });

                });

            });

            function reloadListItemContent() {
                var thisHref = "/products/managed-service-add/<?php echo $data->id ?>";
                $('#listItemContent').fadeTo(300, 0);
                $('#listItemContent').load(thisHref, function() {
                    $(this).fadeTo(200, 1);
                });
            }
        </script>
    <?php
    $this->end();
?>
