<div class="table-responsive">
    <?= $this->Form->create(null, ['id' =>  'submitAddForm', 'url'   =>  '/products/managedServiceSubmitAdd/' . $data->id]) ?>
    <?php
        echo $this->Form->input('Products.id', [
            'type'  =>  'hidden',
            'value' =>  $data->id
        ]);

    ?>
    <table class="table table-striped m-table">
        <thead>
            <tr>
                <th>
                    <?php
                        echo $this->Form->checkbox('check_all', [
                            'hiddenField'   =>  false,
                            'id'    =>  'check_all'
                        ]);
                    ?>
                </th>
                <th>
                    Service ID
                </th>
                <th>
                    Service Name
                </th>
                <th>
                    Service Description
                </th>

                <th>
                    Type
                </th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($services as $key => $value): ?>
            <tr scope="row">
                <td>
                    <!-- checkbox -->
                    <?php
                        echo $this->Form->checkbox('services.'.$value->id.'.is_checked', [
                            'hiddenField'   =>  false
                        ]);
                    ?>
                </td>
                <td><?= $this->Number->format($value->id) ?></td>
                  <td><?= $value->name?></td>
                <td><?= $value->description?></td>
                <td><?= $value->job_type->name?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr scope="row">
                <td colspan="5">&nbsp;</td>
                <td>
                    <?= $this->Form->button(__('Submit'), [
                        'class' =>  'btn btn-primary',
                        'id'    =>  'submit'
                    ]) ?>
                </td>
            </tr>
        </tfoot>
        <?= $this->Form->end() ?>
    </table>
</div> <!-- end table responsive -->

<script type="text/javascript">
    $(document).ready(function(){
        $("#check_all").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
        });
    });
</script>
