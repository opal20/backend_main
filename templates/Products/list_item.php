<?= $this->Form->create(null, ['id' =>  'submitDeleteForm', 'url'   =>  '/users/deleteItems/users.json']) ?>


<div class="row" >
                      <div class="col-12">
                          <div class="card">
                              <div class="card-header">

                                  <?= $this->Form->button(__('Delete selection'), [
                        'class' =>  'btn btn-primary',
                        'id'    =>  'deleteButton'
                    ]) ?>

                              </div><!--end card-header-->

                              <div class="card-body">
                                <!-- <button type="button" class="btn btn-outline-info btn-sm waves-effect waves-light" id="sa-warning">Click me</button> -->
                                  <table id="datatable-buttons" class="table table-striped m-table" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                      <thead>
                                      <tr>
                                        <th>
                                          <?php
                                              echo $this->Form->checkbox('check_all', [
                                                  'hiddenField'   =>  false,
                                                  'id'    =>  'check_all'
                                              ]);
                                          ?>
                                      </th>
                                          <th>ID</th>
                                          <th>Pic</th>
                                          <th>Product Name</th>
                                          <th>Spec</th>
                                          <th>Created</th>
                                          <th>
                                            <?= __('Actions') ?>
                                          </th>
                                      </tr>
                                      </thead>


                                      <tbody>

                                      <?php foreach ($products as $product): ?>

          <tr scope="row">
            <td>
                            <!-- checkbox -->
                            <?php
                                echo $this->Form->checkbox('products.'.$product->id.'.is_checked', [
                                    'hiddenField'   =>  false
                                ]);
                            ?>
                        </td>
              <td><?= $this->Number->format($product->id) ?></td>
              <?php
                  $urlImage   =   "/assets/no_user_image.png";
                  if(!empty($product->image)) {
                      $urlImage   =   $product->image->host . $product->image->url;

                  }
              ?>
              <td><img class="m--marginless m--img-centered" src="<?=$urlImage ?>" style="width: 50px;"></td>
              <td><?= h($product->name) ?></td>
              <td><?= h($product->spec) ?></td>

              <td><?= h($product->created) ?></td>

              <td class="actions">
                  <?= $this->Html->link(__('View'), ['action' => 'managedProductService', $product->id]) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id]) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?>
              </td>
                                      </tr>
                                        <?php endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div> <!-- end col -->
                  </div> <!-- end row -->
                  <script type="text/javascript">
                      $(document).ready(function() {

                          // kalau check all, semua checkbox di checked
                          $("#check_all").click(function(){
                              $('input:checkbox').not(this).prop('checked', this.checked);
                          });

                          $("#deleteButton").click(function(){

                              var $form       =   $('#submitDeleteForm'),
                                  url         =   $form.attr('action'); // url nya ambil dari url form diatas
                              var data        =   $form.serialize();

                              swal.fire(
                                  {title: "Are you sure?",
                                  text: "You won't be able to revert this!",
                                  type: "warning",
                                  showCancelButton: !0,
                                  confirmButtonText: "Yaa, Hapus saja!",
                                  cancelButtonText: "Tidak, Batalkan!",
                                  reverseButtons: !0}).then(function(e){
                                          if(typeof e.value !== 'undefined' && e.value == true) {

                                              $.ajax({
                                                type: "POST",
                                                url: url,
                                                data: data,
                                                success: function(data){
                                                  if (typeof data.status !== 'undefined' && data.status == 1) {
                                                      swal.fire("Deleted!","User berhasil di hapus.","success").then(function(e){
                                                          reloadListItemContent();
                                                      });
                                                  } else {
                                                      alert("Please try again");
                                                  }
                                                }
                                              });
                                          }else{
                                            e.dismiss === Swal.DismissReason.cancel && swal.fire("Batal", "Data User dibatalkan untuk di hapus)", "error")
                                          }
                                      }
                                  );

                          });

                          // untuk apabila mao hapus semua.
                          $("#submitDeleteForm").submit(function(event){
                              event.preventDefault();
                          });

                      });
                      function reloadListItemContent() {
                            var thisHref ="/users";;
                            $('#listItemContent').fadeTo(300, 0);
                            $('#listItemContent').load(thisHref, function() {
                                $(this).fadeTo(200, 1);
                            });
                        }
                  </script>
