<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script 
      type="text/javascript"
      src="<?= $snapJsUrl ?>"
      data-client-key="<?= $clientKey ?>"
    ></script>
  </head>
  <body>
    <script type="text/javascript">
      snap.pay('<?= $order->mt_payment_snap_token ?>');
    </script>
  </body>
</html>