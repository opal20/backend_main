-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.4.11-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win64
-- HeidiSQL Versi:               11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Membuang struktur basisdata untuk backend_db
DROP DATABASE IF EXISTS `backend_db`;
CREATE DATABASE IF NOT EXISTS `backend_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `backend_db`;

-- membuang struktur untuk table backend_db.acos
DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `acos_type_id` smallint(6) NOT NULL DEFAULT 2,
  `model` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.acos: 9 rows
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
REPLACE INTO `acos` (`id`, `parent_id`, `acos_type_id`, `model`, `controller`, `alias`, `description`, `lft`, `rght`, `status`, `created`, `modified`) VALUES
	(1, NULL, 2, '', '', 'top', '', 1, 58, 1, '2020-07-22 06:19:07', '2020-07-22 06:19:07'),
	(3, 1, 2, NULL, 'Users', 'Users', 'Hello world', 4, 5, 1, '2020-07-22 09:20:58', '2020-07-22 09:20:58'),
	(4, 1, 2, NULL, 'ModuleObjects', 'ModuleObjects', 'Untuk atur module object', 6, 7, 1, '2020-07-23 08:13:26', '2020-07-23 08:13:26'),
	(5, 1, 2, NULL, 'Dashboards', 'Dashboards', 'Access to dashboard', 8, 9, 1, '2020-07-27 09:21:33', '2020-07-27 09:21:33'),
	(6, 1, 2, NULL, 'CmsMenus', 'CmsMenus', 'Untuk cms menu', 10, 11, 1, '2020-08-18 08:00:16', '2020-08-18 08:00:16'),
	(28, 1, 2, '', 'Products', 'Products', 'product', 50, 51, 1, '2021-05-14 10:48:12', '2021-05-14 11:06:49'),
	(29, 1, 2, NULL, 'Customers', 'Customers', 'Customers', 52, 53, 1, '2021-05-16 10:35:11', '2021-05-16 10:35:11'),
	(30, 1, 2, NULL, 'Jobs', 'Jobs', 'Jobs', 54, 55, 1, '2021-05-16 10:51:51', '2021-05-16 10:51:51'),
	(31, 1, 2, NULL, 'Services', 'Services', 'service management', 56, 57, 1, '2021-05-18 20:15:02', '2021-05-18 20:15:02');
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.acos_types
DROP TABLE IF EXISTS `acos_types`;
CREATE TABLE IF NOT EXISTS `acos_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.acos_types: 2 rows
/*!40000 ALTER TABLE `acos_types` DISABLE KEYS */;
REPLACE INTO `acos_types` (`id`, `name`) VALUES
	(1, 'Superadmin Only'),
	(2, 'All Admin');
/*!40000 ALTER TABLE `acos_types` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.aros
DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `total_admin` int(11) NOT NULL DEFAULT 0,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.aros: 4 rows
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
REPLACE INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `description`, `lft`, `rght`, `total_admin`, `status`, `created`, `modified`) VALUES
	(1, NULL, '', NULL, 'Developer', 'You can do what ever you want', 1, 8, 0, 1, '2020-07-20 08:07:49', '2020-07-20 08:07:49'),
	(2, 1, '', NULL, 'PM', 'Super admin', 2, 7, 0, 1, '2020-07-20 08:12:42', '2021-05-18 20:15:40'),
	(3, 2, '', NULL, 'Employee', 'Regular Adminsss', 3, 4, 1, 1, '2020-07-20 08:12:58', '2021-05-14 10:47:17'),
	(7, 2, '', NULL, 'Admin-Telkom', 'Untuk Admin Telkom', 5, 6, 1, 1, '2021-05-14 11:08:17', '2021-05-14 11:08:17');
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.aros_acos
DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `_read` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `_update` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `_delete` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `aro_id` (`aro_id`),
  KEY `aco_id` (`aco_id`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.aros_acos: 122 rows
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
REPLACE INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
	(1, 3, 2, '1', '1', '1', '0'),
	(2, 3, 3, '1', '1', '1', '0'),
	(3, 3, 4, '0', '0', '0', '0'),
	(4, 2, 2, '1', '1', '1', '1'),
	(5, 2, 3, '1', '1', '1', '1'),
	(6, 2, 4, '1', '1', '1', '1'),
	(7, 3, 5, '1', '1', '1', '1'),
	(8, 3, 6, '0', '0', '0', '0'),
	(9, 2, 5, '1', '1', '1', '1'),
	(10, 2, 6, '1', '1', '1', '1'),
	(11, 2, 7, '1', '1', '1', '1'),
	(12, 2, 8, '1', '1', '1', '1'),
	(13, 2, 9, '1', '1', '1', '1'),
	(14, 3, 7, '0', '1', '0', '0'),
	(15, 3, 8, '1', '1', '1', '0'),
	(16, 3, 9, '0', '0', '0', '0'),
	(17, 2, 10, '1', '1', '1', '1'),
	(18, 3, 10, '0', '0', '0', '0'),
	(19, 2, 11, '1', '1', '1', '1'),
	(20, 3, 11, '0', '0', '0', '0'),
	(21, 2, 12, '1', '1', '1', '1'),
	(22, 2, 13, '1', '1', '1', '1'),
	(23, 2, 14, '1', '1', '1', '1'),
	(24, 3, 12, '0', '0', '0', '0'),
	(25, 3, 13, '0', '0', '0', '0'),
	(26, 3, 14, '1', '1', '1', '0'),
	(27, 2, 15, '1', '1', '1', '1'),
	(28, 2, 16, '1', '1', '1', '1'),
	(29, 2, 17, '1', '1', '1', '1'),
	(30, 2, 18, '1', '1', '1', '1'),
	(31, 2, 19, '1', '1', '1', '1'),
	(32, 2, 20, '1', '1', '1', '1'),
	(33, 2, 21, '1', '1', '1', '1'),
	(34, 2, 22, '1', '1', '1', '1'),
	(35, 2, 23, '1', '1', '1', '1'),
	(36, 4, 2, '0', '0', '0', '0'),
	(37, 4, 3, '0', '0', '0', '0'),
	(38, 4, 4, '0', '0', '0', '0'),
	(39, 4, 5, '1', '1', '1', '1'),
	(40, 4, 6, '0', '0', '0', '0'),
	(41, 4, 7, '0', '0', '0', '0'),
	(42, 4, 8, '0', '0', '0', '0'),
	(43, 4, 9, '1', '1', '1', '1'),
	(44, 4, 10, '0', '0', '0', '0'),
	(45, 4, 11, '1', '1', '1', '1'),
	(46, 4, 12, '1', '1', '1', '1'),
	(47, 4, 13, '0', '0', '0', '0'),
	(48, 4, 14, '0', '0', '0', '0'),
	(49, 4, 15, '0', '0', '0', '0'),
	(50, 4, 16, '0', '0', '0', '0'),
	(51, 4, 17, '0', '0', '0', '0'),
	(52, 4, 18, '0', '0', '0', '0'),
	(53, 4, 19, '0', '0', '0', '0'),
	(54, 4, 20, '0', '0', '0', '0'),
	(55, 4, 21, '1', '1', '1', '1'),
	(56, 4, 22, '1', '1', '1', '1'),
	(57, 4, 23, '1', '1', '1', '1'),
	(58, 3, 15, '0', '0', '0', '0'),
	(59, 3, 16, '0', '0', '0', '0'),
	(60, 3, 17, '0', '0', '0', '0'),
	(61, 3, 18, '1', '1', '1', '0'),
	(62, 3, 19, '1', '1', '1', '0'),
	(63, 3, 20, '1', '1', '1', '0'),
	(64, 3, 21, '0', '0', '0', '0'),
	(65, 3, 22, '0', '0', '0', '0'),
	(66, 3, 23, '0', '0', '0', '0'),
	(67, 5, 2, '1', '1', '1', '0'),
	(68, 5, 3, '0', '0', '0', '0'),
	(69, 5, 4, '0', '0', '0', '0'),
	(70, 5, 5, '1', '1', '1', '1'),
	(71, 5, 6, '0', '0', '0', '0'),
	(72, 5, 7, '0', '0', '0', '0'),
	(73, 5, 8, '0', '0', '0', '0'),
	(74, 5, 9, '0', '0', '0', '0'),
	(75, 5, 10, '0', '0', '0', '0'),
	(76, 5, 11, '1', '1', '1', '1'),
	(77, 5, 12, '0', '0', '0', '0'),
	(78, 5, 13, '0', '0', '0', '0'),
	(79, 5, 14, '0', '0', '0', '0'),
	(80, 5, 15, '0', '0', '0', '0'),
	(81, 5, 16, '0', '0', '0', '0'),
	(82, 5, 17, '0', '0', '0', '0'),
	(83, 5, 18, '1', '1', '1', '0'),
	(84, 5, 19, '1', '1', '1', '0'),
	(85, 5, 20, '1', '1', '1', '0'),
	(86, 5, 21, '0', '0', '0', '0'),
	(87, 5, 22, '0', '0', '0', '0'),
	(88, 5, 23, '0', '0', '0', '0'),
	(89, 6, 2, '1', '1', '0', '0'),
	(90, 6, 3, '0', '0', '0', '0'),
	(91, 6, 4, '0', '0', '0', '0'),
	(92, 6, 5, '1', '1', '1', '1'),
	(93, 6, 6, '0', '0', '0', '0'),
	(94, 6, 7, '0', '0', '0', '0'),
	(95, 6, 8, '0', '0', '0', '0'),
	(96, 6, 9, '0', '0', '0', '0'),
	(97, 6, 10, '0', '0', '0', '0'),
	(98, 6, 11, '0', '0', '0', '0'),
	(99, 6, 12, '0', '0', '0', '0'),
	(100, 6, 13, '0', '0', '0', '0'),
	(101, 6, 14, '0', '0', '0', '0'),
	(102, 6, 15, '0', '0', '0', '0'),
	(103, 6, 16, '0', '0', '0', '0'),
	(104, 6, 17, '0', '0', '0', '0'),
	(105, 6, 18, '0', '0', '0', '0'),
	(106, 6, 19, '0', '0', '0', '0'),
	(107, 6, 20, '1', '1', '1', '0'),
	(108, 6, 21, '0', '0', '0', '0'),
	(109, 6, 22, '0', '0', '0', '0'),
	(110, 6, 23, '0', '0', '0', '0'),
	(111, 2, 24, '1', '1', '1', '1'),
	(112, 2, 25, '1', '1', '1', '1'),
	(113, 2, 26, '1', '1', '1', '1'),
	(114, 3, 24, '0', '0', '0', '0'),
	(115, 3, 25, '0', '0', '0', '0'),
	(116, 3, 26, '0', '0', '0', '0'),
	(117, 2, 27, '1', '1', '1', '1'),
	(118, 3, 27, '0', '0', '0', '0'),
	(119, 2, 28, '1', '1', '1', '1'),
	(120, 2, 29, '1', '1', '1', '1'),
	(121, 2, 30, '1', '1', '1', '1'),
	(122, 2, 31, '1', '1', '1', '1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.cms_menus
DROP TABLE IF EXISTS `cms_menus`;
CREATE TABLE IF NOT EXISTS `cms_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aco_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `action` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `icon_class` varchar(255) CHARACTER SET utf8 DEFAULT '',
  `is_group_separator` smallint(6) NOT NULL DEFAULT 0,
  `status` smallint(6) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.cms_menus: 10 rows
/*!40000 ALTER TABLE `cms_menus` DISABLE KEYS */;
REPLACE INTO `cms_menus` (`id`, `aco_id`, `parent_id`, `lft`, `rght`, `action`, `name`, `icon_class`, `is_group_separator`, `status`, `created`, `modified`) VALUES
	(1, NULL, NULL, 1, 20, NULL, 'Top Level Menu', '', 0, 1, '2020-08-25 04:11:55', '2020-08-25 04:11:55'),
	(2, 5, 1, 2, 3, '', 'Dashboard', 'fas fa-home', 0, 1, '2020-08-25 04:18:12', '2021-04-28 21:00:29'),
	(3, NULL, 1, 4, 9, '', 'Users', 'fas fa-user-circle', 1, 1, '2020-08-25 06:37:12', '2021-04-19 20:20:13'),
	(4, 3, 3, 5, 6, 'Index', 'Admin List', 'flaticon-users', 0, 1, '2020-08-28 03:26:12', '2020-11-03 14:00:15'),
	(5, NULL, 1, 10, 17, '', 'Administrator', 'fas fa-chalkboard-teacher', 1, 1, '2020-10-17 05:54:57', '2021-04-19 20:08:50'),
	(7, 4, 5, 11, 12, 'index', 'Module CMS', 'flaticon-signs-1', 0, 1, '2020-10-18 04:56:43', '2021-01-25 10:43:48'),
	(10, 8, 5, 13, 14, 'index', 'Company', 'flaticon-avatar', 0, 1, '2020-10-28 10:55:25', '2021-01-18 12:04:22'),
	(11, 9, 3, 7, 8, 'index', 'Customer List', 'flaticon-avatar', 0, 1, '2020-11-03 12:38:53', '2020-11-03 12:38:53'),
	(23, 6, 5, 15, 16, 'index', 'Menus Management', 'fas fa-bars', 0, 1, '2021-05-18 19:47:50', '2021-05-18 19:47:50'),
	(24, 28, 1, 18, 19, 'index', 'Product', 'fas fa-clipboard-list', 0, 1, '2021-05-18 19:48:56', '2021-05-18 19:48:56');
/*!40000 ALTER TABLE `cms_menus` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.contents
DROP TABLE IF EXISTS `contents`;
CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(100) CHARACTER SET utf8 NOT NULL,
  `model_id` int(11) NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `host` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 NOT NULL,
  `mime_type` varchar(100) CHARACTER SET utf8 NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model` (`model`),
  KEY `model_id` (`model_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.contents: 9 rows
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
REPLACE INTO `contents` (`id`, `model`, `model_id`, `type`, `host`, `url`, `mime_type`, `path`, `width`, `height`, `created`, `modified`) VALUES
	(1, 'Product', 3, 'image', 'http://localhost:8765/', 'contents/Product/3/3_image.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\Product\\3\\3_image.png', 400, 400, NULL, NULL),
	(2, 'Product', 2, 'image', 'http://localhost:8765/', 'contents/Product/2/2_image.jpg', 'image/jpeg', 'D:\\rokib\\rokib-backend\\webroot\\contents\\Product\\2\\2_image.jpg', 400, 400, NULL, NULL),
	(3, 'User', 1, 'thumb', 'http://localhost:8765/', 'contents/User/1/1_thumb.jpg', 'image/jpeg', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\1\\1_thumb.jpg', 200, 200, NULL, NULL),
	(4, 'User', 48, 'thumb', 'http://localhost:8765/', 'contents/User/48/48_thumb.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\48\\48_thumb.png', 200, 200, NULL, NULL),
	(5, 'User', 49, 'thumb', 'http://localhost:8765/', 'contents/User/49/49_thumb.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\49\\49_thumb.png', 200, 200, NULL, NULL),
	(6, 'User', 50, 'thumb', 'http://localhost:8765/', 'contents/User/50/50_thumb.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\50\\50_thumb.png', 200, 200, NULL, NULL),
	(7, 'User', 51, 'thumb', 'http://localhost:8765/', 'contents/User/51/51_thumb.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\51\\51_thumb.png', 200, 200, NULL, NULL),
	(8, 'User', 52, 'thumb', 'http://localhost:8765/', 'contents/User/52/52_thumb.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\User\\52\\52_thumb.png', 200, 200, NULL, NULL),
	(9, 'Product', 4, 'image', 'http://localhost:8765/', 'contents/Product/4/4_image.png', 'image/png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\Product\\4\\4_image.png', 400, 400, NULL, NULL);
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.customers
DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `lat` float(11,8) DEFAULT NULL,
  `lng` float(11,8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.customers: ~52 rows (lebih kurang)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
REPLACE INTO `customers` (`id`, `name`, `address`, `no_telp`, `lat`, `lng`, `created`, `modified`) VALUES
	(1, 'Rudiyanto', 'Jl. Lapangan pors rt/rw : 15/04 no. 14 Jakarta pusat', '08138444483', -6.11100006, 106.51399994, '2021-05-15 20:36:28', '2021-05-15 20:36:28'),
	(2, 'Purwanto', 'Jl. Serdang Baru XII rt/rw:16/05 no.34A jakarta pusat', '081511463379', -6.09299994, 106.51499939, '2021-05-15 20:36:29', '2021-05-15 20:36:30'),
	(3, 'Pegadaian', 'Jl. TSS raya no. 52 kel. Duri selatan, tambora', '081212157283', -6.09299994, 106.48000336, '2021-05-15 20:36:32', '2021-05-15 20:36:32'),
	(4, 'Burger King', 'Gading Terrrace Boelevard, Jl. Raya karang satria, bekasi', '081387852333', -6.21500015, 106.06999969, '2021-05-15 20:36:33', '2021-05-15 20:36:33'),
	(5, 'PT. Adi Perkasa', 'Jl. Wahab Affan, bekasi', '081218312696', -6.12300014, 106.58200073, '2021-05-15 20:36:34', '2021-05-15 20:36:34'),
	(6, 'Food N Beverage', 'Jl. Karang satria, Tambun', '02129680663', -6.13299990, 107.01499939, '2021-05-15 20:36:35', '2021-05-15 20:36:35'),
	(7, 'Federal International Finance', 'Jl. Bintara Raya, bekasi', '082299653552', -6.13999987, 106.57199860, '2021-05-15 20:36:35', '2021-05-15 20:36:35'),
	(8, 'Perkantoran PGN', 'Jl. Zainul arifin no. 20, Jakarta barat', '085100990505', -6.09399986, 106.48400116, '2021-05-15 20:36:36', '2021-05-15 20:36:36'),
	(9, 'CIMB NIAGA', 'Jl. Gajah Mada, Gambir, Jakarta Pusat', '081905057274', -6.09499979, 106.48999786, '2021-05-15 20:36:37', '2021-05-15 20:36:37'),
	(10, 'CIMB NIAGA', 'Jl. M.H. Thamrinno. 59, Jakarta Pusat', '087782372040', -6.11299992, 106.49199677, '2021-05-15 20:36:38', '2021-05-15 20:36:38'),
	(11, 'BANK BNI ', 'Jl. Talang Betutu, Kebon Melati, Jakarta Pusat', '081287224834', -6.18499994, 106.00000000, '2021-05-15 20:36:38', '2021-05-25 19:18:54'),
	(12, 'BANK BNI ', 'Jl. Talang Betutu, Kebon Melati, Jakarta Pusat', '081287224834', -6.11999989, 106.49099731, '2021-05-15 20:36:39', '2021-05-15 20:36:39'),
	(13, 'BANK BNI ', 'Jl. Veteran, Jakarta Pusat', '081287224834', -6.09999990, 106.49299622, '2021-05-15 20:36:40', '2021-05-15 20:36:40'),
	(14, 'PT. Satya Graha Dinamika', 'Jl. Letdjend Suprapto, Sumur Batu, Jakarta Pusat', '', -6.16800022, 106.87699890, '2021-05-15 20:36:40', '2021-05-15 20:36:40'),
	(15, 'mukhamad Ikhsan', 'Jl. M.H. Thamrin, Mentenng, Jakarta pusat', '', -6.19000006, 106.82199860, '2021-05-15 20:36:41', '2021-05-25 19:24:33'),
	(16, 'Didik Sugiantoro', 'Jl. Medan Merdeka Barat, Gambir, Jakarta Pusat', '', -6.17799997, 106.81999969, '2021-05-15 20:36:42', '2021-05-15 20:36:42'),
	(17, 'PT. Mount Scopus Indonesia', 'Jl. Cempaka Putih Raya, Jakarta Pusat', '081282093264', -6.10300016, 106.52200317, '2021-05-15 20:36:43', '2021-05-25 19:23:22'),
	(18, 'Suryanto', 'Jl. Gajah Mada rt/rw:02/08 no.1, petojo Utara', '085782162016', -6.10300016, 106.50199890, '2021-05-15 20:36:43', '2021-05-25 19:17:24'),
	(19, 'Suryanto', 'Jl. Gajah Mada rt/rw:02/08 no.1, petojo Utara', '085782162016', -6.09499979, 106.48999786, '2021-05-15 20:36:44', '2021-05-15 20:36:44'),
	(20, 'Rudy Ongkowidjojo', 'Jl. Bungur Besar, Bungur, Jakarta Pusat', '081319317288', -6.10300016, 106.50199890, '2021-05-15 20:36:44', '2021-05-15 20:36:44'),
	(21, 'Suwondo', 'Jl. Bendungan Jago rt/rw:009/001 no. 7, Utan Panjang', '081387575726', -6.09499979, 106.51100159, '2021-05-15 20:36:45', '2021-05-15 20:36:45'),
	(22, 'Rogate Maruli', 'Jl. Sukamulia rt/rw:05/01 no. 125A, kemayoran', '081387533269', -6.10200024, 106.51100159, '2021-05-15 20:36:46', '2021-05-15 20:36:46'),
	(23, 'BANK MANDIRI', 'Jl. K.H. Moch Mansyur no. 222, Jembatan Lima', '08118779222', -6.09000015, 106.48200226, '2021-05-15 20:36:47', '2021-05-15 20:36:47'),
	(24, 'Yan Yan s', 'Jl. Kebon Jahe rt/rw:010/002, Petojo Selatan', '081212211010', -6.10300016, 106.48999786, '2021-05-15 20:36:49', '2021-05-15 20:36:49'),
	(25, 'Yayuk Lestari', 'Jl. Kebon Kosong rt/rw:10/09, Kemayoran', '083878099133', -6.10099983, 106.50399780, '2021-05-15 20:36:49', '2021-05-15 20:36:49'),
	(26, 'Muhammad Zahwan', 'Jl. Johar Baru 5 rt/rw:08/11, Jakarta Pusat', '085156387071', -6.11000013, 106.51300049, '2021-05-15 20:36:50', '2021-05-15 20:36:50'),
	(27, 'Indomarco Pristama', 'Jl. Sultan Agung, Medan Satria, Bekasi', '089667515106', -6.12300014, 106.58300018, '2021-05-15 20:36:50', '2021-05-15 20:36:51'),
	(28, 'PT. INKOTE', 'Jl. Raya Bekasi KM 28 no. 56, Bekasi Barat', '', -6.13000011, 106.58100128, '2021-05-15 20:36:51', '2021-05-15 20:36:51'),
	(29, 'BPJSTK', 'Jl. Pramuka no. 29, Bekasi', '085210809343', -6.14200020, 107.00000000, '2021-05-15 20:36:52', '2021-05-15 20:36:52'),
	(30, 'PT. Mitra Makmur Bagya', 'Jl. Raya Sultan Agung KM 28,5, Bekasi', '08161472406', -6.12500000, 106.58300018, '2021-05-15 20:36:53', '2021-05-15 20:36:53'),
	(31, 'UNIVERSITAS YARSI', 'Jl. Letdjend Suprapto, Jakarta Pusat', '081510094413', -6.10200024, 106.51399994, '2021-05-15 20:36:55', '2021-05-15 20:36:56'),
	(32, 'PERTAMINA', 'Jl. Bungur Besar Raya, Kemayoran', '08127190518', -6.09499979, 106.50199890, '2021-05-15 20:36:56', '2021-05-15 20:36:56'),
	(33, 'Pemprov DKI', 'Jl. Budi Kemuliaan 1, Jakarta Pusat', '082115629247', -6.10400009, 106.49099731, '2021-05-15 20:36:56', '2021-05-15 20:36:57'),
	(34, 'Koperasi Pondok Pesantren', 'Jl. Kebon Kelapa 03, Batu Tulis  Jakarta Pusat', '08112119011', -6.11000013, 106.52100372, '2021-05-15 20:36:57', '2021-05-25 19:12:45'),
	(35, 'Koperasi Pondok Pesantren', 'Jl. Kebon Kelapa 03, Batu Tulis  Jakarta Pusat', '08112119011', -6.09499979, 106.49299622, '2021-05-15 20:36:58', '2021-05-15 20:36:58'),
	(36, 'SDN Pasar baru 05 Pagi', 'Jl. Pintu Besi 1 Jakarta Pusat', '0213511963', -6.09299994, 106.50000000, '2021-05-15 20:36:59', '2021-05-15 20:36:59'),
	(37, 'PT. IndoPack Indonesia', 'Jl. Raya beaksi KM 28, Pondok Ungu, Bekasi', '081285978823', -6.11299992, 106.58300018, '2021-05-15 20:37:01', '2021-05-15 20:37:01'),
	(38, 'test', 'jl test', '082213179998', -6.08500004, 106.48200226, '2021-05-15 20:37:01', '2021-05-25 18:47:51'),
	(39, 'Abdi Hafid', 'Jl. Percetakan Negara IV rt/rw:010/009, Jakarta Pusat', '082213179998', -6.11199999, 106.51300049, '2021-05-15 20:37:02', '2021-05-15 20:37:02'),
	(40, 'Ratih Heraningsih', 'Jl. Krekot bunder 11 no. 3, Pasar Baru', '081287827734', -6.11399984, 106.48500061, '2021-05-15 20:37:04', '2021-05-25 19:08:33'),
	(41, 'Ratih Heraningsih', 'Jl. Krekot bunder 11 no. 3, Pasar Baru', '081287827734', -6.09399986, 106.49500275, '2021-05-15 20:37:04', '2021-05-15 20:37:04'),
	(42, 'Agustin Ningsiih', 'Gg Spor 4 Dalam rt/rw:07/05, Gunung Sahari', '081314733722', -6.09399986, 106.50299835, '2021-05-15 20:37:05', '2021-05-15 20:37:05'),
	(43, 'aass', 'Jl. Kramat Jaya no. 38, Johar Baru, jakarta usat', '08119320282', -6.11299992, 106.50000000, '2021-05-15 20:37:05', '2021-05-25 19:04:10'),
	(44, 'SMA ISLAM AL JIHAD', 'Jl. Kramat Jaya no. 38, Johar Baru, jakarta usat', '08119320282', -6.11100006, 106.51000214, '2021-05-15 20:37:06', '2021-05-15 20:37:06'),
	(45, 'Sunarto', 'Jl. Melati rt/rw:009/002, Serdang, Jakarta Pusat', '08119320282', -6.09299994, 106.51200104, '2021-05-15 20:37:07', '2021-05-15 20:37:07'),
	(46, 'Rina rahmawati', 'Jl. Keutamaan Dalam rt/rw:006/003 no. 21, jakarta Pusat', '081806868334', -6.09200001, 106.09200287, '2021-05-15 20:37:07', '2021-05-15 20:37:07'),
	(47, 'Marotun Nuriah', 'Jl. Sumber Pelita rt/rw:005/001 no. 23, Jakarta Pusat', '081240430705', -6.09399986, 106.51499939, '2021-05-25 18:55:00', '2021-05-25 18:55:00'),
	(48, 'Muhammad Aditya', 'Jl. Kebon Kosong rt/rw:015/004 no. 136, Jakarta Pusat', '083895338141', -6.10099983, 106.50499725, '2021-05-25 18:54:59', '2021-05-25 18:54:59'),
	(49, 'Rini Septiani', 'Jl. Cempaka Putih Barat rt/rw:03/07 no. 9c, Jakarta Pusat', '085775228580', -6.10400009, 106.51399994, '2021-05-25 18:54:58', '2021-05-25 18:54:58'),
	(50, 'BANK BRI', 'JL. MH. Thamrin, jakarta pusat ', '085216934025', -6.11100006, 106.49199677, '2021-05-15 20:36:30', '2021-05-15 20:36:31'),
	(51, 'Naufal', 'jl kemanggisan', '0999888999', NULL, NULL, '2021-05-22 21:20:35', '2021-05-22 21:20:35'),
	(56, 'Rokib', 'jl cpk putih', '0999000999', NULL, NULL, '2021-05-25 19:39:02', '2021-05-25 19:39:02');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.employes
DROP TABLE IF EXISTS `employes`;
CREATE TABLE IF NOT EXISTS `employes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT 1,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_employee` (`user_id`),
  CONSTRAINT `fk_user_employee` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.employes: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `employes` DISABLE KEYS */;
REPLACE INTO `employes` (`id`, `user_id`, `full_name`, `address`, `status`, `created`, `modified`) VALUES
	(1, 1, 'PM', 'Jalan oke aja', 1, '2021-05-04 22:48:46', '2021-05-04 22:48:47'),
	(2, 49, 'op', 'jl oke', 1, '2021-05-08 13:35:43', '2021-05-08 13:35:43'),
	(5, 52, 'Admin Telkom', 'Jalan kemanggisan', 1, '2021-05-14 11:08:59', '2021-05-14 11:08:59');
/*!40000 ALTER TABLE `employes` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_type_id` int(11) DEFAULT NULL,
  `job_status_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `is_urgent` int(1) DEFAULT 0,
  `date` date DEFAULT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT 1,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_job_type` (`job_type_id`),
  KEY `fk_job_status` (`job_status_id`),
  KEY `fk_users` (`user_id`),
  KEY `fk_product` (`product_id`),
  KEY `fk_customer` (`customer_id`),
  CONSTRAINT `FK_cus` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_jobs_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_job_status` FOREIGN KEY (`job_status_id`) REFERENCES `job_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_job_type` FOREIGN KEY (`job_type_id`) REFERENCES `job_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.jobs: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
REPLACE INTO `jobs` (`id`, `job_type_id`, `job_status_id`, `user_id`, `product_id`, `customer_id`, `is_urgent`, `date`, `notes`, `status`, `created`, `modified`) VALUES
	(1, 2, 2, 1, 2, 15, 0, '2021-05-25', NULL, 1, '2021-05-25 19:24:33', '2021-05-25 19:24:33'),
	(2, 1, 1, 1, 3, 56, 0, '2021-05-25', NULL, 1, '2021-05-25 19:39:02', '2021-05-25 19:39:02');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.jobs_users
DROP TABLE IF EXISTS `jobs_users`;
CREATE TABLE IF NOT EXISTS `jobs_users` (
  `job_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`job_id`,`employee_id`) USING BTREE,
  KEY `fk_employee` (`employee_id`),
  CONSTRAINT `fk_employee` FOREIGN KEY (`employee_id`) REFERENCES `employes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.jobs_users: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `jobs_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs_users` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.job_statuses
DROP TABLE IF EXISTS `job_statuses`;
CREATE TABLE IF NOT EXISTS `job_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.job_statuses: ~6 rows (lebih kurang)
/*!40000 ALTER TABLE `job_statuses` DISABLE KEYS */;
REPLACE INTO `job_statuses` (`id`, `name`, `created`, `modified`) VALUES
	(1, 'Created', '2021-05-14 14:35:25', '2021-05-14 14:35:27'),
	(2, 'Confirmed', '2021-05-14 14:35:58', '2021-05-14 14:36:00'),
	(3, 'On Progress', '2021-05-14 14:36:11', '2021-05-14 14:36:12'),
	(4, 'Hold', '2021-05-14 14:36:20', '2021-05-14 14:36:21'),
	(5, 'Cancelled', '2021-05-14 14:36:30', '2021-05-14 14:36:32'),
	(6, 'Complited', '2021-05-14 14:36:41', '2021-05-14 14:36:42');
/*!40000 ALTER TABLE `job_statuses` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.job_types
DROP TABLE IF EXISTS `job_types`;
CREATE TABLE IF NOT EXISTS `job_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.job_types: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `job_types` DISABLE KEYS */;
REPLACE INTO `job_types` (`id`, `name`, `desc`) VALUES
	(1, 'Instalation', 'Untuk Instalasi WIFI'),
	(2, ' Maintanance', 'Untuk Maintanance');
/*!40000 ALTER TABLE `job_types` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.products
DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `spec` varchar(255) NOT NULL,
  `status` int(1) DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.products: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`id`, `name`, `spec`, `status`, `created`, `modified`) VALUES
	(2, 'Wifi Xiomi', 'Super', 1, '2021-05-04 11:11:21', '2021-05-04 11:23:10'),
	(3, 'Wifi Baru', 'Baru banget', 1, '2021-05-04 11:15:11', '2021-05-04 11:15:11'),
	(4, 'Speady', '10sp', 1, '2021-05-18 16:05:20', '2021-05-18 16:05:20');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.products_services
DROP TABLE IF EXISTS `products_services`;
CREATE TABLE IF NOT EXISTS `products_services` (
  `service_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`,`product_id`) USING BTREE,
  KEY `product_id` (`product_id`),
  CONSTRAINT `FKservice2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_services_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.products_services: ~5 rows (lebih kurang)
/*!40000 ALTER TABLE `products_services` DISABLE KEYS */;
REPLACE INTO `products_services` (`service_id`, `product_id`) VALUES
	(1, 2),
	(1, 3),
	(2, 2),
	(2, 3),
	(3, 3);
/*!40000 ALTER TABLE `products_services` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.services
DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_type_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT 1,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjobtype` (`job_type_id`),
  CONSTRAINT `FKjobtype` FOREIGN KEY (`job_type_id`) REFERENCES `job_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.services: ~4 rows (lebih kurang)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
REPLACE INTO `services` (`id`, `job_type_id`, `name`, `description`, `status`, `created`, `modified`) VALUES
	(1, 2, 'Upgrade Wifi', 'untuk upgrade edittt', 1, '2021-05-03 20:33:49', '2021-05-18 20:31:17'),
	(2, 2, 'Wifi 10MBPS', 'Untuk wifi 10MBPS', 1, '2021-05-04 20:45:56', '2021-05-04 20:45:58'),
	(3, 1, 'Wifi 20MBPS', 'Wifi untuk 20 MBPS', 1, '2021-05-04 21:32:02', '2021-05-04 21:32:03');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.settings
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_url` varchar(255) DEFAULT NULL,
  `cms_title` varchar(255) DEFAULT NULL,
  `cms_description` text DEFAULT NULL,
  `cms_keywords` text DEFAULT NULL,
  `cms_author` varchar(255) DEFAULT NULL,
  `cms_app_name` varchar(255) DEFAULT NULL,
  `cms_logo_url` varchar(255) DEFAULT NULL,
  `company_brand_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `customer_phone_number` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `copyright_text` varchar(255) NOT NULL,
  `email_logo_url` varchar(205) DEFAULT NULL,
  `path_content` varchar(255) DEFAULT NULL,
  `path_webroot` varchar(255) DEFAULT NULL,
  `map_api_key` varchar(255) DEFAULT NULL,
  `map_android_api_key` varchar(255) DEFAULT NULL,
  `map_browser_api_key` varchar(255) DEFAULT NULL,
  `firebase_api_key` text NOT NULL,
  `facebook_app_id` varchar(255) DEFAULT NULL,
  `google_client_id` text DEFAULT NULL,
  `google_client_secret` varchar(255) DEFAULT NULL,
  `default_lat` varchar(255) DEFAULT NULL,
  `default_lng` varchar(255) DEFAULT NULL,
  `ios_bundle_id` varchar(255) DEFAULT NULL,
  `android_package_name` varchar(255) DEFAULT NULL,
  `android_minimum_version` varchar(255) DEFAULT NULL,
  `authorization_id` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Membuang data untuk tabel backend_db.settings: 1 rows
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
REPLACE INTO `settings` (`id`, `cms_url`, `cms_title`, `cms_description`, `cms_keywords`, `cms_author`, `cms_app_name`, `cms_logo_url`, `company_brand_name`, `company_address`, `customer_phone_number`, `customer_email`, `admin_email`, `copyright_text`, `email_logo_url`, `path_content`, `path_webroot`, `map_api_key`, `map_android_api_key`, `map_browser_api_key`, `firebase_api_key`, `facebook_app_id`, `google_client_id`, `google_client_secret`, `default_lat`, `default_lng`, `ios_bundle_id`, `android_package_name`, `android_minimum_version`, `authorization_id`, `modified`) VALUES
	(1, 'http://localhost:8765/', 'Trade Com', 'Trade Com', 'Trade Com', 'Trade Com', 'Trade Com', NULL, 'Trade Com', '', '', '', '', '@Trade Com 2019', 'http://www.tradecom.com/img/logo-divertune-small.png', 'D:\\rokib\\rokib-backend\\webroot\\contents\\', 'D:\\rokib\\rokib-backend\\webroot\\', '', '', 'AIzaSyA21Q3UJhCqxo92qv009rNSDU8R2CXDX_A ', '', '', '', '', '-6.175414', '106.827122', NULL, NULL, NULL, 'wt1U5MACWJFTXGenFoZoiLwQGrLgdwooKDicKDJWooLiverpoolADFbHA', '2017-09-05 15:53:25');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- membuang struktur untuk table backend_db.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aro_id` (`aro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel backend_db.users: ~3 rows (lebih kurang)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `aro_id`, `email`, `password`, `created`, `modified`) VALUES
	(1, 2, 'admin@amaya.com', '$2y$10$Pg.nNYzzxfbWhWH0cDXBPu.FysGliOsNskMHldLfosIClNVjHIBWi', '2020-07-02 15:15:51', '2021-04-28 21:11:31'),
	(49, 3, 'naufal@gmail.com', '$2y$10$wo657P5D1Hw/1pt0iKiCC.WWy99B9GcPS/0Y.QffKoXju/2kjXCfK', '2021-05-08 13:35:43', '2021-05-14 14:09:43'),
	(52, 7, 'admin@telkom.com', '$2y$10$CHlE17ZUX9GLlFEk0rXoleIo5CaDfeSuDtUsCBSZ.iDCbjOlVOImG', '2021-05-14 11:08:59', '2021-05-14 14:10:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
