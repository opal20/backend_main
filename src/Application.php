<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App;

use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Http\MiddlewareQueue;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Log\Log;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */

//Authentication Plugin
use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceInterface;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Middleware\AuthenticationMiddleware;
use Psr\Http\Message\ServerRequestInterface;

// Authorization Plugin
use Authorization\AuthorizationService;
use Authorization\AuthorizationServiceInterface;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Policy\ResolverCollection;
use Authorization\Policy\MapResolver;
use Authorization\Policy\OrmResolver;
use Psr\Http\Message\ResponseInterface;
use App\Model\Entity\User;
use App\Policy\GeneralPolicy;

// csrf token 
use Cake\Http\Middleware\CsrfProtectionMiddleware;

// Additional Middleware
// https://book.cakephp.org/4/en/controllers/middleware.html#encrypted-cookie-middleware
use Cake\Http\Middleware\EncryptedCookieMiddleware;

class Application extends BaseApplication 
    implements AuthenticationServiceProviderInterface,
    AuthorizationServiceProviderInterface
// class Application extends BaseApplication
{
    /**
     * Load all the application configuration and bootstrap logic.
     *
     * @return void
     */
    public function bootstrap(): void
    {
        $this->addPlugin('AuditStash');

        // Call parent to load bootstrap from files.
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        }

        /*
         * Only try to load DebugKit in development mode
         * Debug Kit should not be installed on a production system
         */
        if (Configure::read('debug')) {
            Configure::write('DebugKit.ignoreAuthorization', true);
            $this->addPlugin('DebugKit');
        }

        // Load more plugins here
        
        // $this->addPlugin('Authentication');
        $this->addPlugin('Authorization');
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {

        $csrf = new CsrfProtectionMiddleware();
        $csrf->skipCheckCallback(function ($request) {
            // Skip token check for API URLs.
            if (strtoupper($request->getParam('controller')) === 'API') {
                return true;
            }
        });

        $middlewareQueue
            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(Configure::read('Error')))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime'),
            ]))

            // Add routing middleware.
            // If you have a large number of routes connected, turning on routes
            // caching in production could improve performance. For that when
            // creating the middleware instance specify the cache config name by
            // using it's second constructor argument:
            // `new RoutingMiddleware($this, '_cake_routes_')`
            ->add(new RoutingMiddleware($this))

            // Parse various types of encoded request bodies so that they are
            // available as array through $request->getData()
            // https://book.cakephp.org/4/en/controllers/middleware.html#body-parser-middleware
            ->add(new BodyParserMiddleware())

            // Add Cooke Encrypted Middleware
            ->add(new EncryptedCookieMiddleware(
                ['CookieAuth', 'secrets', 'protected'],
                Configure::read('Security.cookieKey')
            ))

            // add Authentication after RoutingMiddleware
            ->add(new AuthenticationMiddleware($this))

            // Add authorization **after** authentication
            ->add(new AuthorizationMiddleware($this))

            // // Ensure routing middleware is added to the queue before CSRF protection middleware.
            ->add($csrf);

        return $middlewareQueue;
    }

    public function getAuthenticationService(ServerRequestInterface $request):  AuthenticationServiceInterface
    {
        $authenticationService  =   new AuthenticationService([
            'unauthenticatedRedirect'   =>  '/users/login',
            'queryParam'                =>  'redirect'
        ]);

        $authenticationService->loadIdentifier('Authentication.Password', [
            'fields'    =>  [
                'username'  =>  'email',
                'password'  =>  'password'
            ]
        ]);

        $authenticationService->loadAuthenticator('Authentication.Session');
        $authenticationService->loadAuthenticator('Authentication.Form', [
            'fields'    =>  [
                'username'  =>  'email',
                'password'  =>  'password'
            ],
            'loginUrl'  =>  '/users/login'
        ]);

        // Cookie Authenticator Aka. RememberMe
        $authenticationService->loadAuthenticator('Authentication.Cookie', [
            'fields'    =>  [
                'username'  =>  'email',
                'password'  =>  'password'
            ],
            'loginUrl'  =>  '/users/login',
            // 'cookie'    =>  [

            // ]
        ]);

        return $authenticationService;
    }

    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        
        $ormResolver = new OrmResolver();
        $mapResolver = new MapResolver();
        // Map a resource class to a policy instance.
        $params = $request->getAttributes('params');
        
        // kalau butuh ormResolver silahkan if else disini. 

        // rules pertama gunakan mapResolver
        $generalPolicyClass    =   new GeneralPolicy();
        $generalPolicyClass->acosAliasName =   $params['params']['controller'];
        $mapResolver->map(User::class, $generalPolicyClass); // so we can use dashboard policy.

        $resolver = new ResolverCollection([$mapResolver, $ormResolver]);

        return new AuthorizationService($resolver);
    }

    /**
     * Bootrapping for CLI application.
     *
     * That is when running commands.
     *
     * @return void
     */
    protected function bootstrapCli(): void
    {
        try {
            $this->addPlugin('Bake');
        } catch (MissingPluginException $e) {
            // Do not halt if the plugin is missing
        }

        $this->addPlugin('Migrations');

        // Load more plugins here
    }
}
