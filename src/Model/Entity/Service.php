<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Service Entity
 *
 * @property int $id
 * @property int|null $job_type_id
 * @property string|null $name
 * @property string|null $desc
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\JobType $job_type
 * @property \App\Model\Entity\Product[] $products
 */
class Service extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'job_type_id' => true,
        'name' => true,
        'description' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'job_type' => true,
        'products' => true,
    ];
}
