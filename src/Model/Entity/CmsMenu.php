<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CmsMenu Entity
 *
 * @property int $id
 * @property int|null $aco_id
 * @property int|null $parent_id
 * @property int $lft
 * @property int $rght
 * @property string|null $action
 * @property string $name
 * @property string|null $icon_class
 * @property int $is_group_separator
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ParentCmsMenu $parent_cms_menu
 * @property \App\Model\Entity\Aco $aco
 * @property \App\Model\Entity\ChildCmsMenu[] $child_cms_menus
 */
class CmsMenu extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'aco_id' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'action' => true,
        'name' => true,
        'icon_class' => true,
        'is_group_separator' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'parent_cms_menu' => true,
        'aco' => true,
        'child_cms_menus' => true,
    ];
}
