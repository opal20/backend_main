<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Job Entity
 *
 * @property int $id
 * @property int|null $job_type_id
 * @property int|null $job_status_id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property int|null $customer_id
 * @property int|null $is_urgent
 * @property \Cake\I18n\FrozenTime|null $date
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\JobType $job_type
 * @property \App\Model\Entity\JobStatus $job_status
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Customer $customer
 */
class Job extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'service_id' => true,
        'job_type_id' => true,
        'job_statuses_id' => true,
        'user_id' => true,
        'product_id' => true,
        'customer_id' => true,
        'is_urgent' => true,
        'date' => true,
        'created' => true,
        'modified' => true,
        'job_type' => true,
        'job_status' => true,
        'users' => true,
        'product' => true,
        'customer' => true,
    ];
    protected $_virtual = ['job_type_with_product_name'];
    protected function _getJobTypeWithProductName()
    {
        $defaultValue   =   null;

        if(isset($this->job_type->name) && isset($this->product->name)) {
            $defaultValue   =   $this->job_type->name . " for " . $this->product->name;
        } else {
            $defaultValue   =   " ";
        }

        return $defaultValue;

    }

}
