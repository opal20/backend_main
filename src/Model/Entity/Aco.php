<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Aco Entity
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int $acos_type_id
 * @property string|null $model
 * @property string|null $controller
 * @property string|null $alias
 * @property string|null $description
 * @property int|null $lft
 * @property int|null $rght
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ParentAco $parent_aco
 * @property \App\Model\Entity\AcosType $acos_type
 * @property \App\Model\Entity\ChildAco[] $child_acos
 * @property \App\Model\Entity\Aro[] $aros
 */
class Aco extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'parent_id' => true,
        'acos_type_id' => true,
        'model' => true,
        'controller' => true,
        'alias' => true,
        'description' => true,
        'lft' => true,
        'rght' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'parent_aco' => true,
        'acos_type' => true,
        'child_acos' => true,
        'aros' => true,
    ];
}
