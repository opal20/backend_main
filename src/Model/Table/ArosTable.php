<?php
// src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Event\EventInterface;
use Cake\Validation\Validator;
use Cake\Log\Log;

class ArosTable extends Table
{
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
      //  $this->addBehavior('AuditStash.AuditLog');
        $this->belongsTo('Users')->setForeignKey('user_id');
        $this->hasMany('ArosAcos')->setForeignKey('aro_id');
    }

    public function beforeSave(EventInterface $event, $entity, $options)
    {
        // Log::write("debug", "masuk before save");
    }

    public function validationDefault(Validator $validator): Validator
    {
    	$validator
    		->notEmptyString('parent_id', 'Please choose parent');

    	return $validator;
    }

}
