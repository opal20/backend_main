<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AcosTypes Model
 *
 * @property \App\Model\Table\AcosTable&\Cake\ORM\Association\HasMany $Acos
 *
 * @method \App\Model\Entity\AcosType newEmptyEntity()
 * @method \App\Model\Entity\AcosType newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\AcosType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AcosType get($primaryKey, $options = [])
 * @method \App\Model\Entity\AcosType findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\AcosType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AcosType[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\AcosType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AcosType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AcosType[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AcosType[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\AcosType[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AcosType[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AcosTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('acos_types');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Acos', [
            'foreignKey' => 'acos_type_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        return $validator;
    }
}
