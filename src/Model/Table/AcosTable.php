<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Core\App;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Event\EventInterface;

/**
 * Acos Model
 *
 * @property \App\Model\Table\AcosTable&\Cake\ORM\Association\BelongsTo $ParentAcos
 * @property \App\Model\Table\AcosTypesTable&\Cake\ORM\Association\BelongsTo $AcosTypes
 * @property \App\Model\Table\AcosTable&\Cake\ORM\Association\HasMany $ChildAcos
 *
 * @method \App\Model\Entity\Aco newEmptyEntity()
 * @method \App\Model\Entity\Aco newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Aco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Aco get($primaryKey, $options = [])
 * @method \App\Model\Entity\Aco findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Aco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Aco[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Aco|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aco saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Aco[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class AcosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('acos');
        $this->setDisplayField('alias');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');
      //  $this->addBehavior('AuditStash.AuditLog');

        $this->belongsTo('ParentAcos', [
            'className' => 'Acos',
            'foreignKey' => 'parent_id',
        ]);
        $this->belongsTo('AcosTypes', [
            'foreignKey' => 'acos_type_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ChildAcos', [
            'className' => 'Acos',
            'foreignKey' => 'parent_id',
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->notEmptyString('parent_id', "Please choose parent");

        $validator
            ->scalar('model')
            ->maxLength('model', 255)
            ->allowEmptyString('model');

        $validator
            ->scalar('controller')
            ->maxLength('controller', 255)
            ->notEmptyString('controller', "Please choose controller");

        $validator
            ->scalar('alias')
            ->maxLength('alias', 255)
            ->allowEmptyString('alias');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->notEmptyString('status');

        return $validator;
    }

    public function beforeSave(EventInterface $event, $entity, $options)
    {
        if($entity->controller) {
            // Clean controller string
            $tempString = explode("Controller", $entity->controller);
            $tempString = $tempString[0];

            $entity->controller =   $tempString;
            $entity->alias      =   $entity->controller;
        }
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentAcos'));
        $rules->add($rules->existsIn(['acos_type_id'], 'AcosTypes'));

        return $rules;
    }

    /**
     * Returns a list of all controller that are available
     * application integrity.
     *
     * @param --
     * @return Array
     */
    public function getAvailableControllers(): Array
    {
        // get restricted controller.
        $restrictedControllerTable = TableRegistry::get('RestrictedControllers');
        // $dataRestrictedController   =   $restrictedControllerTable->find('list', array(
        //     'keyField'  =>  'id',
        //     'valueField'    =>  'name'
        // ));

        // search already used controller.
        $alreadyUsedController = $this->find('list', array(
            'keyField'  =>  'id',
            'valueField'    =>  'controller'
        ));

        $usedControllerList =   array();
        foreach($alreadyUsedController as $key => $value)
        {
            $usedControllerList[]   =   $value."Controller";
        }

        $usedControllerList = array_merge($usedControllerList);

        // access to controller folders.
        $dir    =   new Folder(App::classPath('Controller')[0]);
        $files  =   $dir->find('.*\.php', true);

        $cleanFiles =   array();

        foreach($files as $key => $value) {
            $tempValue  =   explode("Controller.php", $value);
            $cleanFiles[]   =   $tempValue[0] . "Controller";
        }

        // clean files Controller.php
        $availableController    =   array();
        foreach($cleanFiles as $key => $value) {
            if(!in_array($value, $usedControllerList))  $availableController[$value] = $value;
        }

        return $availableController;
    }

}
