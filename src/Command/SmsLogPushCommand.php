<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;

// untuk log dll
use Cake\Log\Log;

class SmsLogPushCommand extends Command
{

	protected $modelClass = 'SmsLogs';
    var $flag_file_name = "SmsLogPushCommand.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("Start Program sms logpush command");

        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program
            
            Log::setConfig('apilog', [
                'className' => 'File',
                'path' => LOGS,
                'levels' => [],
                'scopes' => ['apiaccess', 'payments'],
                'file' => 'apilog.log',
            ]);

            $io->out("Logic dijalankan");
            // Log::warning('Quing Log ', ['scope' => ['apiaccess']]);

            $this->loadModel("SmsLogs");
            $smsLogs    =   $this->SmsLogs->find('all', array(
                'conditions'    =>  [
                    'SmsLogs.status'   =>  0
                ]
            ));

            if(!empty($smsLogs)) {

                $this->loadModel("SmsCampaigns");

                foreach($smsLogs as $key => $value) {
                    $io->out("Processing Phone : " . $value->phone);

                    $json_data = [
                        "source"        => "YUASA.ID",
                        "destination"   =>  "+" . $value->phone,
                        "text"          =>  $value->text_body, 
                        "encoding"      =>  "AUTO"
                    ];
                    $server_key     =   "zoHmzfq9kcy74ntsuC88x9TG9ggKxMuwUsdpOO0pjg";

                    $data = json_encode($json_data);
                    //https://connect.8x8.com/messaging/api-keys
                    $url = 'https://sms.8x8.com/api/v1/subaccounts/Yuasa_Notif/messages';

                    $headers = array(
                        'Content-Type:application/json',
                        'Authorization: Bearer ' . $server_key // ini api key nya
                    );

                    //CURL request to route notification to FCM connection server (provided by Google)
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $result = curl_exec($ch);

                    $io->out(print_r($result));

                    if ($result === FALSE) {
                        $io->out('Oops! FCM Send Error: ' . curl_error($ch));
                    }

                    $dataTemp   =   $value;
                    $dataTemp->status = 1;
                    $dataTemp->counting_sending     =  intval(1 + intval($value->counting_sending));
                    $dataTemp->last_send    =   date("Y-m-d H:i:s");
                    $this->SmsLogs->save($dataTemp);

                    // ganti informasi di sms Campaign
                    if($value->model == "SmsCampaign") {
                        $smsCampaign    =   $this->SmsCampaigns->find('all', array(
                            'conditions'    =>  [
                                'SmsCampaigns.id'   =>  $value->model_id
                            ]
                        ))
                        ->first();

                        if(!empty($smsCampaign)) {
                            $smsCampaign->status    =   3;
                            $this->SmsCampaigns->save($smsCampaign);
                        }

                    }

                }
            }                

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}