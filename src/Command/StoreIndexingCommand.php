<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

class StoreIndexingCommand extends Command
{

	protected $modelClass = 'ProductTypes';

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {	
    	// $io->out("exit sementara");
    	// exit;
        $name = $args->getArgument('name');

        $io->out("Store Indexing Start Program : " . date("Y-m-d H:i:s"));

        $this->loadModel("Stores");
        $dataStores 	=	$this->Stores->find('all', [
        	'conditions'	=>	[
        		'Stores.is_indexing'	=>	0
        	]
        ])
        ->contain(['StoreCities'	=>	['StoreProvinces']]);

        $this->loadModel("StoreIndexings");

        foreach($dataStores as $key => $store) {
        	
        	$dataStoreIndexing 	=	$this->StoreIndexings->findByStoreId($store->id)->first();

        	if($dataStoreIndexing == false) {
        		$storeIndexing 	=	$this->StoreIndexings->newEmptyEntity();
        	} else {
        		$storeIndexing 	=	$dataStoreIndexing;
        	}

        	$dataTemp['StoreIndexings']	=	array(
        		'store_id'	=>	$store->id,
        		'store_city_id'	=> 	$store->store_city_id,
        		'store_city_name'	=> 	$store->store_city->name,
        		'store_province_id'	=>	$store->store_city->store_province->id,
        		'store_province_name'	=>	$store->store_city->store_province->name,
        		'store_name'	=>	$store->name,
        		'store_address'	=>	$store->address
        	);

        	$storeIndexing = $this->StoreIndexings->patchEntity($storeIndexing, $dataTemp);
        	if($this->StoreIndexings->save($storeIndexing)) {
        		$io->out("Store Id " . $storeIndexing->store_id . " has been saved");

                $store->is_indexing     =   1;
                $this->Stores->save($store);

        	} else {
        		$io->out("Store Id " . $storeIndexing->store_id . " not save ===========");
        	}

        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}