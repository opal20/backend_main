<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;

// untuk log dll
use Cake\Log\Log;

class SmsCampaignQueingCommand extends Command
{

	protected $modelClass = 'SmsCampaigns';
    var $flag_file_name = "SmsCampaignQueing.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("Start Program sms campaign quiing");

        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program
            
            Log::setConfig('apilog', [
                'className' => 'File',
                'path' => LOGS,
                'levels' => [],
                'scopes' => ['apiaccess', 'payments'],
                'file' => 'apilog.log',
            ]);

            $io->out("Logic dijalankan");
            // Log::warning('Quing Log ', ['scope' => ['apiaccess']]);

            $this->loadModel("SmsCampaigns");
            $smsCampaign    =   $this->SmsCampaigns->find('all', array(
                'conditions'    =>  [
                    'SmsCampaigns.send_schedule <= "' . date("Y-m-d H:i:s") . '"',
                    'SmsCampaigns.status'   =>  1
                ]
            ))
            ->contain([
                'SmsCampaignsCustomers' =>  ['CustomerInstantBuys']
            ])
            ->first();

            if(!empty($smsCampaign) && !empty($smsCampaign->sms_campaigns_customers)) {

                // ini langkah masukkan ke dalam sms_logs
                $this->loadModel("SmsLogs");

                // ganti dulu status jadi quing. 
                $smsCampaign->status    =   2;
                $this->SmsCampaigns->save($smsCampaign);

                foreach($smsCampaign->sms_campaigns_customers as $key => $value) {

                    $smsLogData     =   $this->SmsLogs->newEmptyEntity();
                    $smsLogData->model      =   "SmsCampaign";
                    $smsLogData->model_id   =   $smsCampaign->id;
                    $smsLogData->text_body  =   $smsCampaign->message_text;
                    $smsLogData->status     =   0; // jadi nanti di cari yang belum dikirim

                    if(empty($value->customer_instant_buy)) {
                        // berarti adhoc
                        $smsLogData->customer_id    =   0;
                        $smsLogData->phone          =   $value->phone;
                    } else {
                        $smsLogData->customer_id    =   $value->customer_instant_buy->id;
                        $smsLogData->phone          =   $value->customer_instant_buy->phone;
                    }

                    if($this->SmsLogs->save($smsLogData)) {
                        $io->out("Phone : " . $smsLogData->phone . " has been saved");
                    } else {
                        $io->out("Phone : " . $smsLogData->phone . " has not been saved");
                    }
                }
            }                

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}