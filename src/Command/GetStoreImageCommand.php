<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;

class GetStoreImageCommand extends Command
{

	protected $modelClass = 'Stores';
	public $vehicleTypeId 	=	1; // Soalnya lagi car

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("exit sementara");
    	exit;
        
        $generalComponent = new GeneralComponent(new ComponentRegistry());
        $setting            =   $generalComponent->getSetting();

        $io->out($setting->cms_url);

        $io->out("Start Program : " . date("Y-m-d H:i:s"));

        $this->loadModel("TStores");
        $tStoresData 	=	$this->TStores->find('all', array(

        ));

        $this->loadModel("Stores");

        $counter = 0;

        foreach($tStoresData as $key => $value) {
        	

        	$dataStore = $this->Stores->find('all', array(
        		'conditions'	=>	array(
        			'UPPER(Stores.name)'	=>	strtoupper($value->name)
        		)
        	))->first();

        	if($dataStore != false) {
                
        		if(!empty($value->image)) {
                    $tmp_name = $setting->path_content . 'tempImage/' . $value->image;
                    $host = "https://mob.hargaaki.com/images/store/" . $value->image; // <-- Source image url (FIX THIS)
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $host);
                    curl_setopt($ch, CURLOPT_VERBOSE, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // <-- don't forget this
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // <-- and this
                    $result = curl_exec($ch);
                    curl_close($ch);
                    $fp = fopen($tmp_name, 'wb');
                    fwrite($fp, $result);
                    fclose($fp);
                    
                    

                    $ext             = pathinfo($tmp_name, PATHINFO_EXTENSION);
                    $resize                = $generalComponent->ResizeImageContent(
                        $tmp_name,
                        $setting->cms_url,
                        $dataStore->id,
                        "Store",
                        "thumb",
                        "image/jpeg",
                        500,
                        500,
                        "cropResize"
                    );

                    $io->out("https://mob.hargaaki.com/images/store/" . $value->image);
                }

        	}

        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}