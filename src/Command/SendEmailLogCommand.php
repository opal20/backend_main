<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;
use Cake\Mailer\Mailer; //untuk test email

class SendEmailLogCommand extends Command
{

	protected $modelClass = 'Stores';
    var $flag_file_name = "SendEmailLog.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	
        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program

            $this->loadModel("EmailLogs");

            $dataEmail  =   $this->EmailLogs->find('all', array(
                'conditions'    =>  array(
                    'EmailLogs.status'  =>  0
                )
            ))
            ->limit(10)
            ->order([
                'EmailLogs.created asc'
            ]);

            foreach($dataEmail as $key => $value) { 
                $io->out($value->to_email);

                $mailer = new Mailer(['from' => $value->from_email, 'transport' => 'santiyogaemail']);
                $mailer->setFrom([$value->from_email => $value->fromtext])
                    ->setEmailFormat('html')
                    ->setTo($value->to_email)
                    ->setSubject($value->subject)
                    ->deliver($value->text_body);

                $dataTemp   =   $value;
                $dataTemp->status = 1;
                $dataTemp->counting_sending     =  intval(1 + intval($value->counting_sending));
                $dataTemp->last_send    =   date("Y-m-d H:i:s");
                $this->EmailLogs->save($dataTemp);
            }

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        // $io->out(print_r($mailer, true));

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}