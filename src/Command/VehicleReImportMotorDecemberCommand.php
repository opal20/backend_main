<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

// PHPSpreadsheet class
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class VehicleReImportMotorDecemberCommand extends Command
{

	protected $modelClass = 'Vehicles';
	public $vehicleTypeId 	=	2; // Soalnya lagi motor

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("exit sementara");
    	exit;
        $name = $args->getArgument('name');

        $io->out("Start Program : " . date("Y-m-d H:i:s"));

        $file = ROOT . DS . 'webroot' . DS . 'contents' . DS . 'Import' . DS . 'motor_battery_20201130.xlsx';
        if(is_file($file)) {
        	$io->out("Start reading : " . $file);

			$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
			/**  Create a new Reader of the type that has been identified  **/
			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			/**  Load $inputFileName to a Spreadsheet Object  **/
			$spreadsheet = $reader->load($file);
			$worksheet = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);

			$modelNameTemp 	=	"";
			$modelObject 	=	null;

			$vehicleBrandObject 	=	null;
			$vehicleBrandId 		=	null;

			foreach ($worksheet as $key => $row) {
				if($key === 1) {
					continue;
				}

				$vehicleObject 			=	null;
				$vehicleTypeObject		=	null;
				$product 				=	null;

				if($row['A'] != "") {
					$vehicleBrandObject	=	$this->Vehicles->VehicleBrands->find('all', array(
						'conditions'	=>	array(
							'VehicleBrands.name'	=> trim($row['A'])
						)
					))->first();

					if(empty($vehicleBrandObject)) {

						$vehicleBrandObject 	=	$this->Vehicles->VehicleBrands->newEmptyEntity();
						$brandData['VehicleBrands']	=	array(
							'name'	=>	trim($row['A']),
							'vehicle_type_id'	=>	$this->vehicleTypeId
						);
						$vehicleBrandObject 	= 	$this->Vehicles->VehicleBrands->patchEntity($vehicleBrandObject, $brandData, [
			            ]);

			            if($this->Vehicles->VehicleBrands->save($vehicleBrandObject)) {
			            	// $io->out("Brand Saved");
			            } else {
			            	// $io->out("Brand NOT Saved");
			            }

					}

				}
				
				$arrayColor 	=	array(
					"FFFF00FF"	=>	"C",
					"FF00FFFF"	=>	"BB",
					"FF800000"	=>	"MF"
				);

				// default aki
				$colorC 	=	$spreadsheet->getActiveSheet()->getStyle('C' . $key)->getFont()->getColor()->getARGB();

				// $io->out($colorC . " adalah warnanya dan ini brand aki " . trim($row['C']) . " dan ini key" . $key);

				// ini default nya
				if(trim($row['C']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorC];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['C']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if(!isset($product->name)) {
						$io->out("key =	" . $key . " product_name = " . $product->name . " search product code = " . $productCode . " vehicle brand = " . $vehicleBrandObject->id . " color  :" . $colorC);
					}

					$vehicle 	=	$this->Vehicles->find()
						->where([
							'Vehicles.name'	=>	trim($row['B']),
							'Vehicles.vehicle_type_id'	=>	$this->vehicleTypeId, 
							'Vehicles.vehicle_brand_id'	=>	$vehicleBrandObject->id
						])
						->first();

					if($vehicle == false) {
						$vehicleObject 	=	$this->Vehicles->newEmptyEntity();	
					} else {
						$vehicleObject 	=	$vehicle;
					}

					
					$vehicleData['Vehicles']	=	array(
						'name'	=>	trim($row['B']),
						'vehicle_type_id'	=>	$this->vehicleTypeId,
						'vehicle_brand_id'	=>	$vehicleBrandObject->id
					);

					$vehicleObject 	= 	$this->Vehicles->patchEntity($vehicleObject, $vehicleData, [
			            ]);

					if($this->Vehicles->save($vehicleObject)) {
		            	// $io->out("Vehicle Saved");

		            	if($product != false) {
							$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
			            	$productsVehiclesObject->product_id 	=	$product->id;
			            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
			            	$productsVehiclesObject->is_default 	=	1;

			            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
			            		// $io->out("New association");
			            	}
						}

		            } else {
		            	$io->out(print_r($vehicleObject->getErrors()));
		            	$io->out("Vehicle NOT Saved");
		            }

				}

				$colorD 	=	$spreadsheet->getActiveSheet()->getStyle('D' . $key)->getFont()->getColor()->getARGB();
				
				// Kolom upgrade
				if(trim($row['D']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorC];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['D']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if($product != false) {
						$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
		            	$productsVehiclesObject->product_id 	=	$product->id;
		            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
		            	$productsVehiclesObject->is_default 	=	0;

		            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
		            		// $io->out("New association");
		            	}
					}

				}

				$colorE 	=	$spreadsheet->getActiveSheet()->getStyle('E' . $key)->getFont()->getColor()->getARGB();

				// Kolom upgrade
				if(trim($row['E']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorE];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['E']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if($product != false) {
						$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
		            	$productsVehiclesObject->product_id 	=	$product->id;
		            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
		            	$productsVehiclesObject->is_default 	=	0;

		            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
		            		// $io->out("New association");
		            	}
					}
				}

				$colorF 	=	$spreadsheet->getActiveSheet()->getStyle('F' . $key)->getFont()->getColor()->getARGB();

				// Kolom upgrade
				if(trim($row['F']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorF];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['F']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if($product != false) {
						$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
		            	$productsVehiclesObject->product_id 	=	$product->id;
		            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
		            	$productsVehiclesObject->is_default 	=	0;

		            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
		            		// $io->out("New association");
		            	}
					}

				}

				$colorG 	=	$spreadsheet->getActiveSheet()->getStyle('G' . $key)->getFont()->getColor()->getARGB();

				// Kolom upgrade
				if(trim($row['G']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorG];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['G']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if($product != false) {
						$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
		            	$productsVehiclesObject->product_id 	=	$product->id;
		            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
		            	$productsVehiclesObject->is_default 	=	0;

		            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
		            		// $io->out("New association");
		            	}
					}

				}

				$colorH 	=	$spreadsheet->getActiveSheet()->getStyle('H' . $key)->getFont()->getColor()->getARGB();

				// Kolom upgrade
				if(trim($row['H']) != "") {
					// $io->out("key = " . $key . " row value = " . $row['B'] . " & color = " . $colorB);
					
					$productTypeCode	=	$arrayColor[$colorH];
					$productCode 		=	$productTypeCode . "-" . str_replace(" ", "", trim($row['H']));

					$product 	=	$this->Vehicles->Products->find('all', array(
						'conditions'	=>	array(
							"Products.product_code"	=>	$productCode
						)
					))->first();

					if($product != false) {
						$productsVehiclesObject 				=	$this->Vehicles->ProductsVehicles->newEmptyEntity();
		            	$productsVehiclesObject->product_id 	=	$product->id;
		            	$productsVehiclesObject->vehicle_id 	=	$vehicleObject->id;
		            	$productsVehiclesObject->is_default 	=	0;

		            	if($this->Vehicles->ProductsVehicles->save($productsVehiclesObject)) {
		            		// $io->out("New association");
		            	}
					}

				}

			}

			$io->out("Finish reading : " . $file);

        } else {
        	$io->out("File not found");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}