<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

// PHPSpreadsheet class
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class ImportStoreCommand extends Command
{

	protected $modelClass = 'Stores';
	public $vehicleTypeId 	=	1; // Soalnya lagi car

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("exit sementara");
    	exit;
        $name = $args->getArgument('name');

        $io->out("Start Program : " . date("Y-m-d H:i:s"));

        $file = ROOT . DS . 'webroot' . DS . 'contents' . DS . 'Import' . DS . 'import_store.xlsx';
        if(is_file($file)) {
        	$io->out("Start reading : " . $file);

			$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
			/**  Create a new Reader of the type that has been identified  **/
			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			/**  Load $inputFileName to a Spreadsheet Object  **/
			$spreadsheet = $reader->load($file);
			$worksheet = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);

			foreach ($worksheet as $key => $row) {
				if($key === 1) {
					continue;
				}

				if(empty($row['C'])) {
					continue;
				}

				$provinceObject 		=	null;
				$storeCitiesObject 		=	null;
				$storeObject 			=	null;

				$this->loadModel("StoreProvinces");
				$provinceObject 	=	$this->StoreProvinces->find()
					->where([
						'StoreProvinces.name' => trim($row['C'])
					])
					->first();

				if(empty($provinceObject)) {

					$provinceObject 	=	$this->StoreProvinces->newEmptyEntity();
					$storeProvinceData['StoreProvinces']	=	array(
						'name'		=>	trim($row['C']),
						'status'	=>	1
					);
					$provinceObject 	= 	$this->StoreProvinces->patchEntity($provinceObject, $storeProvinceData, [
		            ]);

		            if($this->StoreProvinces->save($provinceObject)) {
		            	// $io->out("Brand Saved");
		            } else {
		            	// $io->out("Brand NOT Saved");
		            }

				}

				$this->loadModel("StoreCities");
				$storeCitiesObject 	=	$this->StoreCities->find()
					->where([
						'StoreCities.name' => trim($row['D']),
						'StoreCities.store_province_id'	=>	$provinceObject->id
					])
					->first();

				if(empty($storeCitiesObject)) {

					$storeCitiesObject 	=	$this->StoreCities->newEmptyEntity();
					$storeCitiesData['StoreCities']	=	array(
						'name'		=>	trim($row['D']),
						'store_province_id'	=>	$provinceObject->id,
						'status'	=>	1
					);
					$storeCitiesObject 	= 	$this->StoreCities->patchEntity($storeCitiesObject, $storeCitiesData, [
		            ]);

		            if($this->StoreCities->save($storeCitiesObject)) {
		            	// $io->out("Brand Saved");
		            } else {
		            	// $io->out("Brand NOT Saved");
		            }

				}

				$this->loadModel("Stores");
				$storeObject 	=	$this->Stores->find()
					->where([
						'Stores.company_id' 	=> 	trim($row['B']),
						'Stores.store_city_id'	=>	$storeCitiesObject->id,  
						'Stores.name'			=>	trim($row['E'])
					])
					->first();


				if(empty($storeObject)) {

					$storeObject 	=	$this->Stores->newEmptyEntity();
					$storeData['Stores']	=	array(
						'name'			=>	trim($row['E']),
						'company_id'	=>	trim($row['B']),
						'store_city_id'	=>	$storeCitiesObject->id,
						'address'		=>	trim($row['F']),
						'phone_1'		=>	trim($row['G']),
						'phone_2'		=>	trim($row['H']),
						'email'		=>	trim($row['I']),
						'lng'		=>	trim($row['J']),
						'lat'		=>	trim($row['K']),
						'status'		=>	1
					);
					$storeObject 	= 	$this->Stores->patchEntity($storeObject, $storeData, [
		            ]);

		            if($this->Stores->save($storeObject)) {
		            	$io->out(trim($row['E']) . " has been saved");
		            } else {
		            	$io->out(print_r($storeObject->getErrors(), true));
		            }

				}

			}

			$io->out("Finish reading : " . $file);

        } else {
        	$io->out("File not found");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}