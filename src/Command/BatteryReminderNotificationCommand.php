<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\ActionComponent;

class BatteryReminderNotificationCommand extends Command
{

	protected $modelClass = 'BatteryReminders';
    var $flag_file_name = "BatteryReminderNotification.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("Start Program battery reminder notification command");

        $this->Action = new ActionComponent(new ComponentRegistry()); // initializze action component

        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program

            $io->out("Logic dijalankan");

            $this->loadModel("BatteryReminders");



            $batteryRemindersAkiBasah   =   $this->BatteryReminders->find('all')
                ->where([
                    "DATEDIFF(CURDATE(), installation_date) % 30 = 0",
                    "Products.product_type_id in (1, 4)"
                ])
                ->contain(["VehicleBrands", "Vehicles", "Products", "Customers"]);

            foreach($batteryRemindersAkiBasah as $batteryReminder)  {
                
                if(isset($batteryReminder->customer) && !empty($batteryReminder->customer->push_token)) {

                    // cari umur battery nya dulu disini. 
                    $date1 = $batteryReminder->installation_date;
                    $date2 = date('Y-m-d'); // current date

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);

                    $umurBattere = (($year2 - $year1) * 12) + ($month2 - $month1);

                    $io->out($batteryReminder->id . "Umur battery anda " . $umurBattere . " bulan");

                    $vehicleBrand = "";
                    if(isset($batteryReminder->vehicle_brand->name)) {
                        $vehicleBrand = $batteryReminder->vehicle_brand->name;
                    } else {
                        $vehicleBrand   =   $batteryReminder->vehicle_brand_other;
                    }

                    $vehicle    =   "";
                    if(isset($batteryReminder->vehicle->name)) {
                        $vehicle = $batteryReminder->vehicle->name;
                    } else {
                        $vehicle   =   $batteryReminder->vehicle_other;
                    }

                    $kendaraan = $vehicleBrand . " - " . $vehicle;

                    $this->Action->pushNotifSave(
                        "battery_reminder",
                        $batteryReminder->customer->push_token,
                        $batteryReminder->customer->id,
                        array(
                            'kendaraan' =>  $kendaraan, 
                            'umur_battere'  =>  $umurBattere
                        ),
                        array(),
                        "BatteryReminder",
                        $batteryReminder->id
                    );

                }
            }

            $batteryRemindersAkiKering   =   $this->BatteryReminders->find('all')
                ->where([
                    "DATEDIFF(CURDATE(), installation_date) % 90 = 0",
                    "Products.product_type_id in (2, 5)"
                ])
                ->contain(["VehicleBrands", "Vehicles", "Products", "Customers"]);

            foreach($batteryRemindersAkiKering as $batteryReminder)  {
                
                if(isset($batteryReminder->customer) && !empty($batteryReminder->customer->push_token)) {

                    // cari umur battery nya dulu disini. 
                    $date1 = $batteryReminder->installation_date;
                    $date2 = date('Y-m-d'); // current date

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);

                    $umurBattere = (($year2 - $year1) * 12) + ($month2 - $month1);

                    $io->out($batteryReminder->id . "Umur battery anda " . $umurBattere . " bulan");

                    $vehicleBrand = "";
                    if(isset($batteryReminder->vehicle_brand->name)) {
                        $vehicleBrand = $batteryReminder->vehicle_brand->name;
                    } else {
                        $vehicleBrand   =   $batteryReminder->vehicle_brand_other;
                    }

                    $vehicle    =   "";
                    if(isset($batteryReminder->vehicle->name)) {
                        $vehicle = $batteryReminder->vehicle->name;
                    } else {
                        $vehicle   =   $batteryReminder->vehicle_other;
                    }

                    $kendaraan = $vehicleBrand . " - " . $vehicle;

                    $this->Action->pushNotifSave(
                        "battery_reminder",
                        $batteryReminder->customer->push_token,
                        $batteryReminder->customer->id,
                        array(
                            'kendaraan' =>  $kendaraan, 
                            'umur_battere'  =>  $umurBattere
                        ),
                        array(),
                        "BatteryReminder",
                        $batteryReminder->id
                    );

                }
            }

            $io->out("Logic Berhenti");

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}