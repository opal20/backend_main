<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;

class GoogleCloudMessagingCommand extends Command
{

	protected $modelClass = 'Stores';
    var $flag_file_name = "GoogleCloudMessaging.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("Start Program kirim google gcm");

        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program

            $this->loadModel("PushNotificationLogs");

            $dataPushNotif  =   $this->PushNotificationLogs->find('all', array(
                'conditions'    =>  array(
                    'PushNotificationLogs.status'  =>  0
                )
            ))
            ->limit(10)
            ->order([
                'PushNotificationLogs.created asc'
            ]);

            foreach($dataPushNotif as $key => $value) { 
                $io->out($value->customer_id);

                // mulai curl
                
                $json_data = [
                    "to" => $value->push_token,
                    "notification" => [
                        "body" => $value->text_body,
                        "title" => $value->subject,
                        // "icon" => "ic_launcher"
                    ],
                    "data" => [
                        "click_action"  =>  "FLUTTER_NOTIFICATION_CLICK"
                    ]
                ];

                $data = json_encode($json_data);
                //FCM API end-point
                $url = 'https://fcm.googleapis.com/fcm/send';
                //https://fcm.googleapis.com/v1/projects/myproject-b5ae1/messages:send
                // $url     =   'https://fcm.googleapis.com/v1/projects/yuasa-santiyoga/messages:send';
                //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
                $server_key = 'AAAA4g6Wr9E:APA91bHvc10Q3U0CKYIYPNtXQNkugjGGBtIpc1QAKlpyznYFuachCELYIcEJ1e7BRnVzATzQahUizN_btSi8XXMreX6AScnW1NVoYelf5XbggHzsecRnOLeL2wN7ZPa9JRFmBnXBuGc4';
                //header with content_type api key
                $headers = array(
                    'Content-Type:application/json',
                    'Authorization:key='.$server_key
                );
                //CURL request to route notification to FCM connection server (provided by Google)
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $result = curl_exec($ch);

                if ($result === FALSE) {
                    $io->out('Oops! FCM Send Error: ' . curl_error($ch));
                }

                // End curl

                $dataTemp   =   $value;
                $dataTemp->status = 1;
                $dataTemp->counting_sending     =  intval(1 + intval($value->counting_sending));
                $dataTemp->last_send    =   date("Y-m-d H:i:s");
                $this->PushNotificationLogs->save($dataTemp);
            }

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}