<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;
use Cake\Mailer\Mailer; //untuk test email

class TestEmailCommand extends Command
{

	protected $modelClass = 'Stores';
	public $vehicleTypeId 	=	1; // Soalnya lagi car

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	// $io->out(print_r($this->request));
        
        $mailer = new Mailer(['from' => 'apps@santiyoga.com', 'transport' => 'santiyogaemail']);
        $mailer->setFrom(['apps@santiyoga.com' => 'No Reply'])
            ->setTo('kossa@boleh.net.id')
            ->setSubject('Test email terbaru bang ting ' . time())
            ->deliver('My message testing bang ting' . time());
        $io->out(print_r($mailer, true));

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}