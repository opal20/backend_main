<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

class ProductIndexingCommand extends Command
{

	protected $modelClass = 'ProductTypes';

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {	
    	// $io->out("exit sementara");
    	// exit;
        $name = $args->getArgument('name');

        $io->out("Store Indexing Start Program : " . date("Y-m-d H:i:s"));

        $this->loadModel("Products");
        $dataProducts 	=	$this->Products->find('all', [
        	'conditions'	=>	[
        		'Products.is_indexing'	=>	0
        	]
        ])
        ->contain(
            [
                'Vehicles'	=>	['VehicleBrands'],
                'ProductTypes'
            ]
        )
        ->toArray();

        $this->loadModel("ProductIndexings");

        foreach($dataProducts as $key => $product) {
        	
        	$dataProductIndexing 	=	$this->ProductIndexings->findByProductId($product->id)->first();

        	if($dataProductIndexing == false) {
        		$productIndexing 	=	$this->ProductIndexings->newEmptyEntity();
        	} else {
        		$productIndexing 	=	$dataProductIndexing;
        	}

            $vehicle_string    =    "";
            foreach($product->vehicles as $vehicle) {
                $vehicle_string .= " - " . $vehicle->vehicle_brand->name . " " . $vehicle->name;
            }

        	$dataTemp['ProductIndexings']	=	array(
        		'product_id'	=>	$product->id,
        		'product_name'	=> 	$product->name,
                'product_type_id'  =>  $product->product_type->id,
                'product_type_name'  =>  $product->product_type->name,
        		'vehicles'	=> 	$vehicle_string,
        	);

        	$productIndexing = $this->ProductIndexings->patchEntity($productIndexing, $dataTemp);
        	if($this->ProductIndexings->save($productIndexing)) {
        		$io->out("Product Id " . $productIndexing->product_id . " has been saved");

                $product->is_indexing     =   1;
                $this->Products->save($product);

        	} else {
        		$io->out("Product Id " . $productIndexing->product_id . " not save ===========");
                $io->out(print_r($productIndexing->getErrors(), true));
        	}

        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}