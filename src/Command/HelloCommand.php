<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

// PHPSpreadsheet class
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class HelloCommand extends Command
{

	protected $modelClass = 'ProductTypes';

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {	
    	$io->out("exit sementara");
    	exit;
        $name = $args->getArgument('name');

        $io->out("Start Program : " . date("Y-m-d H:i:s"));

        $file = ROOT . DS . 'webroot' . DS . 'contents' . DS . 'Import' . DS . 'aki_spesifikasi_20201214.xlsx';
        if(is_file($file)) {
        	$io->out("Start reading : " . $file);

			$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
			/**  Create a new Reader of the type that has been identified  **/
			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			/**  Load $inputFileName to a Spreadsheet Object  **/
			$spreadsheet = $reader->load($file);
			$worksheet = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);

			$modelNameTemp 	=	"";
			$modelObject 	=	null;

			foreach ($worksheet as $key => $row) {
				if($key === 1) {
					continue;
				}

				if($modelNameTemp !== trim($row['A'])) {
					$modelNameTemp	=	trim($row['A']);
					$modelObject	=	$this->ProductTypes->find('all', array(
						'conditions'	=>	array(
							'ProductTypes.id'	=> trim($row['A'])
						)
					))->first();

				}

				$this->loadModel("Products");

				$productCode 		=	$modelObject->code . "-" . str_replace(" ", "", trim($row['B']));

				$dataProduct 	=	$this->Products->find('all', [
					'conditions'	=>	array(
						'Products.product_code'	=>	$productCode
					)
				])->first();

				if($dataProduct == false) {
					$io->out("product code = ".$productCode." key = " . $key . " row value = " . $row['H'] . " data product " . $dataProduct->id );
				}


				// Update tokped dan price. 
				$dataProduct->tokopedia 	=	trim($row['L']);
				$dataProduct->price 		=	$spreadsheet->getActiveSheet()->getCell('H' . $key)->getValue();
				$io->out($this->Products->save($dataProduct));
				

				// $product 	=	$this->ProductTypes->Products->newEmptyEntity();
				// $productData['Products']	=	array(
				// 	'name'				=>	$row['B'],
				// 	'tegangan'			=>	$row['C'],
				// 	'kapasitas'			=>	$row['D'],
				// 	'panjang'			=>	$row['E'],
				// 	'lebar'				=>	$row['F'],
				// 	'tinggi'			=>	$row['G'],
				// 	'product_code'		=>	$row['B'],
				// 	'price'				=>	$row['H'],
				// 	'bukalapak'				=>	$row['I'],
				// 	'shopee'				=>	$row['J'],
				// 	'blibli'				=>	$row['K'],
				// 	'product_type_id'	=>	$modelObject->id,
				// );
				// $product 	= 	$this->ProductTypes->Products->patchEntity($product, $productData, [
	   //          ]);

	   //          if ($this->ProductTypes->Products->save($product)) {
	   //          	$io->out("SAVED");
	   //          } else {

	   //          	debug($product->getErrors());
	   //          	$io->out("NOT SAVED");
	   //          }

			}

			$io->out("Finish reading : " . $file);

        } else {
        	$io->out("File not found");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}