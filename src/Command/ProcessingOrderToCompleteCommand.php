<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\GeneralComponent;

class ProcessingOrderToCompleteCommand extends Command
{

	protected $modelClass = 'Orders';
    var $flag_file_name = "ProcessingOrderToComplete.txt";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	$io->out("Start Program kirim processing lebih dari 3 hari");

        // Check cron jalan atau tidak . 
        //FLAG LOCATION
        $path_tmp   =   TMP;
        if(!is_dir($path_tmp))
            mkdir($path_tmp,0755);

        $path_cache =   $path_tmp."cache/";
        if(!is_dir($path_cache))
            mkdir($path_cache,0755);

        $path_cron  =   $path_cache."cron/";
        if(!is_dir($path_cron))
            mkdir($path_cron,0755);

        $flag                       =   $path_cron.$this->flag_file_name;

        if(file_exists($flag))
        {
            $data                   =   explode("|",trim(file_get_contents($flag)));
            $flag_status            =   $data[0];
            $last_update            =   (int)$data[1];
        }
        else
        {
            $flag_status            =   "0";
            $last_update            =   0;
            $handle                 =   fopen($flag, 'wb');
            fwrite($handle,"0|0");
            fclose($handle);
        }

        if($flag_status == "1")
        {
            $long               =   (time()-$last_update)/60;
            if($long > 30)
            {
                $io->out("ada cron jalan");
                if (is_writable($flag))
                {
                    if (!$handle = fopen($flag, 'wb')) {
                         exit;
                    }
                    if (fwrite($handle, "0|".time()) === FALSE) {
                        exit;
                    }
                    fclose($handle);
                }
            }
        }

        if($flag_status == "0")
        {
            //WRITE FLAG 1 TO FILE
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }

                if (fwrite($handle, "1|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

            // Start logic program

            $io->out("Logic dijalankan");

            $this->loadModel("Orders");

            // cari order yang sudah 3 hari yang lalu dengan status processing
            $orders = $this->Orders->find('all', [
                'conditions'    =>  [
                    'Orders.order_status_id' => 3, // status processing adalah 3
                    'DATE(created)  <= DATE(NOW()) - INTERVAL 3 DAY' 
                ]
            ]);

            foreach($orders as $order) {
                $io->out("Order key = " . $order->order_key . " created = " . $order->created);

                $dataTemp   =   $order;
                $dataTemp->order_status_id = 5;
                $this->Orders->save($dataTemp);

            }

            // end logic program
            
            if (is_writable($flag)) {
                if (!$handle = fopen($flag, 'wb')) {
                     exit;
                }
                if (fwrite($handle, "0|".time()) === FALSE) {
                    exit;
                }
                fclose($handle);
            }

        } else {
            $io->out("Another cron is running");
        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}