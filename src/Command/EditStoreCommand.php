<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

// PHPSpreadsheet class
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class EditStoreCommand extends Command
{

	protected $modelClass = 'Stores';
	public $vehicleTypeId 	=	1; // Soalnya lagi car

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
    	// $io->out("exit sementara");
    	// exit;

        $io->out("Start Program : " . date("Y-m-d H:i:s"));

        $this->loadModel("TStores");
        $tStoresData 	=	$this->TStores->find('all', array(

        ));

        $this->loadModel("Stores");

        $counter = 0;

        foreach($tStoresData as $key => $value) {
        	

        	$dataStore = $this->Stores->find('all', array(
        		'conditions'	=>	array(
        			'UPPER(Stores.name)'	=>	strtoupper($value->name)
        		)
        	))->first();

        	if($dataStore != false) {
        		$counter++;
        		$io->out($value->name . " itu nomor " . $counter . " dan id " . $dataStore->id);

        		$dataStore->phone_1 	=	$value->phone_1;
        		$dataStore->phone_2 	=	$value->phone_2;
        		$dataStore->lat 		=	$value->geo_lat;
        		$dataStore->lng 		=	$value->geo_long;

        		$this->Stores->save($dataStore);

        	}

        }

        $io->out("Finish Program : " . date("Y-m-d H:i:s"));

    }
}