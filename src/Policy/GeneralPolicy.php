<?php
declare(strict_types=1);

namespace App\Policy;

use Authorization\IdentityInterface;
use Cake\Log\Log; // to log stuff

/**
 * Article policy
 */
class GeneralPolicy extends AppPolicy
{
    var $acosAliasName  =   "";

    /**
     * Check if $user can create Article
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Article $article
     * @return bool
     */
    public function canAdd(IdentityInterface $user)
    {
        if(isset($this->matrixArosAcos[$this->acosAliasName]["_create"]) && $this->matrixArosAcos[$this->acosAliasName]["_create"] == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if $user can create Article
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Article $article
     * @return bool
     */
    public function canCreate(IdentityInterface $user)
    {
        if(isset($this->matrixArosAcos[$this->acosAliasName]["_create"]) && $this->matrixArosAcos[$this->acosAliasName]["_create"] == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if $user can update Article
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Article $article
     * @return bool
     */
    public function canUpdate(IdentityInterface $user)
    {
        // logged in users can edit their own articles.
        // return $this->isAuthor($user, $article);
        if(isset($this->matrixArosAcos[$this->acosAliasName]["_update"]) && $this->matrixArosAcos[$this->acosAliasName]["_update"] == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if $user can delete Article
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Article $article
     * @return bool
     */
    public function canDelete(IdentityInterface $user)
    {
        // logged in users can delete their own articles.
        // return $this->isAuthor($user, $article);
        if(isset($this->matrixArosAcos[$this->acosAliasName]["_delete"]) && $this->matrixArosAcos[$this->acosAliasName]["_delete"] == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if $user can view Article
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\Article $article
     * @return bool
     */
    public function canView(IdentityInterface $user)
    {
        if(isset($this->matrixArosAcos[$this->acosAliasName]["_read"]) && $this->matrixArosAcos[$this->acosAliasName]["_read"] == 1) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    public function canRead(IdentityInterface $user)
    {   

        if(isset($this->matrixArosAcos[$this->acosAliasName]["_read"]) && $this->matrixArosAcos[$this->acosAliasName]["_read"] == 1) {
            return true;
        } else {
            return false;
        }
    }

    protected function isAuthor(IdentityInterface $user, Article $article)
    {
        return $article->user_id === $user->getIdentifier();
    }
}
