<?php
namespace App\Policy;

use Authorization\IdentityInterface;

// additional 
use Authorization\Policy\BeforePolicyInterface; // add before policy interface
use Cake\Cache\Cache; // For cache system engine
use Cake\Log\Log; // to log stuff
use Cake\ORM\TableRegistry; // to direct call an entity

/**
 * _create
 * _read
 * _update
 * _delete
 */

class AppPolicy implements BeforePolicyInterface
{
    var $matrixArosAcos =   array();

	public function before($user, $resource, $action)
    {
        // able to get aro_id here. 
        $aros_id = $user->get('aro_id');
        if(!empty($aros_id)) {
            $this->matrixArosAcos =    Cache::read($aros_id, 'arosInformation');
            if($this->matrixArosAcos === null) {
                $arosTable      =   TableRegistry::get('Aros');
                $arosData       =   $arosTable->get($aros_id, array(
                    'contain'   =>   array('ArosAcos' => array('Acos'))
                ));

                $matrixArosAcos =   array();

                foreach($arosData->aros_acos as $aros_acos) {
                    $matrixArosAcos[$aros_acos->aco->alias]['_create']  =   $aros_acos->_create;
                    $matrixArosAcos[$aros_acos->aco->alias]['_read']    =   $aros_acos->_read;
                    $matrixArosAcos[$aros_acos->aco->alias]['_update']  =   $aros_acos->_update;
                    $matrixArosAcos[$aros_acos->aco->alias]['_delete']  =   $aros_acos->_delete;

                    $matrixArosAcos[$aros_acos->aco->id]['_create']  =   $aros_acos->_create;
                    $matrixArosAcos[$aros_acos->aco->id]['_read']    =   $aros_acos->_read;
                    $matrixArosAcos[$aros_acos->aco->id]['_update']  =   $aros_acos->_update;
                    $matrixArosAcos[$aros_acos->aco->id]['_delete']  =   $aros_acos->_delete;
                    
                }

                $this->matrixArosAcos = $matrixArosAcos;

                Cache::write($aros_id, $matrixArosAcos, "arosInformation");
                $this->matrixArosAcos = $matrixArosAcos;
            }
        }
        else {
            
            return false;
        }
    }
}