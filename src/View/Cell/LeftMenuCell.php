<?php 

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Log\Log;
use Cake\Cache\Cache; // For cache system engine

class LeftMenuCell extends Cell
{

    public function initialize(): void
    {
        parent::initialize();
    }

    public function display($loggedInUser = false, $activeParentMenuId  =   false)
    {
        
    	$this->loadModel("CmsMenus");
    	// get siapa yang login ketauan kah?
        // var_dump($loggedInUser); // dikirim dari metronic_left_menu
        // get acco list dulu dari sini. 
        $access     =   $this->__getAccess($loggedInUser->aro_id);

        //FILTER CMS MENU BY ACOS
        $listAcoId          =   array();
        foreach($access as $aco_id  =>  $access)
        {
            if($access["_read"]=="1" && is_integer($aco_id))
                $listAcoId[]    =   $aco_id;
        }

    	$cmsMenus 	=	$this->CmsMenus->find('children', 
    		[
    			'for'	=>	1,
                'conditions'    =>  array(
                    'OR'    =>  array(
                        'CmsMenus.aco_id IN'    =>  $listAcoId,
                        'CmsMenus.is_group_separator'   =>  1
                    )
                )
    		]
    	)
    	->find('threaded')
    	->contain(['Acos'])
    	->toArray();

    	$this->set(compact(
    		'cmsMenus', 'loggedInUser', 'activeParentMenuId'
    	));
    }


    // This function also from AppPolicy.php
    private function __getAccess($aro_id) {
        // able to get aro_id here. 
        $matrixArosAcos = false;

        $aros_id = $aro_id;
        if(!empty($aros_id)) {
            $matrixArosAcos =    Cache::read($aros_id, 'arosInformation');

            if($matrixArosAcos === null) {
                $arosTable      =   TableRegistry::get('Aros');
                $arosData       =   $arosTable->get($aros_id, array(
                    'contain'   =>   array('ArosAcos' => array('Acos'))
                ));

                $matrixArosAcos =   array();

                foreach($arosData->aros_acos as $aros_acos) {
                    $matrixArosAcos[$aros_acos->aco->alias]['_create']  =   $aros_acos->_create;
                    $matrixArosAcos[$aros_acos->aco->alias]['_read']    =   $aros_acos->_read;
                    $matrixArosAcos[$aros_acos->aco->alias]['_update']  =   $aros_acos->_update;
                    $matrixArosAcos[$aros_acos->aco->alias]['_delete']  =   $aros_acos->_delete;

                    $matrixArosAcos[$aros_acos->aco->id]['_create']  =   $aros_acos->_create;
                    $matrixArosAcos[$aros_acos->aco->id]['_read']    =   $aros_acos->_read;
                    $matrixArosAcos[$aros_acos->aco->id]['_update']  =   $aros_acos->_update;
                    $matrixArosAcos[$aros_acos->aco->id]['_delete']  =   $aros_acos->_delete;

                }

                Cache::write($aros_id, $matrixArosAcos, "arosInformation");
            }
        }

        return $matrixArosAcos;

    }

}