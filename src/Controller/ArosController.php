<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Aros Controller
 *
 * @property \App\Model\Table\ArosTable $Aros
 * @method \App\Model\Entity\Aro[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArosController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event) 
    {
        parent::beforeFilter($event);
        $this->Authorization->skipAuthorization();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $aros = $this->paginate($this->Aros, [
            'conditions'    =>  [
                'id !=' =>  1
            ]
        ]);

        $this->set(compact('aros'));
    }

    /**
     * View method
     *
     * @param string|null $id Aro id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $aro = $this->Aros->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('aro'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aro = $this->Aros->newEmptyEntity();
        if ($this->request->is('post')) {
            $aro = $this->Aros->patchEntity($aro, $this->request->getData());
            if ($this->Aros->save($aro)) {
                $this->Flash->success(__('The aro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The aro could not be saved. Please, try again.'));
        }

        // get parent. 
        $parentList     =   $this->Aros->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias', 
            'keyPath' =>  'id'
        ]);

        $this->set(compact('aro', 'parentList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Aro id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $aro = $this->Aros->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $aro = $this->Aros->patchEntity($aro, $this->request->getData());
            if ($this->Aros->save($aro)) {
                $this->Flash->success(__('The aro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The aro could not be saved. Please, try again.'));
        }
        
        // get parent. 
        $parentList     =   $this->Aros->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias', 
            'keyPath' =>  'id'
        ]);

        $this->set(compact('aro', 'parentList'));

    }

    /**
     * Delete method
     *
     * @param string|null $id Aro id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $aro = $this->Aros->get($id);
        if ($this->Aros->delete($aro)) {
            $this->Flash->success(__('The aro has been deleted.'));
        } else {
            $this->Flash->error(__('The aro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
