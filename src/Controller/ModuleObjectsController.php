<?php

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine

class ModuleObjectsController extends AppController
{

	var $name = "ModuleObjects";
	var $modelClass	=	"ArosAcos";

	public $paginate = [
        'order' => [
            'Users.id' => 'desc'
        ],
        'sortableFields' =>  [
            'Users.id', 'Aros.alias', 'Users.email', 'Users.created', 'Users.status'
        ]
    ];

	public function initialize(): void
	{
		parent::initialize();

		$this->loadComponent('Paginator');
		$this->loadComponent('Flash');

	}

	public function beforeFilter(\Cake\Event\EventInterface $event)
	{
		parent::beforeFilter($event);
		$this->Authorization->skipAuthorization();

		$this->viewBuilder()->setLayout('metronic_main');
		$this->set('activeParentMenuId', 5);

	}

	public function index()
	{

		$this->loadModel("Users");
		$this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

		$this->request->allowMethod(['get']);

		$arosList	=	$this->ArosAcos->Aros->find('all', array(
			'conditions'	=>	array(
				'Aros.parent_id is not NULL'
			)
		));
		$this->loadComponent('Paginator');
		$arosListData	=	$this->Paginator->paginate($arosList);

		$this->set(compact('arosListData', 'arosList'));

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->setLayout('ajax');
            $this->render('listItem');
        } else if($this->request->is('get') && $this->request->getQuery('excel') != null && $this->request->getQuery('excel') == 1) {

            $this->viewBuilder()->setLayout('ajax');
            $this->set('filename', $this->actionTitle . "_list.xlsx");
            $this->render('listItemExcel');

        }

	}

	function editAccess($id = null)
	{

		$this->loadModel("Users");
		$this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

		$aros 	=	$this->ArosAcos->Aros
			->findById($id)
			->contain(['ArosAcos'])
			->firstOrFail();


		// retrieve all the data
		$arosAcos 	=	$this->ArosAcos
			->findByAroId($id);

		if($this->request->is(['post', 'put'])) {

			$this->ArosAcos->Aros->patchEntity($aros, $this->request->getData(), [
				'associated'	=>	[
					'ArosAcos'
				]
	        ]);

			if($this->ArosAcos->Aros->save($aros)) {

				// Update user cached.
				// Cache::read('aros_informations_' . $aros_id, 'short')
				Cache::delete($id, 'arosInformation');
				$arosAcosData	=	$this->ArosAcos->findByAroId($id)
					->contain('Acos');

				$matrixArosAcosCache =   array();

                foreach($arosAcosData as $aros_acos) {
                    $matrixArosAcosCache[$aros_acos->aco->alias]['_create'] 	=   $aros_acos->_create;
                    $matrixArosAcosCache[$aros_acos->aco->alias]['_read'] 		=   $aros_acos->_read;
                    $matrixArosAcosCache[$aros_acos->aco->alias]['_update'] 	=   $aros_acos->_update;
                    $matrixArosAcosCache[$aros_acos->aco->alias]['_delete'] 	=   $aros_acos->_delete;

                    $matrixArosAcosCache[$aros_acos->aco->id]['_create']  =   $aros_acos->_create;
                    $matrixArosAcosCache[$aros_acos->aco->id]['_read']    =   $aros_acos->_read;
                    $matrixArosAcosCache[$aros_acos->aco->id]['_update']  =   $aros_acos->_update;
                    $matrixArosAcosCache[$aros_acos->aco->id]['_delete']  =   $aros_acos->_delete;
                }

                Cache::write($id, $matrixArosAcosCache, 'arosInformation');

				$this->Flash->metronicSuccess(__('Access List has been updated'));
				return $this->redirect(['action'	=>	'index']);
			} else {
				$this->Flash->metronicError(__('The module objects could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: MO1']]);
			}
		}

		// we have to load all the acos first.
		$setting = array();
	if($id == 2){
				$setting['Acos.id !='] = 1;
		}else{
			$setting['Acos.id !='] = 1;
			$setting['Acos.acos_type_id'] = 2;
		}
		$acosList 	=	$this->ArosAcos->Acos->find('all', array(
			'conditions'	=>	$setting
		));

		// matix aros acos
		$matrixArosAcos	=	array();
		if(!empty($aros->aros_acos)) {
			foreach($aros->aros_acos as $key => $value) {
				$matrixArosAcos[$value->aco_id]	=	$value->toArray();
			}
		}

		$this->set(compact('arosAcos', 'acosList', 'aros', 'matrixArosAcos'));
	}

}
