<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine
use Cake\Core\App;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CmsMenusController extends AppController
{

    public $paginate = [
        'order' => [
            'CmsMenus.left' => 'asc'
        ],
        'sortableFields' =>  [
            'CmsMenus.id'
        ]
    ];

    public function initialize(): void
    {
        parent::initialize();

        // using metronic layout.
        $this->viewBuilder()->setLayout('metronic_main');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        // $this->Authentication->addUnauthenticatedActions(['login', 'add']);
        $this->set('activeParentMenuId', 5);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

        $this->request->allowMethod(['get']);

        $settings   =   array(
            // 'CmsMenus.id !=' =>  1 // 1 untuk top level menu tidak usah di tunjukin
        );

        if ($this->request->is('get')) {

            $finderSettings = array();

            if($this->request->getQuery('email') !== null && $this->request->getQuery('email') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.email LIKE'] =   "%" . $this->request->getQuery('email') . "%";
            }

            if($this->request->getQuery('aro_id') !== null && $this->request->getQuery('aro_id') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.aro_id'] =   $this->request->getQuery('aro_id');
            }
        }

        $this->paginate = [
                'order' => [
                'CmsMenus.lft' => 'asc'
            ],
            'sortableFields' =>  [
                'CmsMenus.id', 'Acos.alias', 'CmsMenus.action', 'CmsMenus.lft', 'ParentCmsMenus.name'
            ],
            'limit' =>  10
        ];

        $settings   =   array_merge($settings, $finderSettings);

        $cmsMenus = $this->paginate($this->CmsMenus->find('all', array(
            'conditions'    =>  $settings,
            'contain'       =>  ['Acos', 'ParentCmsMenus'],
        )));

        $acosList   =   $this->CmsMenus->Acos->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias',
            'keyPath' =>  'id'
        ]);

        $this->set(compact('cmsMenus', 'acosList'));

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->setLayout('ajax');
            $this->render('listItem');
        } else if($this->request->is('get') && $this->request->getQuery('excel') != null && $this->request->getQuery('excel') == 1) {

            $this->viewBuilder()->setLayout('ajax');
            $this->set('filename', $this->actionTitle . "_list.xlsx");
            $this->render('listItemExcel');

        }
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $user = $this->CmsMenus->get($id, [
            'contain' => ['Acos'],
        ]);

        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

        $this->set(compact('cmsMenu'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'create');

        $cmsMenu = $this->CmsMenus->newEmptyEntity();

        if ($this->request->is('post')) {
            $cmsMenu = $this->CmsMenus->patchEntity($cmsMenu, $this->request->getData(), [
            ]);
            if ($this->CmsMenus->save($cmsMenu)) {
                $this->Flash->metronicSuccess(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('Menu could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: CM1']]);
        }

        $parentList   =   $this->CmsMenus->ParentCmsMenus->find('treeList', [
            'conditions'    =>  [
            ],
            'valuePath'   =>  'name',
            'keyPath' =>  'id'
        ]);

        $acosList       =   $this->CmsMenus->Acos->find('treeList', [
            'conditions'    =>  [
                'Acos.id !='    =>  1 // top hanya untuk developer saja
            ],
            'valuePath' =>  'alias',
            'keyPath'   =>  'id'
        ]);
        $this->loadModel("Acos");
        $controllerList =   $this->Acos->getAvailableControllers();
        $this->listItemTitle    =   "Add new menu";
        $this->set('listItemTitle', $this->listItemTitle);

        $this->set(compact('cmsMenu', 'parentList', 'acosList','controllerList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cmsMenu = $this->CmsMenus->get($id, [
            'contain' => ['Acos'],
        ]);

        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $cmsMenu = $this->CmsMenus->patchEntity($cmsMenu, $this->request->getData());
            if ($this->CmsMenus->save($cmsMenu)) {
                $this->Flash->metronicSuccess(__('Menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->metronicError(__('Menu could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: CM2']]);
        }

        $parentList   =   $this->CmsMenus->ParentCmsMenus->find('treeList', [
            'conditions'    =>  [
            ],
            'valuePath'   =>  'name',
            'keyPath' =>  'id'
        ]);

        $acosList       =   $this->CmsMenus->Acos->find('treeList', [
            'conditions'    =>  [
                'Acos.id !='    =>  1 // top hanya untuk developer saja
            ],
            'valuePath' =>  'alias',
            'keyPath'   =>  'id'
        ]);

        $this->listItemTitle    =   "Edit : " . $cmsMenu->get('name');
        $this->set('listItemTitle', $this->listItemTitle);

        $this->set(compact('cmsMenu', 'parentList', 'acosList'));
    }

    function moveUp($id = NULL)
    {

        $cmsMenu = $this->CmsMenus->get($id, [
            'contain' => ['Acos'],
        ]);
        $this->Authorization->authorize($cmsMenu, 'update');

        $resultStatus = "0";
        if (empty($cmsMenu))
        {
            $message      = __("Item not found.");
            $resultStatus = "0";
        }
        else
        {
            //$this->{$this->ModelName}->reorder();
            $moveUp         =   $this->CmsMenus->moveUp($cmsMenu);
            $message        =   __("Item has successfully move up.");
            $resultStatus   =   "1";
        }

        echo json_encode(array(
               "data" => array(
                       "status"     => $resultStatus,
                       "message"    => $message
               )
        ));
        // $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;
    }

    function moveDown($id = NULL)
    {

        $cmsMenu = $this->CmsMenus->get($id, [
            'contain' => ['Acos'],
        ]);
        $this->Authorization->authorize($cmsMenu, 'update');

        $resultStatus = "0";
        if (empty($cmsMenu))
        {
            $message      = __("Item not found.");
            $resultStatus = "0";
        }
        else
        {
            //$this->{$this->ModelName}->reorder();
            $moveDown         =   $this->CmsMenus->moveDown($cmsMenu);
            $message        =   __("Item has successfully move up.");
            $resultStatus   =   "1";
        }

        echo json_encode(array(
               "data" => array(
                       "status"     => $resultStatus,
                       "message"    => $message
               )
        ));
        // $this->viewBuilder()->setLayout('ajax');
        $this->autoRender = false;
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cmsMenu = $this->CmsMenus->get($id);

        // $this->Authorization->authorize($cmsMenu, 'delete');

        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

        if ($this->CmsMenus->delete($cmsMenu)) {
            $this->Flash->metronicSuccess(__('Menu been deleted.'));
        } else {
            $this->Flash->metronicError(__('Menu could not be deleted. Please, try again.'), ['params'  =>  ['code'   =>  'Code: CM3']]);
        }

        return $this->redirect(['action' => 'index']);
    }

}
