<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Cache\Cache; // For cache system engine
use Cake\Core\App;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

     public function initialize(): void
     {
         parent::initialize();
         $this->loadComponent("General");

         // using metronic layout.
         $this->viewBuilder()->setLayout('metronic_main');
     }
     public function beforeFilter(\Cake\Event\EventInterface $event)
     {
         parent::beforeFilter($event);
         // Configure the login action to not require authentication, preventing
         // the infinite redirect loop issue
         $this->Authentication->addUnauthenticatedActions(['login', 'add']);
         $this->set('activeParentMenuId', 3);

         // contoh untuk menggunakan generalComponent ResizeImageContent
         // $this->viewBuilder()->setLayout('ajax');
         // $this->General->ResizeImageContent("/Library/WebServer/Documents/latihan/cake4/cms/webroot/img/scarjo.jpg", "http://www.acisapp.xyz/", "1", "User", "thumb", "image/jpeg", 150, 150, "cropResize", "namafile");
     }

    public function index()
    {
      	$this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
        $this->request->allowMethod(['get']);
          $settings = array();
        $products = $this->paginate($this->Products->find('all', array(
            'conditions'    =>  $settings,
             'contain'       =>  ['Images']
        )));;

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
        $product = $this->Products->get($id, [
            'contain' => ['Services', 'Jobs'],
        ]);

        $this->set(compact('product'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'create');
        $product = $this->Products->newEmptyEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->metronicSuccess(__('The product has been saved.'));
                if($product->imagefile->getError() == 0 && $product->imagefile->getSize() > 0) {
                  // doing the upload here.
                  $tmp_name  = $product->imagefile->getClientFilename();
                  $tmp       = $product->imagefile->getStream()->getMetadata('uri');
                  $mime_type = $product->imagefile->getClientMediaType();

                  $path_tmp = ROOT . DS . 'tmp' . DS . 'upload' . DS;

                  if (!is_dir($path_tmp)) {
                      mkdir($path_tmp, 0755);
                  }

                  $ext             = pathinfo($tmp_name, PATHINFO_EXTENSION);
                  $tmp_file_name   = md5(strval(time()));
                  $tmp_images1_img = $path_tmp . $tmp_file_name . "." . strtolower($ext);
                  $upload          = $product->imagefile->moveTo($tmp_images1_img);

                  $setting            =   $this->General->getSetting();

                  $resize                = $this->General->ResizeImageContent(
                      $tmp_images1_img,
                      $setting->cms_url,
                      $product->get('id'),
                      "Product",
                      "image",
                      $mime_type,
                      400,
                      400,
                      "cropResize"
                  );




                  @unlink($tmp_images1_img);

              }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The product could not be saved. Please, try again.'));
        }
        $services = $this->Products->Services->find('list', ['limit' => 200]);
        $this->set(compact('product', 'services'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');
        $product = $this->Products->get($id, [
            'contain' => ['Services'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->metronicSuccess(__('The product has been saved.'));
                if($product->imagefile->getError() == 0 && $product->imagefile->getSize() > 0) {
                  // doing the upload here.
                  $tmp_name  = $product->imagefile->getClientFilename();
                  $tmp       = $product->imagefile->getStream()->getMetadata('uri');
                  $mime_type = $product->imagefile->getClientMediaType();

                  $path_tmp = ROOT . DS . 'tmp' . DS . 'upload' . DS;

                  if (!is_dir($path_tmp)) {
                      mkdir($path_tmp, 0755);
                  }

                  $ext             = pathinfo($tmp_name, PATHINFO_EXTENSION);
                  $tmp_file_name   = md5(strval(time()));
                  $tmp_images1_img = $path_tmp . $tmp_file_name . "." . strtolower($ext);
                  $upload          = $product->imagefile->moveTo($tmp_images1_img);

                  $setting            =   $this->General->getSetting();

                  $resize                = $this->General->ResizeImageContent(
                      $tmp_images1_img,
                      $setting->cms_url,
                      $product->get('id'),
                      "Product",
                      "image",
                      $mime_type,
                      400,
                      400,
                      "cropResize"
                  );




                  @unlink($tmp_images1_img);

              }
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The product could not be saved. Please, try again.'));
        }
        $services = $this->Products->Services->find('list', ['limit' => 200]);
        $this->set(compact('product', 'services'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function managedProductService($id)
        {

            $this->loadModel("Users");
            $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

            $data   =   $this->Products->find('all', array(
                'conditions'    =>  [
                    'Products.id'   =>  $id
                ],
            ))
            ->contain(['Images','Services'=>['JobTypes']])
            ->first();

            if($data == false) {
                $this->Flash->metronicError(__('Product Vehicle Item cannot be found. Please try again'), ['params'  =>  ['code'   =>  'Code: C3']]);
                return $this->redirect(['action' => 'index']);
            }

            if ($this->request->is('post')) {
                // $this->viewBuilder()->setLayout('ajax');
                debug($this->request->getData());
            }

            // debug($data);
            $this->set(compact('data'));

            if ($this->request->is('ajax')) {
                $this->viewBuilder()->setLayout('ajax');
                $this->render('listItemManagedService');
            }

        }
    public function managedServiceAdd($id)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

      $data   =   $this->Products->find('all', array(
          'conditions'    =>  [
              'Products.id'   =>  $id
          ],
      ))
      ->contain(['Images','Services'=>['JobTypes']])
      ->first();

      if($data == false) {
          $this->Flash->metronicError(__('Discount Item cannot be found. Please try again'), ['params'  =>  ['code'   =>  'Code: C3']]);
          return $this->redirect(['action' => 'index']);
      }

      $condSearchVehicle  = [];

      if ($this->request->is('post')) {
          // $this->viewBuilder()->setLayout('ajax');
          $request  = $this->request->getData();
          if(isset($request['job_type_id']) && !empty($request['job_type_id'])) {
            $condSearchVehicle["Services.job_type_id"]  = $request['job_type_id'];
          }
      }

      // cari dulu ploduk2 yang sudah ada. tidak di masukkan ke pilihan.
      $existingServicesIds   = array();
      if(isset($data->services) && !empty($data->services)) {

        foreach($data->services as $key => $value) {
          $existingServicesIds[$value->id]   = $value->id;
        }
      }

      if(!empty($existingServicesIds)) {
        $filterSearch  = [
            'Services.status' =>  1,
          'Services.id not in'  =>  $existingServicesIds
        ];
      } else {
        $filterSearch  = [
          'Services.status' =>  1
        ];
      }

      $mergeCondVehicle   = array_merge($filterSearch, $condSearchVehicle);

      // debug($mergeCondProduct);
      $this->loadModel("Services");
      $services   = $this->Services->find('all', [
        'conditions'  =>  $mergeCondVehicle
      ])->contain(['JobTypes']);

        $jobTypeList = $this->Services->JobTypes->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
      ]);

      // debug($data);
      $this->set(compact('data', 'services', 'jobTypeList'));

      if ($this->request->is('ajax')) {
          $this->viewBuilder()->setLayout('ajax');
          $this->render('listItemManagedServiceAdd');
      }

  }

  public function deleteItem($id = null, $service_id = null)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

      Log::warning('Masuk deleteItem', ['scope' => ['test']]);

      $this->request->allowMethod(['post', 'delete']);
      Log::warning('Setelah allow method', ['scope' => ['test']]);
      $this->loadModel("ProductsServices");
      $entity   = $this->ProductsServices->find('all', [
        'conditions'  =>  [
          'ProductsServices.product_id' =>  $id,
          'ProductsServices.service_id' =>  $service_id
        ]
      ])->first();

      if ($this->ProductsServices->delete($entity)) {
          $this->Flash->metronicSuccess(__('The Service has been deleted.'));
      } else {
          $this->Flash->metronicError(__('The product could not be deleted. Please, try again.'), ['params'  =>  ['code'   =>  'Code: C3']]);
      }

      return $this->redirect(['action' => 'managedProductService', $id]);
  }

  public function managedServiceSubmitAdd($id = null)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

      $this->request->allowMethod(['post']);
      $product= $this->Products->get($id);

      if($product != false) {

        $request  = $this->request->getData();

        $saveData   = array();

        foreach($request['services'] as $key => $value) {

          $saveData[]   = [
            'product_id'  =>  intval($id),
            'service_id'  =>  $key
          ];
        }



        if(!empty($saveData)) {
          $this->loadModel("ProductsServices");
          $entities   = $this->ProductsServices->newEntities($saveData);

          if($this->ProductsServices->saveMany($entities)) {
            $this->Flash->metronicSuccess(__('The Services has been added to Product.'));
            return $this->redirect(['action'  =>  'managedProductService', $id]);
          } else {
            $this->Flash->metronicError(__('Cannot add product. Please, try again.'), ['params'  =>  ['code'   =>  'Code: KC23']]);
            return $this->redirect(['action'  =>  'managedServiceAdd', $id]);
          }

        }

      }

      $this->Flash->metronicError(__('Cannot add product. Please, try again.'), ['params'  =>  ['code'   =>  'Code: KC23']]);
      return $this->redirect(['action'  =>  'managedServiceAdd', $id]);


  }

  /**
   *
   * AJAX STUFF
   */

  public function deleteItems($id) {

      $this->viewBuilder()->setLayout('ajax');
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

      $data   = array();
      $status = '0';

      $this->request->allowMethod(['post']);

      $product   =   $this->Products->find('all', array(
          'conditions'    =>  [
              'Products.id'   =>  $id
          ],
      ))
      ->contain(['Images','Services'=>['JobTypes']])
      ->first();

      if($product != false) {
        if ($this->request->is('post')) {
          $data =   $this->request->getData();

          if(isset($data['services']) && count($data['services']) > 0) {

            $serviceIds   = array();
            $this->loadModel("ProductsServices");

            foreach($data['services'] as $key => $value) {
              if(intval($value['is_checked']) == 1) {

                $serviceIds[]   = $key;

              }
            }
              // debug($vehicleIds);
            if(!empty($serviceIds)) {
              $entities   = $this->ProductsServices->find('all', [
                'conditions'  =>  [
                  'ProductsServices.service_id in'  =>  $serviceIds,
                  'ProductsServices.product_id' =>  $product->id
                ]
              ]);

              $this->ProductsServices->deleteMany($entities);
              $status   = '1';
            }
          }
        }
      }

      $this->set('data', $data);
      $this->set('status', $status);
      $this->viewBuilder()->setOption('serialize', ['data', 'status']);

  }

}
