<?php

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine
use Authorization\Policy\MapResolver;
use App\Model\Entity\Article;

// Firebase test
use Kreait\Firebase\Factory;
use Firebase\Auth\Token\Exception\InvalidToken;

class DashboardsController extends AppController
{

	var $name = "Dashboards";
	// var $modelClass	=	"ArosAcos";

	public function initialize(): void
	{
		parent::initialize();

		$this->loadComponent('Paginator');
		$this->loadComponent('Flash');

	}

	public function beforeFilter(\Cake\Event\EventInterface $event)
	{
		parent::beforeFilter($event);

		$this->viewBuilder()->setLayout('metronic_main');
		$this->set('activeParentMenuId', 2);

		$this->Authentication->addUnauthenticatedActions(['firebasePhone']);

	}

	public function index()
	{

		// $this->Authorization->skipAuthorization();

		$this->loadModel("Users");
		$this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
		//
		// $this->loadModel("Products");
		//
		// $products = $this->Products->find('all', array(
		// 		// 'conditions'
		// 	)
		// )->contain(['Vehicles']);
		//
		// $this->set('products', $products);
	}

	public function firebase()
	{
		// <!-- The core Firebase JS SDK is always required and must be listed first -->
		// <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>

		// <!-- TODO: Add SDKs for Firebase products that you want to use
		//      https://firebase.google.com/docs/web/setup#available-libraries -->

		// <script>
		//   // Your web app's Firebase configuration
		//   var firebaseConfig = {
		//     apiKey: "AIzaSyDoRMRRyCcAvspYKDD5fM4iynggUEH4rTU",
		//     authDomain: "yuasa-test.firebaseapp.com",
		//     databaseURL: "https://yuasa-test.firebaseio.com",
		//     projectId: "yuasa-test",
		//     storageBucket: "yuasa-test.appspot.com",
		//     messagingSenderId: "388381034910",
		//     appId: "1:388381034910:web:58324ffc4291b2c2e92e8f"
		//   };
		//   // Initialize Firebase
		//   firebase.initializeApp(firebaseConfig);
		// </script>

		$factory = (new Factory)->withServiceAccount(ROOT . DS . "config" . DS . "yuasa-test-firebase-adminsdk-otqca-69bd57e4da.json");

		$auth = $factory->createAuth();

		$userProperties = [
		    'email' => 'kossaaudi84@gmail.com',
		    'emailVerified' => false,
		    'phoneNumber' => '+62816831402',
		    'password' => 'kossa001',
		    'displayName' => 'Kossa Audi Prasena Eminem',
		    'photoUrl' => 'http://www.example.com/12345678/photo.png',
		    'disabled' => false,
		];

		try {
		    $createdUser = $auth->createUser($userProperties);
		    debug($createdUser);
		} catch (\Kreait\Firebase\Exception\Auth\EmailExists $e) {
		    debug($e->getMessage());
		}

	}

	function firebaseLogin()
	{

		$factory = (new Factory)->withServiceAccount(ROOT . DS . "config" . DS . "yuasa-test-firebase-adminsdk-otqca-69bd57e4da.json");

		$auth = $factory->createAuth();

		try {
			$signInResult = $auth->signInWithEmailAndPassword("kossaaudi84@gmail.com", "kossa001");
			debug($signInResult);
		} catch(\Kreait\Firebase\Auth\SignIn\FailedToSignIn $e) {
			// invalid password INVALID_PASSWORD
			// email not found EMAIL_NOT_FOUND
			debug($e->getMessage());
		}

	}

	function firebasePhone()
	{

		$this->Authorization->skipAuthorization();



	}

}
