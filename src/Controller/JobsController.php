<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Cache\Cache; // For cache system engine
use Cake\Core\App;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $jobs
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class JobsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

     public function initialize(): void
     {
         parent::initialize();
         $this->loadComponent("General");

         // using metronic layout.
         $this->viewBuilder()->setLayout('metronic_form');
     }
     public function beforeFilter(\Cake\Event\EventInterface $event)
     {
         parent::beforeFilter($event);
         // Configure the login action to not require authentication, preventing
         // the infinite redirect loop issue
         $this->Authentication->addUnauthenticatedActions(['login', 'add']);
         $this->set('activeParentMenuId', 3);

         // contoh untuk menggunakan generalComponent ResizeImageContent
         // $this->viewBuilder()->setLayout('ajax');
         // $this->General->ResizeImageContent("/Library/WebServer/Documents/latihan/cake4/cms/webroot/img/scarjo.jpg", "http://www.acisapp.xyz/", "1", "User", "thumb", "image/jpeg", 150, 150, "cropResize", "namafile");
     }

    public function index()
    {
      	$this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
        $this->request->allowMethod(['get']);
          $settings = array();
          $finderSettings = array();

          if($this->request->getQuery('job_type_id') !== null && $this->request->getQuery('job_type_id') != "") { // we cannot use isset. so instead using !== null
              $finderSettings['Jobs.job_type_id LIKE'] =  $this->request->getQuery('job_type_id');
          }
          if($this->request->getQuery('job_status_id') !== null && $this->request->getQuery('job_status_id') != "") { // we cannot use isset. so instead using !== null
              $finderSettings['Jobs.job_status_id LIKE'] =  $this->request->getQuery('job_status_id');
          }
          $settings   =   array_merge($settings, $finderSettings);

        $jobs = $this->paginate($this->Jobs->find('all', array(
            'conditions'    =>  $settings,
             'contain'       =>  ['JobStatuses','JobTypes','Products','Services' , 'Customers', 'Users','Employes']
        )));
        $jobTypeList = $this->Jobs->JobTypes->find('list',[
          'keyField' => 'id',
          'valueField' => 'name'
        ]);
        $jobStatusList = $this->Jobs->JobStatuses->find('list',[
          'keyField' => 'id',
          'valueField' => 'name'
        ]);
        $this->set(compact('jobs','jobTypeList','jobStatusList'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
        $job = $this->Jobs->get($id, [
            'contain' => ['Services', 'Jobs'],
        ]);

        $this->set(compact('product'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'create');
        $job = $this->Jobs->newEmptyEntity();


        $jobTypeList = $this->Jobs->JobTypes->find('list',[
          'keyField' => 'id',
          'valueField' => 'name'
        ]);
        $customerList = $this->Jobs->Customers->find('list',[
          'keyField' => 'id',
          'valueField' => 'name'
        ]);
        $valueLongLat = "-6.1819 - 106.809082";
        $customer = $this->Jobs->Customers->newEmptyEntity();
        $job->customer = $customer;
        if ($this->request->is('post')) {
          $job = $this->Jobs->patchEntity($job, $this->request->getData(),[
          'associated'    =>  ['Customers']
        ]);

          if($job->job_type_id !== 1){
            $customer_array = array();
            $this->loadModel("Customers");
            $customerTemp = $this->Customers->find('all',[

              'conditions' => [
                'id' => $job->customer_id
              ]
            ])->toArray();
          foreach ($customerTemp as $key => $value) {
            // code...
            $customer_array[$value->id] = $value;
          }

          $job->customer->id = $job->customer_id;
          $job->customer->name = $customer_array[$job->customer_id]['name'];
          $job->customer->address = $customer_array[$job->customer_id]['address'];
          $job->customer->no_telp = $customer_array[$job->customer_id]['no_telp'];
          $job->customer->lat = $customer_array[$job->customer_id]['lat'];
          $job->customer->lng = $customer_array[$job->customer_id]['lng'];


          }


            $job->job_status_id = 1;
            $job->user_id = $this->loggedInUser->id;


            if ($this->Jobs->save($job)) {
                $this->Flash->metronicSuccess(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The product could not be saved. Please, try again.'));
        }
        // $services = $this->Jobs->Services->find('list', ['limit' => 200]);
        $this->set(compact('job','jobTypeList','customerList','valueLongLat'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
        $job = $this->Jobs->get($id, [
            'contain' => ['Users'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $job = $this->Jobs->patchEntity($job, $this->request->getData());
            if ($this->Jobs->save($job)) {
                $this->Flash->metronicSuccess(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The product could not be saved. Please, try again.'));
        }
        // $services = $this->Jobs->Services->find('list', ['limit' => 200]);
        $this->set(compact('job'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');
        $this->request->allowMethod(['post', 'delete']);
        $job = $this->Jobs->get($id);
        if ($this->Jobs->delete($job)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function getServiceByJobTypeId($jobTypeId = "") {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

      $data = array();

      if($jobTypeId == "") {
          $this->status         =   array(
              'code'  =>  "2",
              'message'   =>  'Not Success'
          );
      }

      $this->loadModel('Services');
      $data   =   $this->Services->find('list', array(
          'conditions'    =>  array(
              'Services.job_type_id' =>  $jobTypeId
          ),
          'order' =>  array(
              'Services.name asc'
          )
      ));

      $tempData   =   $data;
      $data   =   array();
      foreach($tempData as $key => $value) {
          $data[$key] =   $value;
      }



      // $data["1"]  =   "Others";


      $this->set('data', $data);
      $this->viewBuilder()->setOption('serialize', ['status', 'data']);

  }
  public function getProductByServiceId($serviceId = "") {

    $this->loadModel("Users");
    $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

    $data = array();
    $dataService = array();

    if($serviceId == "") {
        $this->status         =   array(
            'code'  =>  "2",
            'message'   =>  'Not Success'
        );
    }
    $service_query = array();
    if($serviceId !== ""){
      $service_query['Services.id'] = $serviceId;
    }
    $this->loadModel('Products');
    // $this->loadModel('ProductsServices');

    if(count($service_query) > 0){
      $dataService   =   $this->Products->find('list', array(
        'conditions'    =>  array(
            'Products.status' =>  1
        )
      ));
      $dataService->matching('Services', function($q) use ($service_query){
  	                return $q->where([
  	                    $service_query
  	                ]);
  	            })->toArray();
    }else{
      $dataService;
    }

    // $data = $this->Products->find('list',array(
    //   'conditions'    =>  array(
    //       'Products.id' =>  1
    //   )
    // ));




    // $data["1"]  =   "Others";


    $this->set('data', $dataService);
    $this->viewBuilder()->setOption('serialize', ['status', 'data']);

}
public function getJobStatusByJobId($jobId = "") {

  $this->loadModel("Users");
  $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

  $data = array();
  //$dataStatus= array();

  if($jobId == "") {
      $this->status         =   array(
          'code'  =>  "2",
          'message'   =>  'Job Id is Missing'
      );
  }
  $job_query = array();
  if($jobId !== ""){
    $job_query['id'] = $jobId;
  }
//$this->loadModel('Products');
  // $this->loadModel('ProductsServices');

  if(count($job_query) > 0){
    $dataJob  =   $this->Jobs->find('all', array(
      'conditions'    =>  array(
          'Jobs.status' =>  1,
          'Jobs.id' => $jobId
      )
    ));
    $status = "";
    foreach ($dataJob as $key => $value) {
      // code...
      $status = $value->job_status_id;
    }
  $this->loadModel('JobStatuses');
  $dataStatus = $this->JobStatuses->find('all', array(
    'conditions' => array(
      'id <=' => $status
    )
  ));
  $data = $dataStatus;
  }else{
    $data;
  }

  // $data = $this->Products->find('list',array(
  //   'conditions'    =>  array(
  //       'Products.id' =>  1
  //   )
  // ));




  // $data["1"]  =   "Others";


  $this->set('data', $data);
  $this->viewBuilder()->setOption('serialize', ['status', 'data']);

}
    public function managedJobEmployee($id)
        {

            $this->loadModel("Users");
            $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

            $data   =   $this->Jobs->find('all', array(
                'conditions'    =>  [
                    'Jobs.id'   =>  $id
                ],
            ))
            ->contain(['Employes','JobStatuses','JobTypes','Products','Services', 'Customers', 'Users'])
            ->first();

            if($data == false) {
                $this->Flash->metronicError(__('Product Vehicle Item cannot be found. Please try again'), ['params'  =>  ['code'   =>  'Code: C3']]);
                return $this->redirect(['action' => 'index']);
            }

            if ($this->request->is('post')) {
                // $this->viewBuilder()->setLayout('ajax');
                debug($this->request->getData());
            }

            // debug($data);
            $this->set(compact('data'));

            if ($this->request->is('ajax')) {
                $this->viewBuilder()->setLayout('ajax');
                $this->render('listItemManagedUsers');
            }

        }
    public function managedEmployeeAdd($id)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

      $data   =   $this->Jobs->find('all', array(
          'conditions'    =>  [
              'Jobs.id'   =>  $id
          ],
      ))
      ->contain(['Employes','Users'])
      ->first();

      if($data == false) {
          $this->Flash->metronicError(__('Discount Item cannot be found. Please try again'), ['params'  =>  ['code'   =>  'Code: C3']]);
          return $this->redirect(['action' => 'index']);
      }

      $condEmployee  = [];

      if($this->loggedInUser->id == $data->user->id){
        $condEmployee['Users.aro_id'] = 3;
      }

      // if ($this->request->is('post')) {
      //     // $this->viewBuilder()->setLayout('ajax');
      //     $request  = $this->request->getData();
      //     if(isset($request['job_type_id']) && !empty($request['job_type_id'])) {
      //       $condSearchVehicle["Services.job_type_id"]  = $request['job_type_id'];
      //     }
      // }

      // cari dulu ploduk2 yang sudah ada. tidak di masukkan ke pilihan.
      $existingEmployeIds   = array();
      if(isset($data->employes) && !empty($data->employes)) {

        foreach($data->employes as $key => $value) {
          $existingEmployeIds[$value->id]   = $value->id;
        }
      }

      if(!empty($existingEmployeIds)) {
        $filterSearch  = [
            'Employes.status' =>  1,

          'Employes.id not in'  =>  $existingEmployeIds
        ];
      } else {
        $filterSearch  = [
          'Employes.status' =>  1,
          'Users.aro_id in' => 3
        ];
      }

      $mergeCondVehicle   = array_merge($filterSearch, $condEmployee);

      // debug($mergeCondProduct);
      $this->loadModel("Employes");
      $employes   = $this->Employes->find('all', [
        'conditions'  =>  $mergeCondVehicle
      ])->contain(['Users'=>['Aros','Thumbs']])->toArray();

      // debug($data);
      $this->set(compact('data', 'employes'));

      if ($this->request->is('ajax')) {
          $this->viewBuilder()->setLayout('ajax');
          $this->render('listItemManagedUserAdd');
      }

  }

  public function deleteItem($id = null, $employe_id = null)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

      Log::warning('Masuk deleteItem', ['scope' => ['test']]);

      $this->request->allowMethod(['post', 'delete']);
      Log::warning('Setelah allow method', ['scope' => ['test']]);
      $this->loadModel("JobsUsers");
      $entity   = $this->JobsUsers->find('all', [
        'conditions'  =>  [
          'JobsUsers.job_id' =>  $id,
          'JobsUsers.employee_id' =>  $employe_id
        ]
      ])->first();

      if ($this->JobsUsers->delete($entity)) {
          $this->Flash->metronicSuccess(__('The Service has been deleted.'));
      } else {
          $this->Flash->metronicError(__('The product could not be deleted. Please, try again.'), ['params'  =>  ['code'   =>  'Code: C3']]);
      }

      return $this->redirect(['action' => 'managedJobUser', $id]);
  }

  public function managedEmployeeSubmitAdd($id = null)
  {

      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');

      $this->request->allowMethod(['post']);
      $job= $this->Jobs->get($id);

      if($job != false) {

        $request  = $this->request->getData();

        $saveData   = array();
        // debug($request);
        // exit;
        foreach($request['employes'] as $key => $value) {
          $saveData[]   = [
            'job_id'  =>  intval($id),
            'employee_id'  =>  $key
          ];
        }



        if(!empty($saveData)) {
          $this->loadModel("JobsUsers");
          $entities   = $this->JobsUsers->newEntities($saveData);

          if($this->JobsUsers->saveMany($entities)) {
            $this->Flash->metronicSuccess(__('The Services has been added to Product.'));
            return $this->redirect(['action'  =>  'managedJobEmployee', $id]);
          } else {
            $this->Flash->metronicError(__('Cannot add product. Please, try again.'), ['params'  =>  ['code'   =>  'Code: KC23']]);
            return $this->redirect(['action'  =>  'managedJobEmployee', $id]);
          }

        }

      }

      $this->Flash->metronicError(__('Cannot add product. Please, try again.'), ['params'  =>  ['code'   =>  'Code: KC23']]);
      return $this->redirect(['action'  =>  'managedServiceAdd', $id]);


  }

  /**
   *
   * AJAX STUFF
   */

  public function deleteItems($id) {

      $this->viewBuilder()->setLayout('ajax');
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

      $data   = array();
      $status = '0';

      $this->request->allowMethod(['post']);

      $job   =   $this->Jobs->find('all', array(
          'conditions'    =>  [
              'Jobs.id'   =>  $id
          ],
      ))
      ->contain(['Employes'])
      ->first();

      if($job != false) {
        if ($this->request->is('post')) {
          $data =   $this->request->getData();

          if(isset($data['employes']) && count($data['employes']) > 0) {

            $employeIds   = array();
            $this->loadModel("JobsUsers");

            foreach($data['employes'] as $key => $value) {
              if(intval($value['is_checked']) == 1) {

                $employeIds[]   = $key;

              }
            }
              // debug($vehicleIds);
            if(!empty($employeIds)) {
              $entities   = $this->JobsUsers->find('all', [
                'conditions'  =>  [
                  'JobsUsers.employee_id in'  =>  $employeIds,
                  'ProductsServices.job_id' =>  $job->id
                ]
              ]);

              $this->JobsUsers->deleteMany($entities);
              $status   = '1';
            }
          }
        }
      }

      $this->set('data', $data);
      $this->set('status', $status);
      $this->viewBuilder()->setOption('serialize', ['data', 'status']);

  }

}
