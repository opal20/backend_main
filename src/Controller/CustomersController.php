<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine
use Cake\Core\App;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{

    public $paginate = [
        'order' => [
            'Customers.id' => 'desc'
        ]
    ];

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("General");

        // using metronic layout.
        $this->viewBuilder()->setLayout('metronic_main');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
        $this->set('activeParentMenuId', 3);

        // contoh untuk menggunakan generalComponent ResizeImageContent
        // $this->viewBuilder()->setLayout('ajax');
        // $this->General->ResizeImageContent("/Library/WebServer/Documents/latihan/cake4/cms/webroot/img/scarjo.jpg", "http://www.acisapp.xyz/", "1", "User", "thumb", "image/jpeg", 150, 150, "cropResize", "namafile");
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        	$this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

        $this->request->allowMethod(['get']);
        $setting            =   $this->General->getSetting();
        $settings   =   array();

        if ($this->request->is('get')) {

            $finderSettings = array();

            if($this->request->getQuery('email') !== null && $this->request->getQuery('email') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.email LIKE'] =   "%" . $this->request->getQuery('email') . "%";
            }

            if($this->request->getQuery('aro_id') !== null && $this->request->getQuery('aro_id') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.aro_id'] =   $this->request->getQuery('aro_id');
            }
        }

        $this->paginate = [
            'order' => [
                'Users.id' => 'desc'
            ],
            'sortableFields' =>  [
                'Users.id', 'Aros.alias', 'Users.email', 'Users.created', 'Users.status'
            ],
            'limit' =>  20
        ];

        $settings   =   array_merge($settings, $finderSettings);

        $customers = $this->Customers->find('all', array(
            'conditions'    =>  $settings,
            'contain'       =>  ['Jobs'],
        ));


        // $companyList   =   $this->Users->Companies->find('list', [
        //     'keyField' => 'id',
        //     'valueField' => 'name'
        // ]);
        $this->set(compact('customers','setting'));

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->setLayout('ajax');
            $this->render('listItem');
        } else if($this->request->is('get') && $this->request->getQuery('excel') != null && $this->request->getQuery('excel') == 1) {

            $this->viewBuilder()->setLayout('ajax');
            $this->set('filename', $this->actionTitle . "_list.xlsx");
            $this->render('listItemExcel');

        }
    }

    /**
     * ListItem method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    // public function listItem()
    // {
    //     $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

    //     $this->viewBuilder()->setLayout('ajax');

    //     $users = $this->paginate($this->Users);

    //     $this->set(compact('users'));
    // }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $customer = $this->Customers->get($id, [
            'contain' => ['Jobs'],
        ]);
        $this->loadModel("Users");
        $this->Authorization->authorize($user, 'read');

        $this->set(compact('customer'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'create');

        $customer = $this->Customers->newEmptyEntity();


        if ($this->request->is('post')) {
            $customer = $this->Customers->patchEntity($user, $this->request->getData());
            // debug($user);
            // exit;
            if ($this->Customers->save($customer)) {


                $this->Flash->metronicSuccess(__('The Customers has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The Customers could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U1']]);
        }


        $this->listItemTitle    =   "Add new user";
        $this->set('listItemTitle', $this->listItemTitle);

        $this->set(compact('customer'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customer = $this->Customers->get($id);
        //debug($customer);

        $this->loadModel("Users");
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'update');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData());
            // $this->viewBuilder()->setLayout('ajax');

            // debug($user->imagefile);

            if ($this->Customers->save($customer)) {



                // Process uploaded file here.


                $this->Flash->metronicSuccess(__('The Customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->metronicError(__('The Customer could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U2']]);
        }



        $this->listItemTitle    =   "Edit : Customers";
        $this->set('listItemTitle', $this->listItemTitle);
        $this->set(compact('customer'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Customers->get($id);
        $this->loadModel("Users");
        $this->Authorization->authorize($user, 'delete');
        if ($this->Customers->delete($customer)) {
            $this->Flash->metronicSuccess(__('The Customers has been deleted.'));
        } else {
            $this->Flash->metronicError(__('The Customers could not be deleted. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U3']]);
        }

        return $this->redirect(['action' => 'index']);
    }



}
