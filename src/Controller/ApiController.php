<?php

/**
 *
 * error code terakhir : 8
 *
 *
 * Update :
 * 1. atas request mas Fauzan tanggal 10 February, battery industri di hide dulu
 * impact di :
 * 				-	getProductsFullInfoLlist
 * 	Update terbaru, sudah menggunakan only_apps khusus untuk battery industri.
 *
 *
 */

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine
use Authorization\Policy\MapResolver;
use Cake\Utility\Security;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Log\Log;

use Cake\Mailer\Mailer; //untuk test email

class ApiController extends AppController
{

	var $name = "Api";
	var $allowAccess 	=	false;
	var $status 		=	array(
		'code'	=>	"1",
		'message'	=>	'success'
	);
	var $setting 		=	array();

	var $noImageUrl 	=	"https://www.santiyoga.com/assets/no_image_available.jpg";

	// var $midtransClientKey	=	"SB-Mid-client-NkeLctNAkp5rYXVX"; // development
	var $midtransClientKey	=	"Mid-client-ea6r_Ns648S6c5Dz";	// production
	// var $snapJsUrl			=	"https://app.sandbox.midtrans.com/snap/snap.js"; // development
	var $snapJsUrl			=	"https://app.midtrans.com/snap/snap.js"; // production

	public function initialize(): void
	{
		parent::initialize();

		$this->loadComponent('RequestHandler');
		$this->loadComponent('General');
		$this->loadComponent('Action');

		Log::setConfig('apilog', [
		    'className' => 'File',
		    'path' => LOGS,
		    'levels' => [],
		    'scopes' => ['apiaccess', 'payments'],
		    'file' => 'apilog.log',
		]);

	}

	public function beforeFilter(\Cake\Event\EventInterface $event)
	{
		$this->Authentication->addUnauthenticatedActions([
		'loginEmployee','getJobTypeList','getJobStatusList','getJobs','getJobByJobId','getJobDetailByEmployeeId'
		]);

		// Get the header as a string
		$authorization 	= 	$this->request->getHeaderLine('Authorization'); // wt1U5MACWJFTXGenFoZoiLwQGrLgdwooKDicKDJWooLiverpoolADFbHA

		$this->setting 		=	$this->General->getSetting();

		if($authorization == $this->setting->authorization_id) {
			$this->allowAccess 	=	true;
		} else {
			$this->status 	=	array(
				'code'	=>	"2",
				'message'	=>	'You are not allowed to access. error code : 2'
			);
		}

	}



	public function index()
	{
		$this->Authorization->skipAuthorization();

		$companies 	=	array();

		if($this->allowAccess) {
			$this->loadModel("Companies");
			$companies = $this->Companies->find('all')->all();

	        $this->set('companies', $companies);
		}

        $this->set('status', $this->status);
        $this->viewBuilder()->setOption('serialize', ['companies', 'status']);
	}


	public function loginEmployee()
	{
		$this->Authorization->skipAuthorization();

		Log::warning('Login test', ['scope' => ['apiaccess']]);

		$request 	= 		array();
		$data 	=	 	array();

		if($this->allowAccess) {
			Log::warning('masuk access', ['scope' => ['apiaccess']]);

			// regardless of POST or GET, redirect if user is logged in

				$this->loadModel('Users');

							$request 	=	$this->request->getData();

							Log::warning("Request : " . print_r($request, true), ['scope' => ['apiaccess']]);

							if(empty($request['email'])) {
								$this->status 	=	 array(
						'code'	=>	"LS01",
						'message'	=>	"Email is required"
					);
							} else if(empty($request['password'])) {
								$this->status 	=	 array(
						'code'	=>	"LS02",
						'message'	=>	"Password is required"
					);

							} else {
								// lakukan sukses

								$password_user = "";
								if(isset($request['password'])) {
								$password_user = $request['password'];
								}
								$email_user = '';
								$condTemp = array();
								if(isset($request['email'])) {
								$email_user = $request['email'];
								$condTemp['Users.email'] = $email_user;
								}
								$tempUsers  = $this->Users->find('all',[
									'conditions' => $condTemp
								])->first();
								if($tempUsers->aro_id == 3){
								if($this->General->my_hash($password_user, $tempUsers->password)){
									Log::warning("Request temp password : " . print_r($tempUsers->password, true), ['scope' => ['apiaccess']]);

									$defaultConditions 	=	array(
										"Users.status"	=>	1,
										'Users.aro_id'	=>	3,

									);
									Log::warning("Request users : " . print_r($defaultConditions, true), ['scope' => ['apiaccess']]);



									if(!empty($request['email'])) {
										$defaultConditions 	=	array_merge($defaultConditions, [
											"Users.email"	=>	$request['email']
										]);
									}

									$user	=	$this->Users->find('all', [
										'conditions'	=>	$defaultConditions
									])
									->contain(['Employes', 'Thumbs','Aros']);


										Log::warning('masuk access', ['scope' => ['apiaccess']]);
											foreach ($user as $key => $value) {
												$testId = $value->id;
												Log::warning("Request users : " . print_r($testId, true), ['scope' => ['apiaccess']]);
												$imageUrl 	=	"";
												if(!empty($value->thumb)) {
													$imageUrl 	=	$value->thumb->host . $value->thumb->url;
												}

											// $data[] 	=	[
											// 	'id'	=>	"" . $value->id . "",
											// 	'aro_id'	=>	"" . $value->aro_id . "",
											// 	'email'	=>	"" . $value->email . "",
											// 	'password'	=>	"" . $value->password . "",
											// 	'full_name'	=>	"" . $value->employe->full_name . "",
											// 	'address'	=>	"" . $value->employe->address . "",
											//
											// 	'imageUrl'	=>	"" . $imageUrl . "",
											// ];
											$data['id'] = $value->employe->id;
											$data['aro_id'] = $value->aro_id;
											$data['email'] = $value->email;
											$data['password'] = $value->password;
											$data['full_name'] = $value->employe->full_name;
											$data['address'] = $value->employe->address;
											$data['imageUrl'] = $imageUrl;
												// code...
											}
											Log::warning("Request users : " . print_r($data, true), ['scope' => ['apiaccess']]);





								}else{
									$this->status 	=	 array(
							'code'	=>	"LS03",
							'message'	=>	"Email or password wrong, please check your login data"
						);
								}
							}else{
								$this->status 	=	 array(
						'code'	=>	"LS04",
						'message'	=>	"Your Account Cannot be Accessible into the System"
					);
							}


							}
							Log::warning("Status : " . print_r($this->status, true), ['scope' => ['apiaccess']]);



		}

		$this->set('status', $this->status);
		$this->set('request', $request);
		$this->set('data', $data);
		$this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);
	}
	public function getJobTypeList() {
		$this->Authorization->skipAuthorization();

		$data 		=	array();
		$request 	=	array();

		if($this->allowAccess) {

			if ($this->request->is('post')) {

				$request 	=	$this->request->getData();

				$this->loadModel('JobTypes');

				$data   =   $this->JobTypes->find('list', array(
								'conditions'    =>  array(

								),
								'order' =>  array(
										'JobTypes.id asc'
								)
						));

						$tempData   =   $data;
						$data   =   array();
						foreach($tempData as $key => $value) {
								$data[$key] =   $value;
						}

			} else {
				$this->status 	=	 array(
					'code'	=>	"IB1",
					'message'	=>	"Only accept POST request"
				);
			}
		}

		$this->set('status', $this->status);
		$this->set('request', $request);
		$this->set('data', $data);
		$this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);

	}
	public function getJobDetailByEmployeeId() {

	  // $this->loadModel("Users");
	  // $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');
		$this->Authorization->skipAuthorization();

	  $data = array();
		if($this->allowAccess) {

			if ($this->request->is('post')) {
				  //$dataStatus= array();
					$this->loadModel('Jobs');
					$this->loadModel('Employes');
					$request 	=	$this->request->getData();

				  if(empty($request['employee_id'])) {
				      $this->status         =   array(
				          'code'  =>  "2",
				          'message'   =>  'Employe ID is Missing'
				      );
				  }
				  $job_query_cond = array();
				  $employe_query_cond = array();
				  if(!empty($request['employee_id'])){
						//$jobId = $request['job_id'];
						$employe_id = $request['employee_id'];
				    //$job_query_cond['Jobs.id'] = $jobId;
				    $job_query_cond['Jobs.status'] = 1;
				    $employe_query_cond['Employes.id'] = $employe_id;
				  }
				//$this->loadModel('Products');
				  // $this->loadModel('ProductsServices');


				    $dataJob  =   $this->Jobs->find('all', array(
				      'conditions'    => $job_query_cond
				    ))->contain([
							'JobStatuses',
							'JobTypes',
							'Services',
							'Products',
							'Customers',
							'Employes' => [
								'Users' => ['Thumbs']
							]]);
						$dataJob->matching('Employes', function($q) use ($employe_query_cond){
									return $q->where([
											$employe_query_cond
									]);
							});
				    $status_id = "";
				    $status_name = "";
				    foreach ($dataJob as $key => $value) {
				      // code...
				      $status_id = $value->job_status_id;
				      $status_name = $value->job_status->name;
							$this->loadModel('JobStatuses');
						 $dataStatus = $this->JobStatuses->find('all', array(
							 'conditions' => array(
								 'id <=' => $status_id
							 )
						 ));
						 $employes = array();
						foreach ($value->employes as $key => $employe) {
											 // code...
											 $imageUrl 	=	"";
											 if(!empty($employe->user->thumb)) {
												 $imageUrl 	=	$employe->user->thumb->host . $employe->user->thumb->url;
											 }
											 $employes[$key] = [
												 'id' =>  $employe->id,
												 'name' => "" . $employe->full_name . "",
												 'employee_address' => "" . $employe->address . "",
												 'image_url' => "" . $imageUrl . "",
											 ];
										 }
						 $tracking_status = array();
						 foreach ($dataStatus as $key => $status) {
							 // code...
							 $tracking_status[] = [
								 "id" => $status->id,
								 "name" => $status->name
							 ];
						 }
						 $data[] = [
							 "job_id" => $value->id,
							 "status_id" => $status_id,
							 "status_name" =>  "" .$status_name . "",
							 'service_name'	=>	"" . $value->service->name . "",
							 'product_name'	=>	"" . $value->product->name . "",
							 'job_type_name'	=>	"" . $value->job_type->name . "",
							 'customer_name'	=>	"" . $value->customer->name . "",
							 'customer_address'	=>	"" . $value->customer->address . "",
							 'customer_notelp'	=>	"" . $value->customer->no_telp . "",
							 'customer_lat'	=>	"" . $value->customer->lat . "",
							 'customer_lng'	=>	"" . $value->customer->lng . "",
							 "tracking_status" => $tracking_status,
							 "employe_sign" => $employes
						 ];
				    }


				}
				else {
					$this->status 	=	 array(
						'code'	=>	"JS1",
						'message'	=>	"Only accept POST request"
					);
			}
		}
		$this->set('status', $this->status);
		$this->set('request', $request);
	  $this->set('data', $data);
	  $this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);

	}
	public function getJobStatusList() {
		$this->Authorization->skipAuthorization();

		$data 		=	array();
		$request 	=	array();

		if($this->allowAccess) {

			if ($this->request->is('post')) {

				$request 	=	$this->request->getData();

				$this->loadModel('JobStatuses');

				$data   =   $this->JobStatuses->find('list', array(
								'conditions'    =>  array(

								),
								'order' =>  array(
										'JobStatuses.id asc'
								)
						));

						$tempData   =   $data;
						$data   =   array();
						foreach($tempData as $key => $value) {
								$data[$key] =   $value;
						}

			} else {
				$this->status 	=	 array(
					'code'	=>	"IB1",
					'message'	=>	"Only accept POST request"
				);
			}
		}

		$this->set('status', $this->status);
		$this->set('request', $request);
		$this->set('data', $data);
		$this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);

	}
	public function getJobByJobId() {
		$this->Authorization->skipAuthorization();

		$data 		=	array();
		$request 	=	array();

		if($this->allowAccess) {

			if ($this->request->is('post')) {

				$request 	=	$this->request->getData();

				$conditions 	=	array(
					'Jobs.status'	=>	1
				);

				$this->loadModel('Jobs');

				if(isset($request['job_id']) && !empty($request['job_id'])) {
					$conditions['Jobs.id']	=	$request['job_id'];
				}

				$job	=	$this->Jobs->find('all', [
					'conditions'	=>	$conditions
				])
				->contain(['JobStatuses','JobTypes','Services', 'Customers', 'Employes']);
				$employes = array();
				foreach ($job as $key => $value) {
					// Log::warning("Request Jobs : " . print_r($value, true), ['scope' => ['apiaccess']]);
					foreach ($value->employes as $key => $employe) {
						// code...
						$employes[$key] = [
							'name' => "" . $employe->full_name . "",
							'employee_address' => "" . $employe->address . ""
						];
					}
					// code...
					$data[] 	=	[
						'id'	=>	"" . $value->id . "",
						'status_id'	=>	"" . $value->job_status_id . "",
						'status_name'	=>	"" . $value->job_status->name . "",
						'service_name'	=>	"" . $value->service->name . "",
						'job_type_name'	=>	"" . $value->job_type->name . "",
						'customer_name'	=>	"" . $value->customer->name . "",
						'customer_address'	=>	"" . $value->customer->address . "",
						'customer_notelp'	=>	"" . $value->customer->no_telp . "",
						'customer_lat'	=>	"" . $value->customer->lat . "",
						'customer_lng'	=>	"" . $value->customer->lng . "",
						'employee' => $employes

					];

				}

					Log::warning("Request data : " . print_r($data, true), ['scope' => ['apiaccess']]);
					Log::warning("Request Employes : " . print_r($employes, true), ['scope' => ['apiaccess']]);
			} else {
				$this->status 	=	 array(
					'code'	=>	"IB1",
					'message'	=>	"Only accept POST request"
				);
			}
		}

		$this->set('status', $this->status);
		$this->set('request', $request);
		$this->set('data', $data);
		$this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);

	}
	public function getStores() {
		$this->Authorization->skipAuthorization();

		$data 		=	array();
		$request 	=	array();


		if($this->allowAccess) {

			if ($this->request->is('post')) {

				$request 	=	$this->request->getData();

				$conditions 	=	array(
					'Jobs.status'	=>	1
				);

				if(isset($request['job_type_id']) && !empty($request['job_type_id'])) {
					$conditions['Jobs.job_type_id']	=	$request['job_type_id'];
				}
				if(isset($request['job_status_id']) && !empty($request['job_status_id'])) {
					$conditions['Jobs.job_status_id']	=	$request['job_status_id'];
				}



				$this->loadModel('Jobs');

				// image google map
				/*
					https://maps.googleapis.com/maps/api/staticmap?center=-6.15976524,106.82047272&zoom=15&size=800x400&markers=color:blue%7Clabel:S%7C-6.15976524,106.82047272&key=AIzaSyArvm8fpamLZF86lha1xsxGrZ9RZRtcUec
				*/

				if($search_query != "") {

					$this->loadModel("StoreIndexings");

					$allConditions 	=	array_merge($conditions, array(
						"match (store_name, store_address, store_city_name, store_province_name) against (:searchQuery IN NATURAL LANGUAGE MODE)"
					));

					$dataSearch 	=	$this->StoreIndexings->find('all')
						->select([
							"score" => "match (store_name, store_address, store_city_name, store_province_name) against (:searchQuery IN NATURAL LANGUAGE MODE)",
						])
						->contain([
							'Stores'	=>	[
								'StoreCities' => [
									'StoreProvinces'
								],
								'Thumb'
							]
						])
						->where($allConditions)
						->order(['score desc'])
						->bind(':searchQuery', $search_query)
						->enableAutoFields(true)
						->toArray();

					$data 	=	array();
					foreach($dataSearch as $key => $value) {
						$data[] 	=	$value->store;
					}

				} else {
					$data   =   $this->Stores->find('all', array(
			            'conditions'    =>  $conditions,
			            'order' =>  array(
			                'Stores.name asc'
			            ),
			            'limit'	=> 50,
			            'contain'	=>	array(
			            	'StoreCities' => array(
			            		'StoreProvinces'
			            	),
			            	'Thumb'
			            )
			        ));
				}



		        $tempData   =   $data;
		        $data   =   array();
		        foreach($tempData as $key => $value) {

		        	$image 	=	$this->noImageUrl;
		        	if(!empty($value->thumb)) {
		        		$image 	=	$value->thumb->host . $value->thumb->url;
		        	}


		        	$dataTemp 	=	array(
		        		'name'		=>	$value->name,
		        		'address'	=>	$value->address,
		        		'phone_1'	=>	$value->phone_1,
		        		'phone_2'	=>	$value->phone_2,
		        		'email'		=>	$value->email,
		        		'lat'		=>	$value->lat,
		        		'lng'		=>	$value->lng,
		        		'google_map'	=>	"https://maps.googleapis.com/maps/api/staticmap?center=".$value->lat.",".$value->lng."2&zoom=15&size=800x400&markers=color:blue%7Clabel:S%7C".$value->lat.",".$value->lng."&key=" . $this->setting['map_static_api_key'],
		        		'image'		=>	$image,
		        		'jam_operasional'	=>	'Senin s/d minggu 08.00 - 17.00'
		        	);

		            $data[$key] =   $dataTemp;
		        }

			} else {
				$this->status 	=	 array(
					'code'	=>	"SL1",
					'message'	=>	"Only accept POST request"
				);
			}
		}

		$this->set('status', $this->status);
		$this->set('request', $request);
		$this->set('data', $data);
		$this->viewBuilder()->setOption('serialize', ['status', 'request', 'data']);
	}



}
