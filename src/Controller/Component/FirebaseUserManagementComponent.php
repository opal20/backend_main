<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache; // For cache system engine

// Firebase test
use Kreait\Firebase\Factory;
use Firebase\Auth\Token\Exception\InvalidToken;

class FirebaseUserManagementComponent extends Component
{
	
	//// FIREBASE STUFF. 

	var $factory	=	false;
	var $auth 		=	false;
	var $firebaseFile 	=	 "yuasa-test-firebase-adminsdk-otqca-69bd57e4da.json"; // put it inside config's folder

	public function initialize(array $config): void
    {
        $this->factory 	= 	(new Factory)->withServiceAccount(ROOT . DS . "config" . DS . $this->firebaseFile);
        $this->auth 	=	$this->factory->createAuth();
    }

    public function getUserByUid($uid = null): Array 
    {

		$status 	=	array(
			'code'	=>	"1",
			'message'	=>	'success'
		);
		$data 	=	array();

		try {

			$data 	=	$this->auth->getUser($uid);

		}	catch 	(\Kreait\Firebase\Exception\Auth\UserNotFound $e) {

			$status 	=	 array(
				'code'	=>	'2',
				'message'	=>	$e->getMessage()
			);

		}

		$returnArray 	=	array(
			'status'	=>	$status, 
			'data'		=>	$data
		);

    	return $returnArray;
    }

    public function deleteUser($uid = null): Array 
    {

		$status 	=	array(
			'code'	=>	"1",
			'message'	=>	'success'
		);
		$data 	=	array();

		try {

			$data 	=	$this->auth->deleteUser($uid);

		}	catch 	(\Kreait\Firebase\Exception\Auth\UserNotFound $e) {

			$status 	=	 array(
				'code'	=>	'2',
				'message'	=>	$e->getMessage()
			);

		}

		$returnArray 	=	array(
			'status'	=>	$status, 
			'data'		=>	$data
		);

    	return $returnArray;
    }

}