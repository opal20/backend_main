<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Authentication\PasswordHasher\DefaultPasswordHasher; // Add this line

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache; // For cache system engine
use Verot\Upload\Upload;

class GeneralComponent extends Component
{
	/** Untuk resize component */
    function ResizeImageContent($source,$host,$model_id,$model_name,$type,$mime_type,$width,$height,$resizeType = 'cropResize', $filename = "")
	{

		$setting 		=	$this->getSetting();
		$ext			=	pathinfo($source,PATHINFO_EXTENSION);
		$Content		=	TableRegistry::get('Contents');

		$query			=	$Content->find("all",array(
								"conditions"	=>	array(
									"Contents.model_id"			=>	$model_id,
									"Contents.model"			=>	$model_name,
									"LOWER(Contents.type)"		=>	strtolower($type),
								)
							));

		$data 			=	$query->first();

		$contentData 	=	false;

		if(!empty($data)) {
			$contentData 	=	$data;
		} else {
			$contentData 	=	$Content->newEmptyEntity();
		}

		$contentData->model 	=	$model_name;
		$contentData->model_id 	=	$model_id;
		$contentData->host 		=	$host;
		$contentData->mime_type =	$mime_type;
		$contentData->cloud 	=	0;

		$path_content	=	$setting->path_content;
		if(!is_dir($path_content))mkdir($path_content, 0755);

		$path_model		=	$path_content. $model_name . DS;
		if(!is_dir($path_model)) mkdir($path_model, 0755);

		$path_model_id	=	$path_model . $model_id . DS;
		if(!is_dir($path_model_id))
			mkdir($path_model_id, 0755);

		// debug($path_model_id);

		if(!empty($filename)) {
			$path_content							=	$path_model_id.$filename.".".$ext;
		} else {
			$path_content							=	$path_model_id.$model_id.'_'.$type.".".$ext;
		}

		@unlink($path_content); // hapus dulu file apabila exist di folder.

		$img 									=	new Upload($source);

		if(!empty($filename)) {
			$img->file_new_name_body   				=	$filename;
		} else {
			$img->file_new_name_body   				=	$model_id.'_'.$type;
		}


		if($resizeType == 'cropResize') {

			$img->image_resize          		=	true;
			$img->image_ratio_crop      		=	true;
			$img->image_y               		=	$height;
			$img->image_x               		=	$width;

		} else if($resizeType == 'resizeMaxWidth') {

			$img->image_resize          		=	true;
			$img->image_ratio_y        			= 	true;
			$img->image_x               		=	$width;
		}
		else if($resizeType == 'cropRatio')
		{
			$img->image_resize          		=	true;
			$img->image_ratio		      		=	true;
			$img->image_y               		=	$height;
			$img->image_x               		=	$width;
		}
		else if($resizeType == 'cropFill')
		{
			$img->image_resize          		=	true;
			$img->image_ratio_fill	      		=	true;
			$img->image_y               		=	$height;
			$img->image_x               		=	$width;
		}

		$img->process($path_model_id);

		$Contents["Content"]["type"]			=	$type;
		if(!empty($filename)) {
			$Contents["Content"]["url"]				=	"contents/{$model_name}/{$model_id}/{$filename}.{$ext}";
		} else {
			$Contents["Content"]["url"]				=	"contents/{$model_name}/{$model_id}/{$model_id}_{$type}.{$ext}";
		}

		$contentData->type 						=	$type;
		if(!empty($filename)) {
			$contentData->url 					=	"contents/{$model_name}/{$model_id}/{$filename}.{$ext}";
		} else {
			$contentData->url 					=	"contents/{$model_name}/{$model_id}/{$model_id}_{$type}.{$ext}";
		}

		$contentData->path			=	$path_content;

		if ($img->processed)
		{

			$size 					=	getimagesize($path_content);
			$contentData->width 	=	$size[0];
			$contentData->height 	=	$size[1];
			$Content->save($contentData);

			return true;
		}

		return false;
	}

	function getSetting() {

		$setting 					=	false;

		$setting 					= 	Cache::read('setting', 'short');
		if($setting === null) {
			// get restricted controller.
	        $settingsTable 				= 	TableRegistry::get('Settings');
	        $query						=	$settingsTable->find('all');
	        $setting 					=	$query->first();

	        Cache::write('setting', $setting, "short");
		}

        return $setting;
	}

	function my_encrypt($string, $key="dkjfdKossAudiPrasena") {
		$result = '';
		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}

		return base64_encode($result);
	}
  function my_hash($password, $hashedPassword){
    if(strlen($password) > 0) {
        return (new DefaultPasswordHasher())->check($password, $hashedPassword);
    }
  }

	function my_decrypt($string, $key="dkjfdKossAudiPrasena") {
		$result = '';
		$string = base64_decode($string);

		for($i=0; $i<strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}

}
