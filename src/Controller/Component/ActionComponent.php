<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache; // For cache system engine
use Verot\Upload\Upload;

class ActionComponent extends Component 
{
	 public $components = ['General'];
	/**
		*Info of email setting
	*/
	var $actionInfo;
	
	/**
		HTML result from generate text
	*/
	var $text;
	var $text2;
	
	var $logsID;

	function emailSave($emailID,$to,$searchInfo=array(),$subjectInfo=array(),$model=null,$model_id=null)
	{
		$menuInstance 			= TableRegistry::get('EmailSettings');
		$mail					= $menuInstance->find('all',array(
			'conditions' => array('name' => $emailID)
		))->first();

		$settings 				=	$this->General->getSetting();

		// debug($mail);
		if($mail != false) {
			$html					=	$this->generateTextInfo($mail->email_setting,$searchInfo);
			$subject				=	(count($subjectInfo)>0) ? $this->generateTextInfo($mail->subject,$subjectInfo) : $mail->subject;
			
			$EmailLog					=	TableRegistry::get('EmailLogs');
			$emailLogData 				=	$EmailLog->newEmptyEntity();

			$emailLogData->to_email 			=	$to;
			$emailLogData->from_email 		=	$mail->from_email;
			$emailLogData->fromtext 	=	$mail->fromtext;
			$emailLogData->subject 		=	$subject;
			$emailLogData->text_body 		=	$html;
			$emailLogData->status		=	0;
			$emailLogData->model 		=	$model;
			$emailLogData->model_id 				=	$model_id;
			$emailLogData->email_setting_id 		=	$mail->id;
			
			// debug($emailLogData);

			$EmailLog->save($emailLogData);

			return true;
		} else {
			return false;
		}
		
		return true;
	}

	function pushNotifSave($push_notification_id = null, $push_token = null, $customer_id = null, $searchInfo=array(),$subjectInfo=array(), $model=null,$model_id=null )
	{
		$menuInstance 			= TableRegistry::get('PushNotificationSettings');
		$pushNotif				= $menuInstance->find('all',array(
			'conditions' => array('name' => $push_notification_id)
		))->first();

		$settings 				=	$this->General->getSetting();

		// debug($mail);
		if($pushNotif != false) {
			$html					=	$this->generateTextInfo($pushNotif->body,$searchInfo);
			$subject				=	(count($subjectInfo)>0) ? $this->generateTextInfo($pushNotif->subject,$subjectInfo) : $pushNotif->subject;
			
			$PushNotificationLog					=	TableRegistry::get('PushNotificationLogs');
			$pushNotificationLogData 				=	$PushNotificationLog->newEmptyEntity();

			$pushNotificationLogData->push_token 						=	$push_token;
			$pushNotificationLogData->customer_id						=	$customer_id;
			$pushNotificationLogData->subject 							=	$subject;
			$pushNotificationLogData->text_body 						=	$html;
			$pushNotificationLogData->status							=	0;
			$pushNotificationLogData->model 							=	$model;
			$pushNotificationLogData->model_id 						=	$model_id;
			$pushNotificationLogData->push_notification_setting_id 	=	$pushNotif->id;
			
			// debug($emailLogData);

			$PushNotificationLog->save($pushNotificationLogData);

			return true;
		} else {
			return false;
		}
		
		return true;
	}

	function generateTextInfo($textInfo, $searchInfo = array())
	{

		if(!empty($searchInfo)) {

			foreach($searchInfo as $key => $value) {
				$textInfo = str_replace("[".$key."]", $value, $textInfo);
			}

			return $textInfo;

		} else {

			return $textInfo;

		}
	}
	
	
}
?>