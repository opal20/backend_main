<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Cache\Cache; // For cache system engine
use Cake\Core\App;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public $paginate = [
        'order' => [
            'Users.id' => 'desc'
        ],
        'sortableFields' =>  [
            'Users.id', 'Aros.alias', 'Users.email', 'Users.created', 'Users.status'
        ]
    ];

    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("General");

        // using metronic layout.
        $this->viewBuilder()->setLayout('metronic_main');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
        $this->set('activeParentMenuId', 3);

        // contoh untuk menggunakan generalComponent ResizeImageContent
        // $this->viewBuilder()->setLayout('ajax');
        // $this->General->ResizeImageContent("/Library/WebServer/Documents/latihan/cake4/cms/webroot/img/scarjo.jpg", "http://www.acisapp.xyz/", "1", "User", "thumb", "image/jpeg", 150, 150, "cropResize", "namafile");
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

        $this->request->allowMethod(['get']);

        $settings   =   array();

        if ($this->request->is('get')) {

            $finderSettings = array();

            if($this->request->getQuery('email') !== null && $this->request->getQuery('email') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.email LIKE'] =   "%" . $this->request->getQuery('email') . "%";
            }

            if($this->request->getQuery('aro_id') !== null && $this->request->getQuery('aro_id') != "") { // we cannot use isset. so instead using !== null
                $finderSettings['Users.aro_id'] =   $this->request->getQuery('aro_id');
            }
        }

        $this->paginate = [
            'order' => [
                'Users.id' => 'desc'
            ],
            'sortableFields' =>  [
                'Users.id', 'Aros.alias', 'Users.email', 'Users.created', 'Users.status'
            ],
            'limit' =>  20
        ];

        $settings   =   array_merge($settings, $finderSettings);

        $users = $this->Users->find('all', array(
            'conditions'    =>  $settings,
            'contain'       =>  ['Aros', 'Employes', 'Thumbs'],
        ));

        $arosList   =   $this->Users->Aros->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias',
            'keyPath' =>  'id'
        ]);
        // $companyList   =   $this->Users->Companies->find('list', [
        //     'keyField' => 'id',
        //     'valueField' => 'name'
        // ]);
        $this->set(compact('users', 'arosList'));

        if ($this->request->is('ajax')) {
            $this->viewBuilder()->setLayout('ajax');
            $this->render('listItem');
        } else if($this->request->is('get') && $this->request->getQuery('excel') != null && $this->request->getQuery('excel') == 1) {

            $this->viewBuilder()->setLayout('ajax');
            $this->set('filename', $this->actionTitle . "_list.xlsx");
            $this->render('listItemExcel');

        }
    }

    /**
     * ListItem method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    // public function listItem()
    // {
    //     $this->Authorization->authorize($this->Users->newEmptyEntity(), 'read');

    //     $this->viewBuilder()->setLayout('ajax');

    //     $users = $this->paginate($this->Users);

    //     $this->set(compact('users'));
    // }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $user = $this->Users->get($id, [
            'contain' => ['Articles'],
        ]);

        $this->Authorization->authorize($user, 'read');

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->Authorization->authorize($this->Users->newEmptyEntity(), 'create');

        $user = $this->Users->newEmptyEntity();
        $userInformation = $this->Users->Employes->newEmptyEntity();
        $user->employe = $userInformation;

        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData(), [
                'associated'    =>  ['Employes']
            ]);
            // debug($user);
            // exit;
            if ($this->Users->save($user)) {

              if($user->imagefile->getError() == 0 && $user->imagefile->getSize() > 0) {
                  // doing the upload here.
                  $tmp_name  = $user->imagefile->getClientFilename();
                  $tmp       = $user->imagefile->getStream()->getMetadata('uri');
                  $mime_type = $user->imagefile->getClientMediaType();

                  $path_tmp = ROOT . DS . 'tmp' . DS . 'upload' . DS;

                  if (!is_dir($path_tmp)) {
                      mkdir($path_tmp, 0755);
                  }

                  $ext             = pathinfo($tmp_name, PATHINFO_EXTENSION);
                  $tmp_file_name   = md5(strval(time()));
                  $tmp_images1_img = $path_tmp . $tmp_file_name . "." . strtolower($ext);
                  $upload          = $user->imagefile->moveTo($tmp_images1_img);

                  $setting            =   $this->General->getSetting();

                  $resize                = $this->General->ResizeImageContent(
                      $tmp_images1_img,
                      $setting->cms_url,
                      $user->get('id'),
                      "User",
                      "thumb",
                      $mime_type,
                      200,
                      200,
                      "cropResize"
                  );

                  @unlink($tmp_images1_img);

              }
                $this->Flash->metronicSuccess(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->metronicError(__('The user could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U1']]);
        }

        $arosList   =   $this->Users->Aros->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias',
            'keyPath' =>  'id'
        ]);
        // $companyList   =   $this->Users->Companies->find('list', [
        //     'keyField' => 'id',
        //     'valueField' => 'name'
        // ]);
        $this->listItemTitle    =   "Add new user";
        $this->set('listItemTitle', $this->listItemTitle);

        $this->set(compact('user', 'arosList','companyList'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Employes', 'Aros', 'Thumbs'],
        ]);

        if(empty($user->employe)) {
            $userInformation = $this->Users->Employes->newEmptyEntity();
            $user->employe = $userInformation;
        }

        $this->Authorization->authorize($user, 'update');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            // $this->viewBuilder()->setLayout('ajax');

            // debug($user->imagefile);

            if ($this->Users->save($user)) {

                // if we edit the loggedin user
                if($user->get('id') == $this->loggedInUser->id) {
                    Cache::delete('user_logged_in_cache' . $this->loggedInUser->id, 'loggedInUser');
                    Cache::write('user_logged_in_cache' . $this->loggedInUser->id, $user, 'loggedInUser');
                    $this->loggedInUser = $user;
                }

                // Process uploaded file here.
                if($user->imagefile->getError() == 0 && $user->imagefile->getSize() > 0) {
                    // doing the upload here.
                    $tmp_name  = $user->imagefile->getClientFilename();
                    $tmp       = $user->imagefile->getStream()->getMetadata('uri');
                    $mime_type = $user->imagefile->getClientMediaType();

                    $path_tmp = ROOT . DS . 'tmp' . DS . 'upload' . DS;

                    if (!is_dir($path_tmp)) {
                        mkdir($path_tmp, 0755);
                    }

                    $ext             = pathinfo($tmp_name, PATHINFO_EXTENSION);
                    $tmp_file_name   = md5(strval(time()));
                    $tmp_images1_img = $path_tmp . $tmp_file_name . "." . strtolower($ext);
                    $upload          = $user->imagefile->moveTo($tmp_images1_img);

                    $setting            =   $this->General->getSetting();

                    $resize                = $this->General->ResizeImageContent(
                        $tmp_images1_img,
                        $setting->cms_url,
                        $user->get('id'),
                        "User",
                        "thumb",
                        $mime_type,
                        200,
                        200,
                        "cropResize"
                    );

                    @unlink($tmp_images1_img);

                }

                $this->Flash->metronicSuccess(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->metronicError(__('The user could not be saved. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U2']]);
        }

        $arosList   =   $this->Users->Aros->find('treeList', [
            'conditions'    =>  [
                'id !=' =>  1 // 1 untuk developer
            ],
            'valuePath'   =>  'alias',
            'keyPath' =>  'id'
        ]);

        // $this->loadModel("Companies");
        // $companyList    =   $this->Companies->find('list');

        $this->listItemTitle    =   "Edit : " . $user->get('email');
        $this->set('listItemTitle', $this->listItemTitle);



        $this->set(compact('user', 'arosList'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id, [
            'contain' => ['Employes'],
        ]);
        $this->Authorization->authorize($user, 'delete');
        if ($this->Users->delete($user)) {
            $this->Flash->metronicSuccess(__('The user has been deleted.'));
        } else {
            $this->Flash->metronicError(__('The user could not be deleted. Please, try again.'), ['params'  =>  ['code'   =>  'Code: U3']]);
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login()
    {

        $this->viewBuilder()->setLayout('metronic_login');

        $this->Authorization->skipAuthorization();

        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Dashboards',
                'action' => 'index',
            ]);

            return $this->redirect($redirect);
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }

    // in src/Controller/UsersController.php
    public function logout()
    {
        $this->Authorization->skipAuthorization();

        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            // delete cache
            Cache::delete('user_logged_in_cache' . $this->loggedInUser->id, 'loggedInUser');
            $this->loggedInUser = false;

            $this->Authentication->logout();

            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    public function deleteItems() {

      $this->viewBuilder()->setLayout('ajax');
      $this->request->allowMethod(['post', 'delete']);
      $this->loadModel("Users");
      $this->Authorization->authorize($this->Users->newEmptyEntity(), 'delete');

      $data   = array();
      $status = '0';




        if ($this->request->is('post')) {
          $data =   $this->request->getData();

          if(isset($data['users']) && count($data['users']) > 0) {

            $userIds   = array();
            $this->loadModel("Users");

            foreach($data['users'] as $key => $value) {
              if(intval($value['is_checked']) == 1) {

                $userIds[]   = $key;

              }
            }
              // debug($vehicleIds);
            if(!empty($userIds)) {
              $entities   = $this->Users->find('all', [
                'conditions'  =>  [
                  'Users.id in'  =>  $userIds,

                ],
                'contain' => ['Employes']
              ]);

              $this->Users->deleteMany($entities);
              $status   = '1';
            }
          }
        }


      $this->set('data', $data);
      $this->set('status', $status);
      $this->viewBuilder()->setOption('serialize', ['data', 'status']);

  }

}
