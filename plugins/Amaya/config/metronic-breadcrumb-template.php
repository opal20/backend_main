<?php 
	return [
        'wrapper' => '<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">{{content}}</ul>',
        'item' => '<li{{attrs}}><a href="{{url}}"{{innerAttrs}}>{{beforeTitle}}{{title}}{{afterTitle}}</a></li>{{separator}}',
        'itemWithoutLink' => '<li{{attrs}}><span{{innerAttrs}}>&nbsp;&nbsp;{{icon}}{{title}}</span></li>{{separator}}',
        'separator' => '<li class="m-nav__separator">{{separator}}</li>'
    ];