<?php

	return [
        // 'prevDisabled'  =>  '<li class="paginate_button page-item previous disabled"><a href="{{url}}" class="page-link"><i class="la la-angle-left"></i></a></li>',
        'first'  =>  '<li style="margin:3px;">
            <a href="{{url}}" class="ajaxPaginator">
                <span class="m-badge m-badge--light m-badge--bordered">
                    <i style="font-size:10px;" class="la la-angle-double-left"></i>
                </span>
            </a>
        </li>',
        'last'  =>  '<li style="margin:3px;">
            <a href="{{url}}" class="ajaxPaginator">
                <span class="m-badge m-badge--light m-badge--bordered">
                    <i style="font-size:10px;" class="la la-angle-double-right"></i>
                </span>
            </a>
        </li>',
        'prevDisabled'  =>  '<li class="paginate_button page-item previous disabled" id="datatable-buttons_previous"><a href="#" aria-controls="datatable-buttons" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>',
        'prevActive'  =>  '<li style="margin:3px;">
            <a href="{{url}}" class="ajaxPaginator">
                <span class="m-badge m-badge--light m-badge--bordered">
                    <i style="font-size:10px;" class="la la-angle-left"></i>
                </span>
            </a>
        </li>',
        'nextDisabled'  =>  '<li class="paginate_button page-item next disabled" id="datatable-buttons_next"><a href="#" aria-controls="datatable-buttons" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li>',
        'nextActive'  =>  '<li style="margin:3px;">
            <a href="{{url}}" class="ajaxPaginator">
                <span class="m-badge m-badge--light m-badge--bordered">
                    <i style="font-size:10px;" class="la la-angle-right"></i>
                </span>
            </a>
        </li>',
        'number' => '<li class="paginate_button page-item  "><a href="{{url}}" aria-controls="datatable" data-dt-idx="2" tabindex="0" class=ajaxPaginator">{{text}}</a></li>',
        'current'   =>  '<li class="paginate_button page-item active"><a href="#" aria-controls="datatable-buttons" data-dt-idx="1" tabindex="0" class="page-link">{{text}}</a></li>',
        'sort'			=>	'<a href={{url}} class="ajaxPaginator">{{text}}</a>',
        'sortAsc'		=>	'<a href={{url}} class="ajaxPaginator"><span>{{text}}&nbsp;<i class="la la-arrow-down"></i></span></a>',
        'sortDesc'		=>	'<a href={{url}} class="ajaxPaginator"><span>{{text}}&nbsp;<i class="la la-arrow-up"></i></span></a>',
        // 'current'  =>  '<li class="paginate_button page-item active"><a href="#" class="page-link">{{text}}</a></li>'
    ];
?>
