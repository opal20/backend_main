# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.27)
# Database: cake_cms
# Generation Time: 2020-10-21 03:36:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `acos_type_id` smallint(2) NOT NULL DEFAULT '2',
  `model` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;

INSERT INTO `acos` (`id`, `parent_id`, `acos_type_id`, `model`, `controller`, `alias`, `description`, `lft`, `rght`, `status`, `created`, `modified`)
VALUES
	(1,NULL,2,'','','top','',1,12,1,'2020-07-22 06:19:07','2020-07-22 06:19:07'),
	(2,1,2,NULL,'Articles','Articles','Untuk article stuff',2,3,1,'2020-07-22 06:36:39','2020-07-22 06:36:39'),
	(3,1,2,NULL,'Users','Users','Hello world',4,5,1,'2020-07-22 09:20:58','2020-07-22 09:20:58'),
	(4,1,2,NULL,'ModuleObjects','ModuleObjects','Untuk atur module object',6,7,1,'2020-07-23 08:13:26','2020-07-23 08:13:26'),
	(5,1,2,NULL,'Dashboards','Dashboards','Access to dashboard',8,9,1,'2020-07-27 09:21:33','2020-07-27 09:21:33'),
	(6,1,2,NULL,'CmsMenus','CmsMenus','Untuk cms menu',10,11,1,'2020-08-18 08:00:16','2020-08-18 08:00:16');

/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acos_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos_types`;

CREATE TABLE `acos_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `acos_types` WRITE;
/*!40000 ALTER TABLE `acos_types` DISABLE KEYS */;

INSERT INTO `acos_types` (`id`, `name`)
VALUES
	(1,'Superadmin Only'),
	(2,'All Admin');

/*!40000 ALTER TABLE `acos_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `total_admin` int(11) NOT NULL DEFAULT '0',
  `status` smallint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `description`, `lft`, `rght`, `total_admin`, `status`, `created`, `modified`)
VALUES
	(1,NULL,'',NULL,'Developer','You can do what ever you want',1,6,0,1,'2020-07-20 08:07:49','2020-07-20 08:07:49'),
	(2,1,'',NULL,'Super Admin','Super admin',2,5,1,1,'2020-07-20 08:12:42','2020-10-18 06:01:12'),
	(3,2,'',NULL,'Admin','Regular Adminsss',3,4,6,1,'2020-07-20 08:12:58','2020-10-20 03:32:23');

/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros_acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `aro_id` (`aro_id`),
  KEY `aco_id` (`aco_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`)
VALUES
	(1,3,2,'1','1','1','1'),
	(2,3,3,'1','1','1','1'),
	(3,3,4,'1','1','1','1'),
	(4,2,2,'1','1','1','1'),
	(5,2,3,'1','1','1','1'),
	(6,2,4,'1','1','1','1'),
	(7,3,5,'1','1','1','1'),
	(8,3,6,'1','0','1','1'),
	(9,2,5,'1','1','1','1'),
	(10,2,6,'1','1','1','1');

/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `body` text,
  `published` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `user_key` (`user_id`),
  CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` (`id`, `user_id`, `title`, `slug`, `body`, `published`, `created`, `modified`)
VALUES
	(3,1,'Hello World2 lingker','Hello-World','This is the body',0,'2020-07-02 09:24:48','2020-07-09 04:20:13'),
	(4,1,'Article with tags','Article-with-tags','Hello world, this is the article with tags\r\n\r\nRunning collller',0,'2020-07-09 03:35:46','2020-07-20 05:57:58'),
	(5,3,'Kossa Article-edit','Kossa-Article','Hello world, this is the article',0,'2020-07-13 04:35:48','2020-07-20 07:12:07');

/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table articles_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles_tags`;

CREATE TABLE `articles_tags` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`tag_id`),
  KEY `tag_key` (`tag_id`),
  CONSTRAINT `articles_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`),
  CONSTRAINT `articles_tags_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `articles_tags` WRITE;
/*!40000 ALTER TABLE `articles_tags` DISABLE KEYS */;

INSERT INTO `articles_tags` (`article_id`, `tag_id`)
VALUES
	(4,1),
	(3,2),
	(5,5),
	(5,6),
	(5,7);

/*!40000 ALTER TABLE `articles_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table audit_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `audit_logs`;

CREATE TABLE `audit_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction` char(36) NOT NULL,
  `type` varchar(7) NOT NULL,
  `primary_key` int(10) unsigned DEFAULT NULL,
  `source` varchar(255) NOT NULL,
  `parent_source` varchar(255) DEFAULT NULL,
  `original` mediumtext,
  `changed` mediumtext,
  `meta` mediumtext,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction` (`transaction`),
  KEY `type` (`type`),
  KEY `primary_key` (`primary_key`),
  KEY `source` (`source`),
  KEY `parent_source` (`parent_source`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `audit_logs` WRITE;
/*!40000 ALTER TABLE `audit_logs` DISABLE KEYS */;

INSERT INTO `audit_logs` (`id`, `transaction`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`, `created`)
VALUES
	(1,'1e6fa1f5-59fe-4c61-a339-0e03678c1ac0','update',1,'articles',NULL,'{\"title\":\"First Post\"}','{\"title\":\"First Postsss\"}','[]','2020-07-20 05:29:56'),
	(2,'1b865cf5-dea9-4562-bfcc-98c0aed11663','update',1,'articles',NULL,'{\"title\":\"First Postsss\"}','{\"title\":\"First Posts\"}','{\"ip\":\"::1\",\"url\":\"\\/articles\\/edit\\/first-post\",\"user\":3}','2020-07-20 05:34:08'),
	(3,'5e7369c7-92a3-4650-9389-c520f63a0e81','delete',1,'articles',NULL,NULL,NULL,'{\"ip\":\"::1\",\"url\":\"\\/articles\\/delete\\/first-post\",\"user\":3}','2020-07-20 05:35:11'),
	(4,'d04a294f-d9c0-4c85-82ed-349b32163d58','update',4,'articles',NULL,'{\"body\":\"Hello world, this is the article with tags\"}','{\"body\":\"Hello world, this is the article with tags\\r\\n\\r\\nRunning collller\"}','{\"ip\":\"::1\",\"url\":\"\\/articles\\/edit\\/Article-with-tags\",\"user\":3}','2020-07-20 05:57:58'),
	(5,'977d73e1-2f9d-4160-ac14-7b7da4ad0392','update',5,'articles',NULL,'{\"body\":\"Hello world, this is the articlesss\"}','{\"body\":\"Hello world, this is the article\"}','{\"ip\":\"::1\",\"url\":\"\\/articles\\/edit\\/Kossa-Article\",\"user\":3}','2020-07-20 07:12:07'),
	(6,'2cde6e43-b6cb-448f-bda5-fd5733ef1982','update',3,'aros',NULL,'{\"description\":\"Regular Admin\"}','{\"description\":\"Regular Adminsss\"}','{\"ip\":\"::1\",\"url\":\"\\/aros\\/edit\\/3\",\"user\":3}','2020-07-20 08:37:40'),
	(7,'49692574-d22a-425a-b655-697e3562db51','create',1,'acos',NULL,'{\"id\":1,\"acos_type_id\":2,\"model\":\"\",\"controller\":\"\",\"alias\":\"top\",\"description\":\"\",\"lft\":1,\"rght\":2,\"status\":1}','{\"id\":1,\"acos_type_id\":2,\"model\":\"\",\"controller\":\"\",\"alias\":\"top\",\"description\":\"\",\"lft\":1,\"rght\":2,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":3}','2020-07-22 06:19:07'),
	(8,'4534b5ce-83d7-4a52-a269-6f92a6309842','create',2,'acos',NULL,'{\"id\":2,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"ArticlesController\",\"alias\":\"Articles\",\"description\":\"Untuk article stuff\",\"lft\":2,\"rght\":3,\"status\":1}','{\"id\":2,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"Articles\",\"alias\":\"Articles\",\"description\":\"Untuk article stuff\",\"lft\":2,\"rght\":3,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":3}','2020-07-22 06:36:39'),
	(9,'82277bec-2d3b-4251-9a78-23aeeb002b82','create',3,'acos',NULL,'{\"id\":3,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"UsersController\",\"alias\":\"Users\",\"description\":\"Hello world\",\"lft\":4,\"rght\":5,\"status\":1}','{\"id\":3,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"Users\",\"alias\":\"Users\",\"description\":\"Hello world\",\"lft\":4,\"rght\":5,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":3}','2020-07-22 09:20:58'),
	(10,'bf9c14bb-1f48-4e0a-8ffa-70631852cd6a','create',4,'acos',NULL,'{\"id\":4,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"ModuleObjectsController\",\"alias\":\"ModuleObjects\",\"description\":\"Untuk atur module object\",\"lft\":6,\"rght\":7,\"status\":1}','{\"id\":4,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"ModuleObjects\",\"alias\":\"ModuleObjects\",\"description\":\"Untuk atur module object\",\"lft\":6,\"rght\":7,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":3}','2020-07-23 08:13:26'),
	(11,'a5c51c89-76ff-4b2e-9131-db13a09945eb','create',5,'acos',NULL,'{\"id\":5,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"DashboardController\",\"alias\":\"Dashboard\",\"description\":\"Access to dashboard\",\"lft\":8,\"rght\":9,\"status\":1}','{\"id\":5,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"Dashboard\",\"alias\":\"Dashboard\",\"description\":\"Access to dashboard\",\"lft\":8,\"rght\":9,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":8}','2020-07-27 09:21:33'),
	(12,'14805a3a-ad80-45f8-976e-fb7dfbd02063','create',11,'users',NULL,'{\"id\":11,\"aro_id\":3,\"email\":\"admin@boleh.net.id\",\"password\":\"$2y$10$owXkoze5GDrjQ.S0OwR4H.FUjreEaKR953m9rlESPjMB\\/dr9nVNTy\"}','{\"id\":11,\"aro_id\":3,\"email\":\"admin@boleh.net.id\",\"password\":\"$2y$10$owXkoze5GDrjQ.S0OwR4H.FUjreEaKR953m9rlESPjMB\\/dr9nVNTy\"}','{\"ip\":\"::1\",\"url\":\"\\/users\\/add\",\"user\":8}','2020-08-13 07:20:45'),
	(13,'2d949111-e8de-4559-a5c9-45785b2043da','create',6,'acos',NULL,'{\"id\":6,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"CmsMenusController\",\"alias\":\"CmsMenus\",\"description\":\"Untuk cms menu\",\"lft\":10,\"rght\":11,\"status\":1}','{\"id\":6,\"parent_id\":1,\"acos_type_id\":2,\"controller\":\"CmsMenus\",\"alias\":\"CmsMenus\",\"description\":\"Untuk cms menu\",\"lft\":10,\"rght\":11,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/acos\\/add\",\"user\":8}','2020-08-18 08:00:16'),
	(14,'c6d1e74e-4d2b-498c-9a3c-8ca1735bdf8d','create',1,'cms_menus',NULL,'{\"id\":1,\"lft\":1,\"rght\":2,\"name\":\"Top Level Menu\",\"icon_class\":\"\",\"is_group_separator\":0,\"status\":1}','{\"id\":1,\"lft\":1,\"rght\":2,\"name\":\"Top Level Menu\",\"icon_class\":\"\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":8}','2020-08-25 04:11:55'),
	(15,'47a7f933-a3dd-48ec-a941-ef783103a7f6','create',2,'cms_menus',NULL,'{\"id\":2,\"aco_id\":5,\"parent_id\":1,\"lft\":2,\"rght\":3,\"action\":\"\",\"name\":\"Dashboard\",\"icon_class\":\"flaticon-analytics\",\"is_group_separator\":0,\"status\":1}','{\"id\":2,\"aco_id\":5,\"parent_id\":1,\"lft\":2,\"rght\":3,\"action\":\"\",\"name\":\"Dashboard\",\"icon_class\":\"flaticon-analytics\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":8}','2020-08-25 04:18:12'),
	(16,'c786fef5-4b30-492f-8eea-6942ad353689','create',3,'cms_menus',NULL,'{\"id\":3,\"aco_id\":3,\"parent_id\":1,\"lft\":4,\"rght\":5,\"action\":\"index\",\"name\":\"User List\",\"icon_class\":\"flaticon-avatar\",\"is_group_separator\":0,\"status\":1}','{\"id\":3,\"aco_id\":3,\"parent_id\":1,\"lft\":4,\"rght\":5,\"action\":\"index\",\"name\":\"User List\",\"icon_class\":\"flaticon-avatar\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":8}','2020-08-25 06:37:12'),
	(17,'3436deb0-0810-478d-8a4b-2b5c1f6e6fc0','update',3,'cms_menus',NULL,'{\"aco_id\":3,\"action\":\"index\",\"name\":\"User List\",\"icon_class\":\"flaticon-avatar\",\"is_group_separator\":0}','{\"aco_id\":null,\"action\":\"\",\"name\":\"Users\",\"icon_class\":\"flaticon-users\",\"is_group_separator\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/edit\\/3\",\"user\":8}','2020-08-28 03:25:36'),
	(18,'f3a93b9a-c18a-4df4-a4b0-348838186c4b','create',4,'cms_menus',NULL,'{\"id\":4,\"aco_id\":3,\"parent_id\":3,\"lft\":5,\"rght\":6,\"action\":\"Index\",\"name\":\"User List\",\"icon_class\":\"flaticon-users\",\"is_group_separator\":0,\"status\":1}','{\"id\":4,\"aco_id\":3,\"parent_id\":3,\"lft\":5,\"rght\":6,\"action\":\"Index\",\"name\":\"User List\",\"icon_class\":\"flaticon-users\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":8}','2020-08-28 03:26:12'),
	(19,'3d4471c4-82b7-4a58-9683-5092a496d4c7','create',5,'cms_menus',NULL,'{\"id\":5,\"parent_id\":1,\"lft\":8,\"rght\":9,\"action\":\"\",\"name\":\"Administrator\",\"icon_class\":\"flaticon-imac\",\"is_group_separator\":1,\"status\":1}','{\"id\":5,\"parent_id\":1,\"lft\":8,\"rght\":9,\"action\":\"\",\"name\":\"Administrator\",\"icon_class\":\"flaticon-imac\",\"is_group_separator\":1,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":2}','2020-10-17 05:54:57'),
	(20,'9def3abd-59ec-4352-b34c-531202debe1a','create',6,'cms_menus',NULL,'{\"id\":6,\"aco_id\":6,\"parent_id\":5,\"lft\":9,\"rght\":10,\"action\":\"Index\",\"name\":\"Cms Menu\",\"icon_class\":\"flaticon-signs-1\",\"is_group_separator\":0,\"status\":1}','{\"id\":6,\"aco_id\":6,\"parent_id\":5,\"lft\":9,\"rght\":10,\"action\":\"Index\",\"name\":\"Cms Menu\",\"icon_class\":\"flaticon-signs-1\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":2}','2020-10-17 05:57:36'),
	(21,'6631364c-35ca-42f7-b090-3fb285b44f5c','create',7,'cms_menus',NULL,'{\"id\":7,\"aco_id\":4,\"parent_id\":5,\"lft\":11,\"rght\":12,\"action\":\"\",\"name\":\"Module CMS\",\"icon_class\":\"flaticon-signs-1\",\"is_group_separator\":0,\"status\":1}','{\"id\":7,\"aco_id\":4,\"parent_id\":5,\"lft\":11,\"rght\":12,\"action\":\"\",\"name\":\"Module CMS\",\"icon_class\":\"flaticon-signs-1\",\"is_group_separator\":0,\"status\":1}','{\"ip\":\"::1\",\"url\":\"\\/cms-menus\\/add\",\"user\":2}','2020-10-18 04:56:43'),
	(22,'2cfa722f-8c89-4e17-80da-7217a1b25879','update',11,'users',NULL,'{\"password\":\"$2y$10$owXkoze5GDrjQ.S0OwR4H.FUjreEaKR953m9rlESPjMB\\/dr9nVNTy\"}','{\"password\":\"$2y$10$2IhhzFTmFys6bVXOxGxNluirqibqorYAMvH43sBLyVbxA4LChpv5C\"}','{\"ip\":\"::1\",\"url\":\"\\/users\\/edit\\/11\",\"user\":1}','2020-10-18 05:01:01'),
	(23,'fcad0696-4edf-4976-b8ac-2db9986162aa','update',1,'users',NULL,'{\"password\":\"$2y$10$2IhhzFTmFys6bVXOxGxNluirqibqorYAMvH43sBLyVbxA4LChpv5C\"}','{\"password\":\"$2y$10$Pg.nNYzzxfbWhWH0cDXBPu.FysGliOsNskMHldLfosIClNVjHIBWi\"}','{\"ip\":\"::1\",\"url\":\"\\/users\\/edit\\/1\",\"user\":1}','2020-10-18 05:01:57');

/*!40000 ALTER TABLE `audit_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table audit_stash_phinxlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `audit_stash_phinxlog`;

CREATE TABLE `audit_stash_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `audit_stash_phinxlog` WRITE;
/*!40000 ALTER TABLE `audit_stash_phinxlog` DISABLE KEYS */;

INSERT INTO `audit_stash_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`)
VALUES
	(20171018185609,'CreateAuditLogs','2020-07-20 05:25:23','2020-07-20 05:25:23',0);

/*!40000 ALTER TABLE `audit_stash_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cms_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cms_menus`;

CREATE TABLE `cms_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aco_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `action` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `icon_class` varchar(255) DEFAULT '',
  `is_group_separator` smallint(1) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `lft` (`lft`),
  KEY `rght` (`rght`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cms_menus` WRITE;
/*!40000 ALTER TABLE `cms_menus` DISABLE KEYS */;

INSERT INTO `cms_menus` (`id`, `aco_id`, `parent_id`, `lft`, `rght`, `action`, `name`, `icon_class`, `is_group_separator`, `status`, `created`, `modified`)
VALUES
	(1,NULL,NULL,1,14,NULL,'Top Level Menu','',0,1,'2020-08-25 04:11:55','2020-08-25 04:11:55'),
	(2,5,1,2,3,'','Dashboard','flaticon-analytics',0,1,'2020-08-25 04:18:12','2020-08-25 04:18:12'),
	(3,NULL,1,4,7,'','Users','flaticon-users',1,1,'2020-08-25 06:37:12','2020-08-28 03:25:36'),
	(4,3,3,5,6,'Index','User List','flaticon-users',0,1,'2020-08-28 03:26:12','2020-08-28 03:26:12'),
	(5,NULL,1,8,13,'','Administrator','flaticon-imac',1,1,'2020-10-17 05:54:57','2020-10-17 05:54:57'),
	(6,6,5,9,10,'Index','Cms Menu','flaticon-signs-1',0,1,'2020-10-17 05:57:36','2020-10-17 05:57:36'),
	(7,4,5,11,12,'','Module CMS','flaticon-signs-1',0,1,'2020-10-18 04:56:43','2020-10-18 04:56:43');

/*!40000 ALTER TABLE `cms_menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table contents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contents`;

CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `host` varchar(255) NOT NULL,
  `url` varchar(100) NOT NULL,
  `mime_type` varchar(100) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `model` (`model`),
  KEY `model_id` (`model_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;

INSERT INTO `contents` (`id`, `model`, `model_id`, `type`, `host`, `url`, `mime_type`, `path`, `width`, `height`, `created`, `modified`)
VALUES
	(2,'User',1,'thumb','http://localhost:8765/','contents/User/1/namafile.jpg','image/jpeg','/Library/WebServer/Documents/latihan/cake4/cms/webroot/contents/User/1/namafile.jpg',150,150,NULL,NULL),
	(3,'User',11,'thumb','http://localhost:8765/','contents/User/11/11_thumb.jpg','image/jpeg','/Library/WebServer/Documents/latihan/cake4/cms/webroot/contents/User/11/11_thumb.jpg',200,200,NULL,NULL),
	(4,'User',10,'thumb','http://localhost:8765/','contents/User/10/10_thumb.jpg','image/jpeg','/Library/WebServer/Documents/latihan/cake4/cms/webroot/contents/User/10/10_thumb.jpg',200,200,NULL,NULL);

/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table restricted_controllers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `restricted_controllers`;

CREATE TABLE `restricted_controllers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `restricted_controllers` WRITE;
/*!40000 ALTER TABLE `restricted_controllers` DISABLE KEYS */;

INSERT INTO `restricted_controllers` (`id`, `name`)
VALUES
	(1,'AccountController'),
	(2,'PagesController'),
	(3,'TemplateController'),
	(4,'AppController'),
	(5,'ApiController'),
	(6,'ErrorController');

/*!40000 ALTER TABLE `restricted_controllers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_url` varchar(255) DEFAULT NULL,
  `cms_title` varchar(255) DEFAULT NULL,
  `cms_description` text,
  `cms_keywords` text,
  `cms_author` varchar(255) DEFAULT NULL,
  `cms_app_name` varchar(255) DEFAULT NULL,
  `cms_logo_url` varchar(255) DEFAULT NULL,
  `company_brand_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `customer_phone_number` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `copyright_text` varchar(255) NOT NULL,
  `email_logo_url` varchar(205) DEFAULT NULL,
  `path_content` varchar(255) DEFAULT NULL,
  `path_webroot` varchar(255) DEFAULT NULL,
  `map_api_key` varchar(255) DEFAULT NULL,
  `map_android_api_key` varchar(255) DEFAULT NULL,
  `map_browser_api_key` varchar(255) DEFAULT NULL,
  `firebase_api_key` text NOT NULL,
  `facebook_app_id` varchar(255) DEFAULT NULL,
  `google_client_id` text,
  `google_client_secret` varchar(255) DEFAULT NULL,
  `default_lat` varchar(255) DEFAULT NULL,
  `default_lng` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`id`, `cms_url`, `cms_title`, `cms_description`, `cms_keywords`, `cms_author`, `cms_app_name`, `cms_logo_url`, `company_brand_name`, `company_address`, `customer_phone_number`, `customer_email`, `admin_email`, `copyright_text`, `email_logo_url`, `path_content`, `path_webroot`, `map_api_key`, `map_android_api_key`, `map_browser_api_key`, `firebase_api_key`, `facebook_app_id`, `google_client_id`, `google_client_secret`, `default_lat`, `default_lng`, `modified`)
VALUES
	(1,'http://localhost:8765/','Trade Com','Trade Com','Trade Com','Trade Com','Trade Com',NULL,'Trade Com','','','','','@Trade Com 2019','http://www.tradecom.com/img/logo-divertune-small.png','/Library/WebServer/Documents/latihan/cake4/cms/webroot/contents/','/Library/WebServer/Documents/latihan/cake4/cms/webroot/','','','','','','','','-6.175414','106.827122','2017-09-05 15:53:25');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`id`, `title`, `created`, `modified`)
VALUES
	(1,'TagHello','2020-07-03 05:07:37','2020-07-03 05:07:37'),
	(2,'Berita','2020-07-03 05:07:55','2020-07-03 05:08:39'),
	(3,'Teknologi','2020-07-09 04:12:02','2020-07-09 04:12:02'),
	(4,'pemilu','2020-07-09 04:12:02','2020-07-09 04:12:02'),
	(5,'jamiroquai','2020-07-13 04:35:48','2020-07-13 04:35:48'),
	(6,'virtual','2020-07-13 04:35:48','2020-07-13 04:35:48'),
	(7,'insanity','2020-07-13 04:35:48','2020-07-13 04:35:48');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_informations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_informations`;

CREATE TABLE `user_informations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_key` (`user_id`),
  CONSTRAINT `user_information_fk1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_informations` WRITE;
/*!40000 ALTER TABLE `user_informations` DISABLE KEYS */;

INSERT INTO `user_informations` (`id`, `user_id`, `company`, `created`, `modified`)
VALUES
	(1,3,'kossa company',NULL,'2020-07-20 04:50:27'),
	(2,7,'hello world','2020-07-20 05:08:11','2020-07-20 05:08:11'),
	(3,6,'u2 company','2020-07-20 05:10:29','2020-07-20 05:10:29'),
	(4,2,'Hello world','2020-07-20 05:13:19','2020-07-20 05:13:19'),
	(5,8,'boleh','2020-07-24 10:16:01','2020-07-24 10:16:01'),
	(6,9,'asdlfkjasdlfkj','2020-08-12 08:26:56','2020-08-12 08:26:56'),
	(7,10,'U2 is the bombs','2020-08-12 08:27:32','2020-08-13 02:29:46'),
	(8,11,'boleh','2020-08-13 07:20:45','2020-08-13 07:20:45'),
	(9,1,'Boleh','2020-10-18 05:01:57','2020-10-18 05:01:57');

/*!40000 ALTER TABLE `user_informations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aro_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `aro_id`, `email`, `password`, `created`, `modified`)
VALUES
	(1,2,'admin@amaya.com','$2y$10$Pg.nNYzzxfbWhWH0cDXBPu.FysGliOsNskMHldLfosIClNVjHIBWi','2020-07-02 15:15:51','2020-10-18 05:01:57'),
	(2,3,'sugar@town.com','$2y$10$JEvi6WcG0Hqzx9pPsc0OJeIP3tKOdGF9ZJRjIE1Efzqeq/qajKvwe','2020-07-10 07:10:16','2020-07-20 05:13:19'),
	(3,3,'kossa@boleh.net.id','$2y$10$SVIjPvGmm0pKJLLUfxE5wu4N4COy4csuqUnwOfhMvaqchJv7Kees.','2020-07-13 04:23:10','2020-07-21 02:41:35'),
	(6,NULL,'bono@u2.com','$2y$10$Ru0kXMLbnnAuWL2/5Aj7Lu0ewOS30KfK4KO4JcDifNfzEsf6Y9.VW','2020-07-20 04:07:05','2020-07-20 05:10:29'),
	(7,NULL,'kossa2@boleh.com','$2y$10$gDPoS8KU0kE4KN4giS..UOjuqRdprfJ9CE/1PeJEL/HGweTVqgp56','2020-07-20 05:08:11','2020-07-20 05:08:11'),
	(8,3,'kossa2@boleh.net.id','$2y$10$LIEYNXnduvrdSmy3psTFq.UKlNKPjdKWxz4H72xfVwHHj7SvUvpLO','2020-07-24 10:16:01','2020-07-24 10:16:01'),
	(9,3,'test@gmail.com','$2y$10$r3Sy18CjQhffWLYAL/L7e.N08u13tvgmfZyNOVC.8I9YwsoJOBob6','2020-08-12 08:26:56','2020-08-12 08:26:56'),
	(10,3,'test2@gmail.com','$2y$10$y/3TP.ZIsPdo3ZqHO8Sqa.IPve0hA4dYYuRdSoTLTknP18qh5l7ti','2020-08-12 08:27:32','2020-10-20 01:08:36'),
	(11,3,'admin@boleh.net.id','$2y$10$2IhhzFTmFys6bVXOxGxNluirqibqorYAMvH43sBLyVbxA4LChpv5C','2020-08-13 07:20:45','2020-10-19 12:17:53');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
