<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CmsMenusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CmsMenusTable Test Case
 */
class CmsMenusTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CmsMenusTable
     */
    protected $CmsMenus;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.CmsMenus',
        'app.Acos',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CmsMenus') ? [] : ['className' => CmsMenusTable::class];
        $this->CmsMenus = TableRegistry::getTableLocator()->get('CmsMenus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->CmsMenus);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getAvailableControllers method
     *
     * @return void
     */
    public function testGetAvailableControllers(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
