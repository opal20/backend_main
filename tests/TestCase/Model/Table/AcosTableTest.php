<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AcosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AcosTable Test Case
 */
class AcosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AcosTable
     */
    protected $Acos;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Acos',
        'app.AcosTypes',
        'app.Aros',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Acos') ? [] : ['className' => AcosTable::class];
        $this->Acos = TableRegistry::getTableLocator()->get('Acos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Acos);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
