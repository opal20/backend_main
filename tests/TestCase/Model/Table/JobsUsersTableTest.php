<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JobsUsersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JobsUsersTable Test Case
 */
class JobsUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\JobsUsersTable
     */
    protected $JobsUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.JobsUsers',
        'app.Jobs',
        'app.Employes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('JobsUsers') ? [] : ['className' => JobsUsersTable::class];
        $this->JobsUsers = $this->getTableLocator()->get('JobsUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->JobsUsers);

        parent::tearDown();
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
