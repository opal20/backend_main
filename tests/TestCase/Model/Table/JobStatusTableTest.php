<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\JobStatusTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\JobStatusTable Test Case
 */
class JobStatusTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\JobStatusTable
     */
    protected $JobStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.JobStatus',
        'app.Jobs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('JobStatus') ? [] : ['className' => JobStatusTable::class];
        $this->JobStatus = $this->getTableLocator()->get('JobStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->JobStatus);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
