<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmployesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmployesTable Test Case
 */
class EmployesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\EmployesTable
     */
    protected $Employes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Employes',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Employes') ? [] : ['className' => EmployesTable::class];
        $this->Employes = $this->getTableLocator()->get('Employes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Employes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
